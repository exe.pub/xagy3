use std::time::Instant;

use crate::discord;
use crate::reactor::{PanicSender, ReactorError, Void};
use crate::{events::ForwardedEvent, irc_conn, rcon};
use async_trait::async_trait;

pub type EndpointId = String;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub enum EndpointType {
    Irc,
    Xonotic,
    Discord,
}

pub enum TypedEndpoint {
    Irc(irc_conn::IrcConnection),
    Xonotic(rcon::RconConnection),
    Discord(discord::DiscordConnection),
}

impl TypedEndpoint {
    pub fn get_endpoint_type(&self) -> EndpointType {
        match self {
            TypedEndpoint::Xonotic(_) => EndpointType::Xonotic,
            TypedEndpoint::Irc(_) => EndpointType::Irc,
            TypedEndpoint::Discord(_) => EndpointType::Discord,
        }
    }
}

#[derive(Debug)]
pub enum ConnectionStatus<T> {
    Connected(T),
    Disconnected(Instant),
}

impl<T> Default for ConnectionStatus<T> {
    fn default() -> ConnectionStatus<T> {
        ConnectionStatus::Disconnected(Instant::now())
    }
}

#[async_trait]
pub trait Endpoint {
    fn get_endpoint_id(&self) -> &str;
    async fn disconnected_since(&self) -> Option<Instant>;
    async fn run(
        &self,
        event_sender: &PanicSender<ForwardedEvent>,
    ) -> Result<Void, ReactorError>;
}

impl std::fmt::Debug for dyn Endpoint + Sync + Send {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Endpoint({})", self.get_endpoint_id())
    }
}

#[async_trait]
impl Endpoint for TypedEndpoint {
    fn get_endpoint_id(&self) -> &str {
        match self {
            TypedEndpoint::Irc(e) => e.get_endpoint_id(),
            TypedEndpoint::Xonotic(e) => e.get_endpoint_id(),
            TypedEndpoint::Discord(e) => e.get_endpoint_id(),
        }
    }

    async fn disconnected_since(&self) -> Option<Instant> {
        match self {
            TypedEndpoint::Irc(e) => e.disconnected_since().await,
            TypedEndpoint::Xonotic(e) => e.disconnected_since().await,
            TypedEndpoint::Discord(e) => e.disconnected_since().await,
        }
    }

    async fn run(
        &self,
        event_sender: &PanicSender<ForwardedEvent>,
    ) -> Result<Void, ReactorError> {
        match self {
            TypedEndpoint::Irc(e) => e.run(event_sender).await,
            TypedEndpoint::Xonotic(e) => e.run(event_sender).await,
            TypedEndpoint::Discord(e) => e.run(event_sender).await,
        }
    }
}

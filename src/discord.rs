use crate::command::Command;
use crate::config;
use crate::endpoint::{Endpoint, EndpointId};
use crate::events::{
    DeliveredEvent, DeliveredEventPayload, ForwardedEvent, MessageSpec,
    XagyEvent,
};
use crate::irc_conn::IrcEvent;
use crate::rcon::{f64_to_decimal, format_cts_time, RconForwardedEvent};
use crate::reactor::{PanicSender, ReactorError, Void};
use bimap::BiMap;
use dpcolors::DPString;
use serenity::async_trait;
use serenity::builder::CreateMessage;
use serenity::model::channel::{Channel, Message};
use serenity::model::gateway::Ready;
use serenity::model::prelude::ChannelId;
use serenity::prelude::*;
use std::fmt::Write as _;
use std::sync::Arc;
use std::time::{Duration, Instant};
use thiserror::Error;
use tokio::sync::mpsc::UnboundedReceiver;
use tokio::sync::Mutex;
use tokio::time::sleep;
use tracing::{error, info, warn};

#[derive(Debug, Error)]
pub enum DiscordError {
    #[error("Serenity error: {0}")]
    SerenityError(#[from] SerenityError),
    #[error("Discord connection lost")]
    Disconnected,
    #[error("Internal error")]
    InternalError,
    #[error("Configuration error: {0}")]
    ConfigurationError(String),
}

struct DiscordEventHandler {
    endpoint_id: EndpointId,
    command_prefixes: Vec<String>,
    sender: PanicSender<ForwardedEvent>,
    self_id: u64,
}

#[derive(Debug, Clone)]
pub enum DiscordEvent {
    Message(MessageSpec, String, DPString),
}

impl XagyEvent for DiscordEvent {
    fn matches_input(&self, inp: &config::RoutingRuleInput) -> bool {
        match self {
            DiscordEvent::Message(spec, _user, msg) => {
                inp.from_channels.contains(spec) && msg.starts_with(&inp.prefix)
            }
        }
    }
}

#[async_trait]
impl EventHandler for DiscordEventHandler {
    async fn message(&self, ctx: Context, msg: Message) {
        info!("Got discord message from {}: {msg:?}", msg.author.id.get());
        if msg.author.id.get() == self.self_id {
            // we ignore messages from ourselves
            return;
        }
        let spec = match ctx.http.get_channel(msg.channel_id).await {
            Ok(Channel::Guild(channel)) => MessageSpec::Channel(channel.name),
            Ok(Channel::Private(channel)) => {
                MessageSpec::PrivateUsername(channel.recipient.name)
            }
            Ok(other) => {
                error!(
                    "Could not get spec for discord, unknown channel {:?}",
                    other
                );
                MessageSpec::None
            }
            Err(e) => {
                error!("Could not get spec for discord {}", e);
                MessageSpec::None
            }
        };
        let mut content_with_attachments = msg.content.clone();
        for attach in msg.attachments.iter() {
            content_with_attachments
                .push_str(&format!("\n{}: {}", attach.filename, attach.url));
        }
        let content = DPString::parse_none(&content_with_attachments);
        let server_nick = msg.author_nick(ctx.http.clone()).await;
        let username = server_nick
            .unwrap_or(msg.author.global_name.unwrap_or(msg.author.name));
        let ev = if let Some(cmd) = Command::maybe_from(
            &self.command_prefixes,
            msg.author.id.get() as usize,
            DPString::parse_none(&username),
            spec.clone(),
            content.clone(),
        ) {
            ForwardedEvent::new_command(self.endpoint_id.clone(), cmd)
        } else {
            ForwardedEvent::new_discord(
                self.endpoint_id.clone(),
                DiscordEvent::Message(spec, username, content),
            )
        };
        self.sender.send(ev)
    }

    async fn ready(&self, _: Context, ready: Ready) {
        info!("Discord endpoint is connected: {}!", ready.user.name);
    }
}

pub struct DiscordConnection {
    endpoint_id: EndpointId,
    pub endpoint_config: config::Endpoint,
    client: Mutex<Client>,
    outer_event_receiver: Mutex<UnboundedReceiver<DeliveredEvent>>,
    pub channels: BiMap<String, u64>,
    http: Arc<serenity::http::Http>,
}

impl DiscordConnection {
    pub async fn new(
        endpoint_id: &str,
        endpoint_config: config::Endpoint,
        token: &str,
        channels: BiMap<String, u64>,
        self_id: u64,
        outer_event_receiver: Mutex<UnboundedReceiver<DeliveredEvent>>,
        sender: PanicSender<ForwardedEvent>,
    ) -> Result<Self, DiscordError> {
        let intents = GatewayIntents::GUILD_MESSAGES
            | GatewayIntents::DIRECT_MESSAGES
            | GatewayIntents::MESSAGE_CONTENT;
        let client = Client::builder(token, intents)
            .event_handler(DiscordEventHandler {
                endpoint_id: endpoint_id.into(),
                sender,
                command_prefixes: endpoint_config.command_prefixes.clone(),
                self_id,
            })
            .await?;
        let http = client.http.clone();
        Ok(Self {
            endpoint_id: endpoint_id.into(),
            endpoint_config,
            outer_event_receiver,
            client: Mutex::new(client),
            channels,
            http,
        })
    }

    async fn send_message(
        &self,
        spec: MessageSpec,
        msg: &str,
    ) -> Result<(), DiscordError> {
        match spec {
            MessageSpec::Channel(chan) => {
                if let Some(chan_id) = self.channels.get_by_left(&chan) {
                    let chan = ChannelId::new(*chan_id);
                    let cmsg = CreateMessage::new().content(msg);
                    chan.send_message(&self.http, cmsg).await?;
                    Ok(())
                } else {
                    Err(DiscordError::ConfigurationError(format!(
                        "Could not find channel id for channel {}",
                        chan
                    )))
                }
            }
            _ => {
                warn!("Discord endpoint can't handle message spec {:?}", spec);
                Ok(())
            }
        }
    }

    async fn process_rcon_event(
        &self,
        prefix: String,
        message_spec: MessageSpec,
        _from: EndpointId,
        ev: RconForwardedEvent,
    ) -> Result<(), DiscordError> {
        match ev {
            RconForwardedEvent::Chat {
                player_name,
                team,
                message,
            } => {
                let msg = if let Some(_team) = team {
                    format!(
                        "**{}({})** {}",
                        prefix,
                        player_name.to_none(),
                        message.to_none()
                    )
                } else {
                    format!(
                        "**{}{}:** {}",
                        prefix,
                        player_name.to_none(),
                        message.to_none()
                    )
                };
                self.send_message(message_spec, &msg).await?;
            }
            RconForwardedEvent::Join {
                mode,
                player,
                map,
                players,
            } => {
                let mut msg =
                    format!("`{}+ join: {}", prefix, player.name.to_none());
                if let Some(ref country) = player.city_data.country {
                    let _ = write!(msg, " {}", country.1);
                }
                if let Some(formatted_stats) = player.format_stats(mode) {
                    let _ = write!(msg, " [{}]", formatted_stats.to_none());
                }
                let _ = write!(
                    msg,
                    "` [{}](https://xdf.gg/map/{}) `[{}/{}]`",
                    map, map, players.0, players.1
                );
                self.send_message(message_spec, &msg).await?;
            }
            RconForwardedEvent::Part {
                player,
                map,
                players,
            } => {
                let mut msg =
                    format!("`{}- part: {}", prefix, player.name.to_none());
                if let Some(ref country) = player.city_data.country {
                    let _ = write!(msg, " {}", country.1);
                }
                let _ = write!(
                    msg,
                    "` [{}](https://xdf.gg/map/{}) `[{}/{}]`",
                    map, map, players.0, players.1
                );
                self.send_message(message_spec, &msg).await?;
            }
            RconForwardedEvent::GameStart { mode, map, players } => {
                if players.0 > 0 {
                    let msg = format!(
                        "`{}Playing {} on` [{}](https://xdf.gg/map/{}) `[{}/{}]`",
                        prefix,
                        mode.to_long_str(),
                        map,
                        map,
                        players.0,
                        players.1,
                    );
                    self.send_message(message_spec, &msg).await?;
                }
            }
            RconForwardedEvent::ServerConnected { host, connect_url } => {
                let msg = format!(
                    "`{}Server Connected: {}; connect at: {}`",
                    prefix, host, connect_url
                );
                self.send_message(message_spec, &msg).await?;
            }
            RconForwardedEvent::ServerDisconnected { host } => {
                let msg = format!("`{}Server Disconnected: {}`", prefix, host);
                self.send_message(message_spec, &msg).await?;
            }
            RconForwardedEvent::NameChange { old_name, new_name } => {
                let msg = format!(
                    "`{}* {} is now known as {}`",
                    prefix,
                    old_name.to_none(),
                    new_name.to_none()
                );
                self.send_message(message_spec, &msg).await?;
            }
            RconForwardedEvent::VCall { player_name, vote } => {
                let msg = format!(
                    "`{}* {} calls a vote for {}`",
                    prefix,
                    player_name.to_none(),
                    vote.to_none()
                );
                self.send_message(message_spec, &msg).await?;
            }
            RconForwardedEvent::VStop { player_name, vote } => {
                let msg = format!(
                    "`{}* {} stopped {} vote for {}`",
                    prefix,
                    player_name.to_none(),
                    player_name.to_none(),
                    vote.to_irc()
                );
                self.send_message(message_spec, &msg).await?;
            }
            RconForwardedEvent::VResult {
                vote,
                passed,
                accepted,
                rejected,
                ..
            } => {
                let res = if passed { "passed" } else { "rejected" };
                let msg = format!(
                    "`{}* vote for {} {}: {}:{}`",
                    prefix,
                    vote.to_none(),
                    res,
                    accepted,
                    rejected
                );
                self.send_message(message_spec, &msg).await?;
            }
            RconForwardedEvent::RecordSet {
                player_name,
                map,
                pos,
                time,
            } => {
                let (position, medal) = match pos {
                    1 => ("1st", "\u{1F947}"),
                    2 => ("2nd", "\u{1F948}"),
                    3 => ("3rd", "\u{1F949}"),
                    _ => ("", ""),
                };
                if pos <= 3 {
                    let msg = format!(
                        "`{}\\o/ {map} {medal}{position}: {} - {}`",
                        prefix,
                        player_name.to_none(),
                        format_cts_time(f64_to_decimal(time, 2))
                    );
                    self.send_message(message_spec, &msg).await?;
                }
            }
        }
        Ok(())
    }

    async fn process_irc_event(
        &self,
        prefix: String,
        message_spec: MessageSpec,
        _from: EndpointId,
        ev: IrcEvent,
    ) -> Result<(), DiscordError> {
        match ev {
            IrcEvent::Message(_chan, nick, msg) => {
                info!("{}, {}", nick, msg.to_none());
                let msg = format!("**{prefix}{nick}:** {}", msg.to_none());
                self.send_message(message_spec, &msg).await?;
            }
            _ => {}
        }
        Ok(())
    }

    async fn process_discord_event(
        &self,
        _prefix: String,
        _message_spec: MessageSpec,
        _from: EndpointId,
        _ev: DiscordEvent,
    ) -> Result<(), DiscordError> {
        Ok(())
    }

    async fn process_command(
        &self,
        prefix: String,
        message_spec: MessageSpec,
        cmd: Command,
    ) -> Result<(), DiscordError> {
        if let Some(cmd_prefix) = self.endpoint_config.command_prefixes.get(0) {
            let msg = format!(
                "**{}{}:** {}{} {}",
                prefix,
                cmd.nickname.to_none(),
                cmd_prefix,
                cmd.cmd,
                cmd.args.to_none()
            );
            self.send_message(message_spec, &msg).await?;
        }
        Ok(())
    }

    async fn process_reactor_message(
        &self,
        message_spec: MessageSpec,
        message: DPString,
    ) -> Result<(), DiscordError> {
        self.send_message(message_spec, &format!("`{}`", message.to_none()))
            .await?;
        Ok(())
    }

    async fn recv_outer_events(&self) -> Result<Void, DiscordError> {
        let mut receiver = self.outer_event_receiver.lock().await;
        loop {
            if let Some(ev) = receiver.recv().await {
                if !ev.validate_ttl() {
                    warn!(
                        "Discarding old event at endpoint {}",
                        self.endpoint_id
                    );
                    continue;
                }
                info!("Endpoint {} receieved event {:?}", self.endpoint_id, ev);
                match ev {
                    DeliveredEvent::EndpointEvent {
                        from,
                        message_spec,
                        payload,
                        prefix,
                        ..
                    } => match payload {
                        DeliveredEventPayload::RconEvent(re) => {
                            self.process_rcon_event(
                                prefix,
                                message_spec,
                                from,
                                re,
                            )
                            .await?;
                        }
                        DeliveredEventPayload::IrcEvent(ie) => {
                            self.process_irc_event(
                                prefix,
                                message_spec,
                                from,
                                ie,
                            )
                            .await?;
                        }
                        DeliveredEventPayload::DiscordEvent(ev) => {
                            self.process_discord_event(
                                prefix,
                                message_spec,
                                from,
                                ev,
                            )
                            .await?;
                        }
                        DeliveredEventPayload::Command(cmd) => {
                            self.process_command(prefix, message_spec, cmd)
                                .await?;
                        }
                    },
                    DeliveredEvent::ReactorMessage {
                        message_spec,
                        message,
                        ..
                    } => {
                        self.process_reactor_message(message_spec, message)
                            .await?
                    }
                }
            } else {
                return Err(DiscordError::InternalError);
            }
        }
    }
    async fn run_connection(&self) -> Result<Void, DiscordError> {
        self.client.lock().await.start().await?;
        Err(DiscordError::Disconnected)
    }
}

#[async_trait]
impl Endpoint for DiscordConnection {
    fn get_endpoint_id(&self) -> &str {
        &self.endpoint_id
    }

    async fn disconnected_since(&self) -> Option<Instant> {
        None
    }

    async fn run(
        &self,
        _forwarded_event_sender: &PanicSender<ForwardedEvent>,
    ) -> Result<Void, ReactorError> {
        const RECONNECT_PERIOD: u64 = 10;
        loop {
            let res = tokio::select! {
                res = self.run_connection() => res,
                res = self.recv_outer_events() => res
            };
            info!(
                "Discord endpoint {} disconnected: {:?}",
                self.endpoint_id,
                res.unwrap_err()
            );
            sleep(Duration::from_secs(RECONNECT_PERIOD)).await;
        }
    }
}

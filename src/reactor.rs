use crate::command::DispatchTable;
use crate::endpoint::{Endpoint, EndpointId, TypedEndpoint};
use crate::events::{
    DeliveredEvent, EventPayload, ForwardedEvent, DELIVERED_EVENT_TTL,
};
use crate::{config, discord};
use crate::{
    config::{Config, Connection, RoutingRule},
    irc_conn, rcon,
};
use maxminddb::Reader;
use sqlx::postgres::PgPool;
use std::collections::HashMap;
use std::sync::Arc;
use std::time::{Duration, Instant};
use thiserror::Error;
use tokio::sync::mpsc::error::SendError;
use tokio::sync::mpsc::{
    unbounded_channel, UnboundedReceiver, UnboundedSender,
};
use tokio::sync::Mutex;
use tracing::{error, info, trace, warn};

#[derive(Debug)]
pub enum Void {}

#[derive(Debug, Clone)]
pub struct PanicSender<T>(UnboundedSender<T>);

impl<T> PanicSender<T> {
    pub fn new(inner: UnboundedSender<T>) -> Self {
        Self(inner)
    }

    pub fn send(&self, thing: T) {
        match self.0.send(thing) {
            Ok(()) => {}
            Err(_) => panic!(
                "Error sending thing of type {} through an UnboundedSender!",
                std::any::type_name::<T>()
            ),
        }
    }
}

#[derive(Debug)]
pub struct Metrics {
    pub number_of_players: prometheus::IntGaugeVec,
    pub number_of_active_players: prometheus::IntGaugeVec,
    pub number_of_chat_messages: prometheus::IntCounterVec,
    pub xonotic_cpu: prometheus::GaugeVec,
    pub xonotic_lost: prometheus::GaugeVec,
    pub xonotic_offset_avg: prometheus::GaugeVec,
    pub xonotic_offset_max: prometheus::GaugeVec,
    pub xonotic_offset_sdev: prometheus::GaugeVec,
    pub number_of_records: prometheus::IntCounterVec,
    pub xonotic_maps: prometheus::IntGaugeVec,
}

/// Irrecoverable error which happens in one of endpoints or reactor itself.
/// If such error occurs it's better to output as much diagnostic info as possible
/// and stop the application.
#[derive(Debug, Error)]
pub enum ReactorError {
    #[error("Database Error: {0}")]
    DbError(#[from] sqlx::Error),
    #[error("Database migration error: {0}")]
    MigrateError(#[from] sqlx::migrate::MigrateError),
    #[error("Async error: {0}")]
    AsyncError(String),
    #[error("MMDB error: {0}")]
    MaxMindDBError(#[from] maxminddb::MaxMindDBError),
    #[error("Discord endpoint construction error: {0}")]
    DiscordError(#[from] discord::DiscordError),
    #[error("Misconfiguration")]
    ImproperlyConfigured(String),
}

impl<T> std::convert::From<SendError<T>> for ReactorError {
    fn from(_e: SendError<T>) -> ReactorError {
        ReactorError::AsyncError(format!(
            "Failed to send instance of type {} to an UnboundedChannel!",
            std::any::type_name::<T>()
        ))
    }
}

#[derive(Clone)]
pub struct EndpointRef {
    pub endpoint: Arc<TypedEndpoint>,
    pub config: Arc<config::Endpoint>,
    pub delivered_event_sender: UnboundedSender<DeliveredEvent>,
    pub routing_rules: Arc<Vec<RoutingRule>>,
}

#[derive(Debug)]
pub struct Globals {
    pub db_pool: PgPool,
    pub metrics: Metrics,
    pub geoip_city: Reader<Vec<u8>>,
    pub geoip_asn: Reader<Vec<u8>>,
}

pub struct Reactor {
    // this is the central part of the application
    // starts the endpoints
    // ensures the connections are alive
    // reconnects if needed
    // forwards events from one endpoint to another per routing rules
    // handles commands and replies
    receiver: Mutex<UnboundedReceiver<ForwardedEvent>>,
    sender: UnboundedSender<ForwardedEvent>,
    pub endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    pub globals: Arc<Globals>,
}

pub async fn can_send_to_endpoint<T: Endpoint>(endpoint: &T) -> bool {
    if let Some(disconnected_since) = endpoint.disconnected_since().await {
        disconnected_since.elapsed() < DELIVERED_EVENT_TTL * 2
    } else {
        true
    }
}

pub const WARMUP_TIME: Duration = Duration::from_secs(5);

impl Reactor {
    pub async fn new(
        config: Config,
        metrics: Metrics,
    ) -> Result<Self, ReactorError> {
        let db_pool = PgPool::connect(&config.db_url).await?;
        info!("Running DB migrations...");
        sqlx::migrate!().run(&db_pool).await?;
        info!("Database ready!");
        let geoip_city = Reader::open_readfile(&config.geodb_city_path)?;
        let geoip_asn = Reader::open_readfile(&config.geodb_asn_path)?;
        let mut endpoints = HashMap::new();
        let globals = Arc::new(Globals {
            db_pool,
            metrics,
            geoip_city,
            geoip_asn,
        });
        let (sender, receiver) = unbounded_channel();
        for (endpoint_id, endpoint_cfg) in config.endpoints.into_iter() {
            let (s, r) = unbounded_channel();
            let connection_cfg = endpoint_cfg.connection.clone();
            let routing_rules = endpoint_cfg.routing_rules.clone();
            let endpoint_cfg_clone = endpoint_cfg.clone();
            let endpoint = match connection_cfg {
                Connection::Xonotic {
                    addr,
                    secure,
                    password,
                    log_listen_addr,
                    public_addr,
                    logfile,
                    ..
                } => {
                    Arc::new(TypedEndpoint::Xonotic(rcon::RconConnection::new(
                        endpoint_id.clone(),
                        endpoint_cfg,
                        rcon::RconRemote::new(
                            addr,
                            rcon::RconSecure::from_u8(secure),
                            &password,
                        ),
                        log_listen_addr,
                        public_addr,
                        Mutex::new(r),
                        globals.clone(),
                        logfile,
                    )))
                }
                Connection::Irc {
                    server,
                    nickname,
                    realname,
                    channels,
                    quakenet_auth,
                } => {
                    Arc::new(TypedEndpoint::Irc(irc_conn::IrcConnection::new(
                        endpoint_id.clone(),
                        endpoint_cfg,
                        irc_conn::IrcConfig {
                            server,
                            nickname,
                            realname,
                            channels,
                            quakenet_auth,
                        },
                        Mutex::new(r),
                    )))
                }
                Connection::Discord {
                    token,
                    channels,
                    self_id,
                } => Arc::new(TypedEndpoint::Discord(
                    discord::DiscordConnection::new(
                        &endpoint_id,
                        endpoint_cfg,
                        &token,
                        channels,
                        self_id,
                        Mutex::new(r),
                        PanicSender(sender.clone()),
                    )
                    .await?,
                )),
            };
            info!(
                "Inserting endpoint {}, routing rules: {:?}",
                endpoint_id, routing_rules
            );
            endpoints.insert(
                endpoint_id.clone(),
                EndpointRef {
                    endpoint,
                    config: Arc::new(endpoint_cfg_clone),
                    delivered_event_sender: s,
                    routing_rules: Arc::new(routing_rules),
                },
            );
        }
        Ok(Self {
            sender,
            receiver: Mutex::new(receiver),
            endpoints: Arc::new(endpoints),
            globals,
        })
    }

    async fn run_endpoints(&self) -> Result<Void, ReactorError> {
        let mut futures = Vec::new();
        for i in self.endpoints.values() {
            let endpoint = i.endpoint.clone();
            let sender = PanicSender::new(self.sender.clone());
            if !i.config.disabled {
                info!("Starting endpoint: {}", endpoint.get_endpoint_id());
                futures.push(tokio::spawn(async move {
                    endpoint.run(&sender).await
                }));
            }
        }
        if futures.is_empty() {
            error!("No endpoints are enabled in config, have nothing to run!");
            Err(ReactorError::ImproperlyConfigured(String::from(
                "no endpoints",
            )))
        } else {
            let (res, _, _) = futures::future::select_all(futures).await;
            // unwrap_err should be safe here because it's Result<Void, _>
            Err(ReactorError::AsyncError(format!(
                "Error selecting future: {}",
                res.unwrap_err()
            )))
        }
    }

    async fn run_event_listener(&self) -> Result<Void, ReactorError> {
        let dispatcher = DispatchTable::default();
        let receiver = &mut self.receiver.lock().await;
        let start_ts = Instant::now();
        loop {
            let maybe_event = receiver.recv().await;
            if let Some(event) = maybe_event {
                if start_ts.elapsed() < WARMUP_TIME {
                    // do not forward events which occur during startup to avoid excess flood
                    continue;
                }
                if let Some(endpoint_ref) = self.endpoints.get(&event.from) {
                    if let EventPayload::Command(ref cmd) = event.payload {
                        info!(
                            "Received command {:?} on endpoint {}",
                            cmd,
                            endpoint_ref.endpoint.get_endpoint_id()
                        );
                        dispatcher
                            .run_command(
                                self.globals.db_pool.clone(),
                                endpoint_ref.clone(),
                                self.endpoints.clone(),
                                cmd.clone(),
                            )
                            .await;
                    }
                    for rule in endpoint_ref.routing_rules.iter() {
                        trace!(
                            "Applying routing rule {:?} to event {:?}",
                            rule.input,
                            event
                        );
                        if event.payload.matches_input(&rule.input) {
                            for output in rule.output.iter() {
                                for target_spec in &output.targets {
                                    info!(
                                        "Forwarding event {:?} to target {:?} with prefix {:?}",
                                        event, target_spec, output.prefix
                                    );
                                    if let Some(target_endpoint) = self
                                        .endpoints
                                        .get(&target_spec.endpoint_id)
                                    {
                                        if can_send_to_endpoint(
                                            target_endpoint.endpoint.as_ref(),
                                        )
                                        .await
                                        {
                                            let dev =
                                                event.clone().into_delivered(
                                                    target_spec
                                                        .message_spec
                                                        .clone(),
                                                    &rule.input.prefix,
                                                    &output.prefix,
                                                );
                                            target_endpoint
                                                .delivered_event_sender
                                                .send(dev)?;
                                        }
                                    } else {
                                        warn!(
                                            "Target endpoint {} not found!",
                                            target_spec.endpoint_id
                                        );
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return Err(ReactorError::ImproperlyConfigured(format!(
                        "No endpoint id: {}",
                        event.from
                    )));
                }
            } else {
                return Err(ReactorError::AsyncError(String::from(
                    "Forwarded event receiver disconnected",
                )));
            }
        }
    }

    pub async fn run(self) -> Result<Void, ReactorError> {
        tokio::select! {
            res1 = self.run_endpoints() => res1,
            res2 = self.run_event_listener() => res2
        }
    }
}

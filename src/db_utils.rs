use crate::geo::{Geoip2AsnData, Geoip2CityData};
use argon2::{Argon2, PasswordHash, PasswordVerifier};
use chrono::DateTime;
use dpcolors::DPString;
use rust_decimal::Decimal;
use serde::Deserialize;
use sqlx::{postgres::PgPool, types::ipnetwork::IpNetwork};
use std::collections::HashMap;
use std::net::IpAddr;
use tracing::warn;

pub enum MapVoteResult {
    PlayerDoesNotExist,
    NewVote,
    Update(i8, chrono::DateTime<chrono::Utc>, DPString),
}

pub async fn get_current_rating(
    pool: PgPool,
    endpoint_id: String,
    map: String,
) -> sqlx::Result<(i64, u64)> {
    let res = sqlx::query!(
        r#"SELECT count(*), sum(vote) FROM map_rating
           WHERE map=$1 AND endpoint_id=$2
           GROUP BY map"#,
        map,
        endpoint_id
    )
    .fetch_optional(&pool)
    .await?;
    Ok(match res {
        Some(r) => match (r.sum, r.count) {
            (Some(sum), Some(count)) => (sum, count as u64),
            _ => (0, 0),
        },
        None => (0, 0),
    })
}

pub async fn get_player_by_crypto_idfp<
    'a,
    E: sqlx::Executor<'a, Database = sqlx::Postgres>,
>(
    e: E,
    crypto_idfp: &str,
) -> sqlx::Result<Option<(i32, i32)>> {
    Ok(sqlx::query!(
        "select id, player_id from player_key WHERE crypto_idfp = $1",
        crypto_idfp
    )
    .fetch_optional(e)
    .await?
    .map(|r| (r.id, r.player_id)))
}

pub async fn store_map_vote(
    pool: PgPool,
    crypto_idfp: &str,
    endpoint_id: &str,
    map: String,
    vote: i8,
    comment: DPString,
) -> sqlx::Result<MapVoteResult> {
    let mut transaction = pool.begin().await?;
    let res = if let Some((_key_id, player_id)) =
        get_player_by_crypto_idfp(&mut *transaction, crypto_idfp).await?
    {
        if let Some(existing_vote) = sqlx::query!(
            r#"SELECT id,vote,created_at,comment FROM map_rating
               WHERE player_id=$1 AND map=$2 AND endpoint_id=$3"#,
            player_id,
            map,
            endpoint_id
        )
        .fetch_optional(&mut *transaction)
        .await?
        {
            sqlx::query!(
                    r#"UPDATE map_rating SET vote=$1, created_at=CURRENT_TIMESTAMP, comment=$2
                   WHERE id=$3"#,
                    vote as i16,
                    comment.to_none(),
                    existing_vote.id
                )
                .execute(&mut *transaction)
                .await?;
            MapVoteResult::Update(
                existing_vote.vote as i8,
                existing_vote.created_at,
                DPString::parse(&existing_vote.comment),
            )
        } else {
            sqlx::query!(
                r#"INSERT INTO map_rating
                   (player_id, endpoint_id, map, vote, comment, created_at)
                   VALUES ($1, $2, $3, $4, $5, CURRENT_TIMESTAMP)"#,
                player_id,
                endpoint_id,
                map,
                vote as i16,
                comment.to_none(),
            )
            .execute(&mut *transaction)
            .await?;
            MapVoteResult::NewVote
        }
    } else {
        MapVoteResult::PlayerDoesNotExist
    };
    transaction.commit().await?;
    Ok(res)
}

pub async fn store_player_identification(
    pool: PgPool,
    nickname: DPString,
    endpoint_id: String,
    ip_addr: IpAddr,
    crypto_idfp: Option<String>,
    stats_id: Option<u64>,
    city_data: Geoip2CityData,
    asn_data: Option<Geoip2AsnData>,
) -> sqlx::Result<Option<i32>> {
    let ip_addr =
        IpNetwork::new(ip_addr, if ip_addr.is_ipv4() { 32 } else { 128 })
            .unwrap();
    let mut transaction = pool.begin().await?;
    let (country, continent, city, subdivisions, location) = (
        city_data.country,
        city_data.continent,
        city_data.city,
        city_data.subdivisions,
        city_data.location,
    );
    let mut subdivision_names = String::new();
    let mut subdivision_codes = String::new();
    let mut first_iter = true;
    for i in subdivisions {
        if !first_iter {
            subdivision_codes.push_str("->");
            subdivision_names.push_str("->");
        } else {
            first_iter = false;
        }
        subdivision_codes.push_str(&i.0);
        subdivision_names.push_str(&i.1);
    }
    let maybe_player_id = if let Some(crypto_idfp) = crypto_idfp {
        // do we have already existing Player record for this player?
        let players = sqlx::query!(
            "select id, player_id from player_key WHERE crypto_idfp = $1",
            crypto_idfp
        )
        .fetch_all(&mut *transaction)
        .await?;
        let (player_id, key_id) = if players.is_empty() {
            let player_id = sqlx::query!(
                r#"INSERT INTO player (nickname, nickname_nocolors, created_at)
                   VALUES ($1, $2, CURRENT_TIMESTAMP)
                   RETURNING player.id"#,
                nickname.to_dp(),
                nickname.to_none()
            )
            .fetch_one(&mut *transaction)
            .await
            .unwrap();
            let key_id = sqlx::query!(
                r#"INSERT INTO player_key (player_id, crypto_idfp, created_at, last_used_at)
                   VALUES ($1, $2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
                   RETURNING player_key.id"#,
                player_id.id,
                crypto_idfp,
            )
            .fetch_one(&mut *transaction)
            .await?;
            (player_id.id, key_id.id)
        } else {
            sqlx::query!(
                r#"UPDATE player_key SET last_used_at=CURRENT_TIMESTAMP
                   WHERE id=$1"#,
                players[0].id
            )
            .execute(&mut *transaction)
            .await?;
            (players[0].player_id, players[0].id)
        };
        sqlx::query!(
            r#"INSERT INTO player_identification (player_key_id, endpoint_id, stats_id, nickname, nickname_nocolors, created_at, asn_id, asn_name, country, city, subdivision_codes, subdivision_names, continent, radius, latitude, longitude, ip_addr)
               VALUES ($1, $2, $3, $4, $5, CURRENT_TIMESTAMP, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16)"#,
            key_id,
            endpoint_id,
            stats_id.map(|x| x as i32),
            nickname.to_dp(),
            nickname.to_none(),
            asn_data.as_ref().map(|d| d.asn_id as i32),
            asn_data.map(|d| d.asn_name),
            country.map(|c| c.0),
            city,
            subdivision_codes,
            subdivision_names,
            continent.map(|c| c.0),
            location.map(|l| l.0 as i32),
            location.map(|l| l.1),
            location.map(|l| l.2),
            ip_addr
        ).execute(&mut *transaction).await?;
        Some(player_id)
    } else {
        sqlx::query!(
            r#"INSERT INTO player_identification (player_key_id, endpoint_id, stats_id, nickname, nickname_nocolors, created_at, asn_id, asn_name, country, city, subdivision_codes, subdivision_names, continent, radius, latitude, longitude, ip_addr)
               VALUES (NULL, $1, $2, $3, $4, CURRENT_TIMESTAMP, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)"#,
            endpoint_id,
            stats_id.map(|x| x as i32),
            nickname.to_dp(),
            nickname.to_none(),
            asn_data.as_ref().map(|d| d.asn_id as i32),
            asn_data.map(|d| d.asn_name),
            country.map(|c| c.0),
            city,
            subdivision_codes,
            subdivision_names,
            continent.map(|c| c.0),
            location.map(|l| l.0 as i32),
            location.map(|l| l.1),
            location.map(|l| l.2),
            ip_addr
        ).execute(&mut *transaction).await?;
        None
    };
    transaction.commit().await?;
    Ok(maybe_player_id)
}

pub async fn get_web_user(
    pool: &PgPool,
    id: &uuid::Uuid,
) -> sqlx::Result<Option<String>> {
    let user = sqlx::query!(
        r#"
    SELECT username FROM web_user WHERE id=$1"#,
        id
    )
    .fetch_optional(pool)
    .await?;
    Ok(user.map(|u| u.username))
}

pub async fn check_password(
    pool: &PgPool,
    username: &str,
    password: &str,
) -> sqlx::Result<Option<uuid::Uuid>> {
    let user = sqlx::query!(
        r#"
    SELECT id, username, password FROM web_user WHERE username=$1
    "#,
        username
    )
    .fetch_optional(pool)
    .await?;
    Ok(match user {
        Some(u) => match PasswordHash::new(&u.password) {
            Ok(h) => {
                let password_good = Argon2::default()
                    .verify_password(password.as_bytes(), &h)
                    .is_ok();
                if password_good {
                    Some(u.id)
                } else {
                    None
                }
            }
            _ => {
                warn!("Could not parse password hash of user {}", u.id);
                None
            }
        },
        None => None,
    })
}

pub async fn store_feedback(
    pool: &PgPool,
    player_id: Option<i32>,
    endpoint_id: &str,
    feedback_type: &str,
    message: &str,
) -> sqlx::Result<()> {
    sqlx::query!(
        r#"
    INSERT INTO feedback (player_id, endpoint_id, created_at, feedback_type, message)
    VALUES ($1, $2, CURRENT_TIMESTAMP, $3, $4)
    "#,
        player_id,
        endpoint_id,
        feedback_type,
        message
    )
    .execute(pool)
    .await?;
    Ok(())
}

pub async fn create_web_user(
    pool: &PgPool,
    id: &uuid::Uuid,
    username: &str,
    password_hash: &str,
    player_id: i64,
) -> sqlx::Result<()> {
    sqlx::query!(
        r#"
    INSERT INTO web_user (id, username, created_at, last_login_at, password, player_id)
    VALUES ($1, $2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, $3, $4)
    "#,
        id,
        username,
        password_hash,
        player_id as i64,
    )
    .execute(pool)
    .await?;
    Ok(())
}

struct IdentificationRow {
    endpoint_id: String,
    crypto_idfp: Option<String>,
    nickname: String,
    stats_id: Option<i32>,
    ip_addr: IpNetwork,
    asn_id: Option<i32>,
    asn_name: Option<String>,
    country: Option<String>,
    city: Option<String>,
    subdivision_names: Option<String>,
    created_at: DateTime<chrono::Utc>,
}

impl IdentificationRow {
    fn into_queried_identification(self) -> QueriedIdentification {
        QueriedIdentification {
            endpoint_id: self.endpoint_id,
            crypto_idfp: self.crypto_idfp,
            nickname: DPString::parse(&self.nickname),
            stats_id: self.stats_id,
            ip_addr: self.ip_addr.ip(),
            asn_id: self.asn_id,
            asn_name: self.asn_name,
            country: self.country,
            city: self.city,
            subdivisions: self.subdivision_names,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct QueriedIdentification {
    pub endpoint_id: String,
    pub crypto_idfp: Option<String>,
    pub nickname: DPString,
    pub stats_id: Option<i32>,
    pub ip_addr: IpAddr,
    pub asn_id: Option<i32>,
    pub asn_name: Option<String>,
    pub country: Option<String>,
    pub city: Option<String>,
    pub subdivisions: Option<String>,
}

#[derive(Deserialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum QueryType {
    Nickname,
    CryptoIdfp,
    StatsId,
    AsnId,
    AsnName,
    IpAddr,
    Country,
    City,
}

macro_rules! do_query_identifications {
    ($executor:ident, $q:ident, $predicate:tt) => {
        sqlx::query_as!(
            IdentificationRow,
        r#"
    SELECT player_identification.nickname,
           player_identification.stats_id,
           player_identification.ip_addr,
           player_key.crypto_idfp as "crypto_idfp?",
           player_identification.asn_id,
           player_identification.asn_name,
           player_identification.country,
           player_identification.city,
           player_identification.subdivision_names,
           player_identification.endpoint_id,
           player_identification.created_at
    FROM player_identification
    LEFT OUTER JOIN player_key ON player_key.id=player_identification.player_key_id
    WHERE "# + $predicate + " ORDER BY player_identification.created_at",
            $q,
        ).fetch_all($executor).await
    }
}

pub async fn query_identification(
    pool: &PgPool,
    query_type: QueryType,
    query: &str,
) -> sqlx::Result<Vec<(QueriedIdentification, Vec<DateTime<chrono::Utc>>)>> {
    let rows = match query_type {
        QueryType::Nickname => {
            do_query_identifications!(pool, query, "player_identification.nickname_nocolors ILIKE '%' || $1 || '%'")
        }
        QueryType::CryptoIdfp => do_query_identifications!(
            pool,
            query,
            "player_key.crypto_idfp ILIKE $1"
        ),
        QueryType::StatsId => do_query_identifications!(
            pool,
            query,
            "text(player_identification.stats_id) = $1"
        ),
        QueryType::AsnId => do_query_identifications!(
            pool,
            query,
            "text(player_identification.asn_id) = $1"
        ),
        QueryType::AsnName => do_query_identifications!(
            pool,
            query,
            "player_identification.asn_name ILIKE '%' || $1 || '%'"
        ),
        QueryType::Country => do_query_identifications!(
            pool,
            query,
            "player_identification.country ILIKE $1"
        ),
        QueryType::IpAddr => do_query_identifications!(
            pool,
            query,
            "player_identification.ip_addr = query_inet($1)"
        ),
        QueryType::City => do_query_identifications!(
            pool,
            query,
            "player_identification.city ILIKE '%' || $1 || '%'"
        ),
    }?;
    let mut res = HashMap::new();
    for row in rows.into_iter() {
        let created_at = row.created_at;
        let entry = res
            .entry(row.into_queried_identification())
            .or_insert_with(Vec::new);
        entry.push(created_at);
    }
    let mut res: Vec<_> = res.into_iter().collect();
    res.sort_by(|a, b| {
        let max_a = a.1.iter().max().unwrap();
        let max_b = b.1.iter().max().unwrap();
        max_b.cmp(max_a)
    });

    Ok(res)
}

pub async fn query_identifications_by_date(
    pool: &PgPool,
    endpoint_id: &str,
    start_time: DateTime<chrono::Utc>,
    end_time: DateTime<chrono::Utc>,
) -> sqlx::Result<Vec<(QueriedIdentification, Vec<DateTime<chrono::Utc>>)>> {
    let rows = sqlx::query_as!(
        IdentificationRow,
        r#"
    SELECT player_identification.nickname,
           player_identification.stats_id,
           player_identification.ip_addr,
           player_key.crypto_idfp as "crypto_idfp?",
           player_identification.asn_id,
           player_identification.asn_name,
           player_identification.country,
           player_identification.city,
           player_identification.subdivision_names,
           player_identification.endpoint_id,
           player_identification.created_at
    FROM player_identification
    INNER JOIN player_key ON player_key.id=player_identification.player_key_id
    WHERE
           player_identification.endpoint_id = $1 AND
           player_identification.created_at >= $2 AND
           player_identification.created_at < $3
    ORDER BY player_identification.created_at"#,
        endpoint_id,
        end_time,
        start_time
    )
    .fetch_all(pool)
    .await?;
    let mut res = HashMap::new();
    for row in rows.into_iter() {
        let created_at = row.created_at;
        let entry = res
            .entry(row.into_queried_identification())
            .or_insert_with(Vec::new);
        entry.push(created_at);
    }
    let mut res: Vec<_> = res.into_iter().collect();
    res.sort_by(|a, b| {
        let max_a = a.1.iter().max().unwrap();
        let max_b = b.1.iter().max().unwrap();
        max_b.cmp(max_a)
    });
    Ok(res)
}

pub async fn store_anon_cts_record(
    pool: PgPool,
    endpoint_id: &str,
    map: &str,
    nickname: DPString,
    time: String,
) -> sqlx::Result<()> {
    sqlx::query!(
        r#"
        INSERT INTO cts_record_anon
        (endpoint_id, nickname, nickname_nocolors, created_at, time, map)
        VALUES ($1, $2, $3, CURRENT_TIMESTAMP, $4, $5)
        "#,
        endpoint_id,
        nickname.to_dp(),
        nickname.to_none(),
        time,
        map
    )
    .execute(&pool)
    .await?;
    Ok(())
}

pub async fn store_cts_record(
    pool: PgPool,
    endpoint_id: &str,
    map: &str,
    crypto_idfp: Option<&str>,
    nickname: DPString,
    time: Decimal,
    ip_addr: IpAddr,
) -> sqlx::Result<()> {
    let key_id = match crypto_idfp {
        Some(c) => get_player_by_crypto_idfp(&pool, c)
            .await?
            .map(|(key_id, _)| key_id),
        None => None,
    };
    let ip_addr =
        IpNetwork::new(ip_addr, if ip_addr.is_ipv4() { 32 } else { 128 })
            .unwrap();
    sqlx::query!(
        r#"
        INSERT INTO cts_record
        (endpoint_id, created_at, player_key_id, nickname, nickname_nocolors, time, map, ip_addr)
        VALUES ($1, CURRENT_TIMESTAMP, $2, $3, $4, $5, $6, $7)
        "#,
        endpoint_id,
        key_id,
        nickname.to_dp(),
        nickname.to_none(),
        time,
        map,
        ip_addr
    ).execute(&pool).await?;
    // insert the row
    Ok(())
}

#[derive(Debug)]
pub struct CtsRecord {
    pub endpoint_id: String,
    pub nickname: DPString,
    pub created_at: DateTime<chrono::Utc>,
    pub crypto_idfp: Option<String>,
    pub time: Decimal,
    pub map: String,
    pub ip_addr: IpNetwork,
}

pub async fn query_cts_records(
    pool: &PgPool,
    start_time: DateTime<chrono::Utc>,
    end_time: DateTime<chrono::Utc>,
) -> sqlx::Result<Vec<CtsRecord>> {
    let rows = sqlx::query!(
        r#"
        SELECT endpoint_id, cts_record.created_at, player_key.crypto_idfp as "crypto_idfp?", nickname, time, map, ip_addr
        FROM cts_record LEFT OUTER JOIN player_key ON cts_record.player_key_id=player_key.id
        WHERE cts_record.created_at < $1 AND
              cts_record.created_at >= $2
        ORDER BY cts_record.created_at DESC
        "#,
        start_time,
        end_time,
    ).fetch_all(pool).await?;
    let rows = rows
        .into_iter()
        .map(|r| CtsRecord {
            endpoint_id: r.endpoint_id,
            nickname: DPString::parse(&r.nickname),
            created_at: r.created_at,
            crypto_idfp: r.crypto_idfp,
            time: r.time,
            map: r.map,
            ip_addr: r.ip_addr,
        })
        .collect();
    Ok(rows)
}

#[derive(Debug)]
pub struct AnonCtsRecord {
    pub endpoint_id: String,
    pub nickname: DPString,
    pub created_at: DateTime<chrono::Utc>,
    pub time: DPString,
    pub map: String,
}

pub async fn query_anon_cts_records(
    pool: &PgPool,
    start_time: DateTime<chrono::Utc>,
    end_time: DateTime<chrono::Utc>,
) -> sqlx::Result<Vec<AnonCtsRecord>> {
    let rows = sqlx::query!(
        r#"
        SELECT endpoint_id, created_at, nickname, time, map
        FROM cts_record_anon
        WHERE created_at < $1 AND
              created_at >= $2
        ORDER BY created_at DESC
        "#,
        start_time,
        end_time
    )
    .fetch_all(pool)
    .await?;
    let rows = rows
        .into_iter()
        .map(|r| AnonCtsRecord {
            endpoint_id: r.endpoint_id,
            created_at: r.created_at,
            nickname: DPString::parse(&r.nickname),
            time: DPString::parse(&r.time),
            map: r.map,
        })
        .collect();
    Ok(rows)
}

use crate::command::{private_reply, Command, CommandState};
use crate::db_utils;
use crate::endpoint::{Endpoint, EndpointId, TypedEndpoint};
use crate::reactor::EndpointRef;
use dpcolors::DPString;
use std::collections::HashMap;
use std::future::Future;
use std::pin::Pin;
use std::sync::Arc;
use tracing::warn;

async fn map_rating_inner(
    endpoint: EndpointRef,
    _endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Option<CommandState> {
    let vote: i8 = match cmd.cmd.as_str() {
        "+++" => 3,
        "++" => 2,
        "+" => 1,
        "-" => -1,
        "--" => -2,
        "---" => -3,
        _ => {
            warn!(
                "Got invalid map vote command: {}. This should never happen please fix the bug!",
                cmd.cmd
            );
            return None;
        }
    };

    let endpoint_id = endpoint.endpoint.get_endpoint_id();

    match endpoint.endpoint.as_ref() {
        TypedEndpoint::Xonotic(e) => {
            if let Some(player) =
                e.players.read().await.get_by_entity_id(cmd.user as u32)
            {
                if let Some(ref crypto_idfp) = player.crypto_idfp {
                    let pool = e.globals.db_pool.clone();
                    let map = e.get_current_map().await;
                    db_utils::store_map_vote(
                        pool,
                        crypto_idfp,
                        endpoint_id,
                        map,
                        vote,
                        cmd.args,
                    )
                    .await
                    .unwrap(); // lets panic on db errors for now
                    private_reply(
                        endpoint.clone(),
                        cmd.user,
                        DPString::parse(
                            "Thanks for voting, your opinion is highly appreciated",
                        ),
                    )
                    .await;
                } else {
                    private_reply(
                    endpoint.clone(),
                    cmd.user,
                    DPString::parse("Sorry, votes from non-authenticated users aren't accepted. Please install xonotic with crypto support and enable stats tracking at least for 1 game.")).await
                }
            } else {
                warn!(
                    "Player with entity id {} not found at endpoint {}",
                    cmd.user,
                    endpoint.endpoint.get_endpoint_id()
                )
            }
        }
        _ => {
            warn!(
                "Got map rating command from non-xonotic endpoint! Please fix the bug!"
            )
        }
    }
    None
}

pub fn map_rating(
    _pool: sqlx::PgPool,
    _state: CommandState,
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Pin<Box<dyn Future<Output = Option<CommandState>> + Send>> {
    Box::pin(map_rating_inner(endpoint, endpoints, cmd))
}

use crate::command;
use crate::command::Command;
use crate::command::CommandUserId;
use crate::config::RoutingRuleInput;
use crate::discord;
use crate::endpoint::EndpointId;
use crate::irc_conn;
use crate::rcon;
use std::str::FromStr;
use std::time::Duration;
use std::time::Instant;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum MessageSpec {
    None,
    Channel(String),
    Private(CommandUserId),
    PrivateUsername(String),
}

#[derive(Debug, Error)]
pub enum MessageSpecParseError {
    #[error("Invalid message spec: {0}")]
    InvalidSpec(String),
}

impl FromStr for MessageSpec {
    type Err = MessageSpecParseError;
    fn from_str(s: &str) -> Result<MessageSpec, Self::Err> {
        if let Some(s) = s.strip_prefix('#') {
            Ok(MessageSpec::Channel(String::from(s)))
        } else if let Some(s) = s.strip_prefix('@') {
            Ok(MessageSpec::PrivateUsername(String::from(s)))
        } else if s.is_empty() {
            Ok(MessageSpec::None)
        } else {
            Err(MessageSpecParseError::InvalidSpec(format!(
                "Message spec must start from '#' or '@', got '{}'",
                s
            )))
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TargetSpec {
    pub endpoint_id: EndpointId,
    pub message_spec: MessageSpec,
}

#[derive(Debug, Error)]
pub enum TargetSpecParseError {
    #[error("Invalid target spec: {0}")]
    InvalidTargetSpec(String),
    #[error(transparent)]
    InvalidMessageSpec(MessageSpecParseError),
}

impl FromStr for TargetSpec {
    type Err = TargetSpecParseError;

    fn from_str(s: &str) -> Result<TargetSpec, Self::Err> {
        if let Some((endpoint_id, msg_spec)) = s.split_once('#') {
            Ok(TargetSpec {
                endpoint_id: String::from(endpoint_id),
                message_spec: MessageSpec::Channel(msg_spec.into()),
            })
        } else if let Some((endpoint_id, msg_spec)) = s.split_once('@') {
            Ok(TargetSpec {
                endpoint_id: String::from(endpoint_id),
                message_spec: MessageSpec::PrivateUsername(msg_spec.into()),
            })
        } else {
            Ok(TargetSpec {
                endpoint_id: s.into(),
                message_spec: MessageSpec::None,
            })
        }
    }
}

pub trait XagyEvent {
    fn matches_input(&self, inp: &RoutingRuleInput) -> bool;
}

#[derive(Debug, Clone)]
pub enum EventPayload {
    RconEvent(rcon::RconForwardedEvent),
    IrcEvent(irc_conn::IrcEvent),
    DiscordEvent(discord::DiscordEvent),
    Command(command::Command),
}

#[derive(Debug, Clone)]
pub enum DeliveredEventPayload {
    RconEvent(rcon::RconForwardedEvent),
    IrcEvent(irc_conn::IrcEvent),
    DiscordEvent(discord::DiscordEvent),
    Command(command::Command),
}

impl EventPayload {
    pub fn into_delivered_payload(
        self,
        in_prefix: &str,
    ) -> DeliveredEventPayload {
        match self {
            EventPayload::RconEvent(ev) => match ev {
                rcon::RconForwardedEvent::Chat {
                    player_name,
                    team,
                    message,
                } => DeliveredEventPayload::RconEvent(
                    rcon::RconForwardedEvent::Chat {
                        player_name,
                        team,
                        message: message.skip(in_prefix.len()),
                    },
                ),
                ev => DeliveredEventPayload::RconEvent(ev),
            },
            EventPayload::IrcEvent(ev) => match ev {
                irc_conn::IrcEvent::Message(chan, nick, msg) => {
                    DeliveredEventPayload::IrcEvent(
                        irc_conn::IrcEvent::Message(
                            chan,
                            nick,
                            msg.skip(in_prefix.len()),
                        ),
                    )
                }
                ev => DeliveredEventPayload::IrcEvent(ev),
            },
            EventPayload::DiscordEvent(ev) => match ev {
                discord::DiscordEvent::Message(spec, nick, msg) => {
                    DeliveredEventPayload::DiscordEvent(
                        discord::DiscordEvent::Message(
                            spec,
                            nick,
                            msg.skip(in_prefix.len()),
                        ),
                    )
                } // ev => DeliveredEventPayload::DiscordEvent(ev),
            },
            EventPayload::Command(cmd) => DeliveredEventPayload::Command(cmd),
        }
    }
}

impl EventPayload {
    pub fn matches_input(&self, inp: &RoutingRuleInput) -> bool {
        match self {
            EventPayload::RconEvent(e) => e.matches_input(inp),
            EventPayload::IrcEvent(e) => e.matches_input(inp),
            EventPayload::Command(cmd) => {
                Command::matches_input(&cmd.message_spec, inp)
            }
            EventPayload::DiscordEvent(e) => e.matches_input(inp),
        }
    }
}

#[derive(Debug, Clone)]
pub struct ForwardedEvent {
    pub from: EndpointId,
    pub payload: EventPayload,
}

#[derive(Debug, Clone)]
pub enum DeliveredEvent {
    EndpointEvent {
        from: EndpointId,
        message_spec: MessageSpec,
        payload: DeliveredEventPayload,
        prefix: String,
        timestamp: Instant,
    },
    ReactorMessage {
        message_spec: MessageSpec,
        message: dpcolors::DPString,
        timestamp: Instant,
    },
}

impl ForwardedEvent {
    pub fn new_rcon(from: EndpointId, ev: rcon::RconForwardedEvent) -> Self {
        Self {
            from,
            payload: EventPayload::RconEvent(ev),
        }
    }

    pub fn new_discord(from: EndpointId, ev: discord::DiscordEvent) -> Self {
        Self {
            from,
            payload: EventPayload::DiscordEvent(ev),
        }
    }

    pub fn new_command(from: EndpointId, ev: command::Command) -> Self {
        Self {
            from,
            payload: EventPayload::Command(ev),
        }
    }

    pub fn new_irc(from: EndpointId, ev: irc_conn::IrcEvent) -> Self {
        Self {
            from,
            payload: EventPayload::IrcEvent(ev),
        }
    }

    pub fn into_delivered(
        self,
        message_spec: MessageSpec,
        in_prefix: &str,
        out_prefix: &str,
    ) -> DeliveredEvent {
        let payload = self.payload.into_delivered_payload(in_prefix);
        DeliveredEvent::EndpointEvent {
            from: self.from,
            payload,
            prefix: String::from(out_prefix),
            message_spec,
            timestamp: Instant::now(),
        }
    }
}

pub const DELIVERED_EVENT_TTL: Duration = Duration::from_secs(5);

impl DeliveredEvent {
    pub fn validate_ttl(&self) -> bool {
        let timestamp = match self {
            DeliveredEvent::EndpointEvent { timestamp, .. } => timestamp,
            DeliveredEvent::ReactorMessage { timestamp, .. } => timestamp,
        };
        timestamp.elapsed() < DELIVERED_EVENT_TTL
    }
}

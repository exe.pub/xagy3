use crate::command::{private_reply, public_reply, Command, CommandState};
use crate::endpoint::{Endpoint, EndpointId, TypedEndpoint};
use crate::reactor::EndpointRef;
use dpcolors::DPString;
use std::collections::HashMap;
use std::fmt::Write as _;
use std::future::Future;
use std::pin::Pin;
use std::sync::Arc;

const MO: glob::MatchOptions = glob::MatchOptions {
    case_sensitive: false,
    require_literal_separator: false,
    require_literal_leading_dot: false,
};

async fn maps_inner(
    _state: CommandState,
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Option<CommandState> {
    let args = cmd.args.to_none();
    let (endpoint_ref, pattern) = if let Some((endpoint_id, pattern)) =
        args.split_once(' ')
    {
        if let Some(endpoint_ref) = endpoints.get(endpoint_id) {
            (endpoint_ref, pattern)
        } else {
            private_reply(
                endpoint,
                cmd.user,
                DPString::parse(&format!("{}: no such endpoint", endpoint_id)),
            )
            .await;
            return None;
        }
    } else {
        (&endpoint, args.as_str())
    };
    if let Ok(pat) = glob::Pattern::new(pattern) {
        if let TypedEndpoint::Xonotic(conn) = endpoint_ref.endpoint.as_ref() {
            if let Some(maps) = conn.state.read().await.cvars.get("g_maplist") {
                let mut maps_matched: Vec<_> = maps
                    .split(' ')
                    .filter(|i| pat.matches_with(i, MO))
                    .collect();
                maps_matched.sort();
                let msg = if maps_matched.is_empty() {
                    String::from("no maps like that :(")
                } else if maps_matched.len() <= 10 {
                    format!(
                        "{} maps: {}",
                        maps_matched.len(),
                        maps_matched.join(", ")
                    )
                } else {
                    format!(
                        "{} maps: {}",
                        maps_matched.len(),
                        maps_matched[..10].join(", ")
                    )
                };
                public_reply(
                    &cmd.message_spec,
                    endpoint.clone(),
                    endpoints.clone(),
                    DPString::parse(&msg),
                )
                .await;
                None
            } else {
                private_reply(
                    endpoint.clone(),
                    cmd.user,
                    DPString::parse("maplist not initialized, maybe try later"),
                )
                .await;
                None
            }
        } else {
            private_reply(
                endpoint,
                cmd.user,
                DPString::parse(
                    "maps command only works for xonotic endpoints",
                ),
            )
            .await;
            None
        }
    } else {
        private_reply(
            endpoint,
            cmd.user,
            DPString::parse(&format!("invalid glob pattern: {}", args)),
        )
        .await;
        None
    }
}

pub fn maps(
    _pool: sqlx::PgPool,
    state: CommandState,
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Pin<Box<dyn Future<Output = Option<CommandState>> + Send>> {
    Box::pin(maps_inner(state, endpoint, endpoints, cmd))
}

async fn who_inner(
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Option<CommandState> {
    if cmd.args.is_empty() {
        private_reply(
            endpoint,
            cmd.user,
            DPString::parse("usage: !who <endpoint_id>"),
        )
        .await;
        return None;
    }
    let endpoint_id = cmd.args.to_none();
    if let Some(endpoint_ref) = endpoints.get(&endpoint_id) {
        if let TypedEndpoint::Xonotic(ref conn) = endpoint_ref.endpoint.as_ref()
        {
            let msg = {
                let players = conn.players.read().await;
                let state = conn.state.read().await;
                let mut s = format!(
                    "{} ({}@{}): ",
                    conn.get_endpoint_id(),
                    state.game.mode.to_short_str(),
                    state.game.map
                );
                if players.is_empty() {
                    let _ = write!(s, "empty :(");
                } else {
                    for (_ent_id, player) in players.iter() {
                        let _ = write!(
                            s,
                            "{}^7; ",
                            player.name.to_dp(),
                            // player.slot_id,
                            // player.entity_id,
                            // ent_id
                        );
                    }
                }
                s
            };
            public_reply(
                &cmd.message_spec,
                endpoint,
                endpoints,
                DPString::parse(&msg),
            )
            .await;
        } else {
            private_reply(
                endpoint,
                cmd.user,
                DPString::parse("who only works for xonotic endpoints"),
            )
            .await;
        }
    } else {
        private_reply(
            endpoint,
            cmd.user,
            DPString::parse(&format!("{}: no such endpoint", endpoint_id)),
        )
        .await;
    }
    None
}

pub fn who(
    _pool: sqlx::PgPool,
    _state: CommandState,
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Pin<Box<dyn Future<Output = Option<CommandState>> + Send>> {
    Box::pin(who_inner(endpoint, endpoints, cmd))
}

use super::players::CryptoIdfp;
use super::state::ResourcesState;
use super::Player;
use crate::config::RoutingRuleInput;
use crate::events::XagyEvent;
use dpcolors::DPString;
use serde::{Deserialize, Deserializer};
use std::net::IpAddr;
use std::time::Instant;

pub type SlotId = u32;
pub type EntityId = u32;
pub type TeamId = u32;
pub type CvarName = String;
pub type CvarValue = String;
// pub type EntityField = String;
// pub type EntityFieldValue = String;
pub type MapName = String;

#[derive(Debug, Clone)]
pub struct StatusPlayer {
    pub updated_at: Instant,
    pub entity_id: EntityId,
    pub ip: IpAddr,
    pub port: u32,
    pub ping: i32,
    pub frags: i32,
    pub time: u32,
    pub pl: u32,
    pub name: DPString,
}

#[derive(Debug, Clone, Copy)]
pub enum Team {
    Red,
    Blue,
    Magenta,
    Yellow,
    Spec,
    Unknown,
}

impl Team {
    pub fn color_code(self) -> &'static str {
        match self {
            Team::Red => "^1",
            Team::Blue => "^4",
            Team::Magenta => "^6",
            Team::Yellow => "^3",
            Team::Spec => "^9",
            Team::Unknown => "^0",
        }
    }
    pub fn from_u32(val: u32) -> Self {
        match val {
            5 => Self::Red,
            14 => Self::Blue,
            13 => Self::Yellow,
            10 => Self::Magenta,
            _ => Self::Unknown,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Default)]
pub enum GameMode {
    Deathmatch,
    Duel,
    TeamDeathmatch,
    Cts,
    Ctf,
    FreezeTag,
    Domination,
    KeyHunt,
    Nexball,
    ClanArena,
    Rune,
    Overall,
    Assault,
    KeepAway,
    #[default]
    Unknown,
}

impl GameMode {
    pub fn to_long_str(self) -> &'static str {
        match self {
            GameMode::Deathmatch => "Deathmatch",
            GameMode::Duel => "Duel",
            GameMode::TeamDeathmatch => "Team Deathmatch",
            GameMode::Cts => "Complete the Stage",
            GameMode::Ctf => "Capture the Flag",
            GameMode::FreezeTag => "Freeze Tag",
            GameMode::KeyHunt => "Key Hunt",
            GameMode::Domination => "Domination",
            GameMode::Nexball => "Nexball",
            GameMode::ClanArena => "Clan Arena",
            GameMode::Rune => "Rune",
            GameMode::Assault => "Assault",
            GameMode::KeepAway => "Keep Away",
            GameMode::Unknown => "Unknown Game Mode",
            GameMode::Overall => "Overall",
        }
    }

    pub fn to_short_str(self) -> &'static str {
        match self {
            GameMode::Deathmatch => "dm",
            GameMode::Duel => "duel",
            GameMode::TeamDeathmatch => "tdm",
            GameMode::Cts => "cts",
            GameMode::Ctf => "ctf",
            GameMode::FreezeTag => "ft",
            GameMode::KeyHunt => "kh",
            GameMode::Domination => "dom",
            GameMode::Nexball => "nexball",
            GameMode::ClanArena => "ca",
            GameMode::Rune => "rune",
            GameMode::Assault => "as",
            GameMode::KeepAway => "ka",
            GameMode::Unknown => "unknown",
            GameMode::Overall => "overall",
        }
    }
}

impl From<&str> for GameMode {
    fn from(s: &str) -> GameMode {
        match s {
            "dm" => GameMode::Deathmatch,
            "duel" => GameMode::Duel,
            "tdm" => GameMode::TeamDeathmatch,
            "cts" => GameMode::Cts,
            "ctf" => GameMode::Ctf,
            "kh" => GameMode::KeyHunt,
            "nb" => GameMode::Nexball,
            "nexball" => GameMode::Nexball,
            "ca" => GameMode::ClanArena,
            "rune" => GameMode::Rune,
            "overall" => GameMode::Overall,
            "dom" => GameMode::Domination,
            "as" => GameMode::Assault,
            "ft" => GameMode::FreezeTag,
            "ka" => GameMode::KeepAway,
            _ => GameMode::Unknown,
        }
    }
}

impl<'de> Deserialize<'de> for GameMode {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        Ok(GameMode::from(s.as_str()))
    }
}

#[derive(Debug, Clone)]
pub enum RconEvent {
    Cvar {
        name: String,
        value: String,
    },
    StatusHost(String),
    StatusMap(String),
    StatusTiming(ResourcesState),
    StatusPlayer(StatusPlayer),
    StatusPlayers(u16, u16),
    Join {
        slot_id: SlotId,
        entity_id: EntityId,
        ip: IpAddr,
        name: DPString,
    },
    PlayerInfo {
        slot_id: SlotId,
        entity_id: EntityId,
        allow_uidtracking: bool,
        allow_uid2name: bool,
        allow_uidranking: bool,
        ip: IpAddr,
        crypto_idfp: Option<CryptoIdfp>,
        name: DPString,
    },
    Part {
        slot_id: SlotId,
    },
    Chat {
        slot_id: SlotId,
        message: String,
        team_id: Option<Team>,
    },
    GameStart {
        mode: GameMode,
        map: MapName,
    },
    XonstatsQueryComplete {
        entity_id: EntityId,
    },
    PlayerRemoved {
        player: Player,
    },
    NameChange {
        slot_id: SlotId,
        name: DPString,
    },
    VCall {
        slot_id: SlotId,
        vote: DPString,
    },
    VStop {
        slot_id: SlotId,
    },
    VResult {
        passed: bool,
        accepted: i16,
        rejected: i16,
        abstained: i16,
        didnt_vote: i16,
        mincount: i16,
    },
    RecordSet {
        pos: u16,
        entity_id: EntityId,
        slot_id: SlotId,
        time: f64,
    },
    AnonRecordSet {
        nickname: DPString,
        time: String,
    },
    MapRating {
        map: MapName,
        count: u64,
        sum: i64,
    },
}

#[derive(Debug, Clone)]
pub enum RconForwardedEvent {
    Chat {
        player_name: DPString,
        team: Option<Team>,
        message: DPString,
    },
    GameStart {
        mode: GameMode,
        map: MapName,
        players: (u16, u16),
    },
    ServerConnected {
        host: String,
        connect_url: String,
    },
    ServerDisconnected {
        host: String,
    },
    Join {
        mode: GameMode,
        player: Player,
        map: MapName,
        players: (u16, u16),
    },
    Part {
        player: Player,
        map: MapName,
        players: (u16, u16),
    },
    NameChange {
        old_name: DPString,
        new_name: DPString,
    },
    VCall {
        player_name: DPString,
        vote: DPString,
    },
    VStop {
        player_name: DPString,
        vote: DPString,
    },
    VResult {
        vote: DPString,
        passed: bool,
        accepted: i16,
        rejected: i16,
        abstained: i16,
        didnt_vote: i16,
        mincount: i16,
    },
    RecordSet {
        player_name: DPString,
        map: MapName,
        pos: u16,
        time: f64,
    },
}

impl XagyEvent for RconForwardedEvent {
    fn matches_input(&self, inp: &RoutingRuleInput) -> bool {
        match &self {
            RconForwardedEvent::Chat { message, .. } => {
                message.to_none().starts_with(&inp.prefix)
            }
            _ => inp.prefix.is_empty(),
        }
    }
}

use super::types::*;
use dpcolors::DPString;
use std::collections::HashMap;
use std::time::{Instant, SystemTime};

#[derive(Debug, Clone)]
pub struct ServerState {
    pub host: String,
    pub connected: bool,
    pub protocol: (u32, String),
    pub version: String,
    pub last_updated: Instant,
    pub resources: ResourcesState,
    pub game: GameState,
    pub cvars: Cvars,
}

impl Default for ServerState {
    fn default() -> Self {
        Self {
            host: String::from("unknown"),
            connected: false,
            last_updated: Instant::now(),
            protocol: (3504, String::from("DP7")),
            version: String::from("unknown"),
            resources: ResourcesState::default(),
            game: GameState::default(),
            cvars: Cvars::default(),
        }
    }
}

#[derive(Debug, Clone, Default, Copy)]
pub struct ResourcesState {
    pub cpu: f32,
    pub lost: f32,
    pub offset_avg: f32,
    pub offset_max: f32,
    pub offset_sdev: f32,
}

#[derive(Debug, Clone)]
pub struct GameState {
    pub mode: GameMode,
    pub map: MapName,
    pub players: (u16, u16),
    pub vote: Option<(SlotId, DPString)>,
}

impl Default for GameState {
    fn default() -> GameState {
        GameState {
            mode: Default::default(),
            map: String::from("_init"),
            players: (0, 0),
            vote: None,
        }
    }
}

#[derive(Debug, Clone, Default)]
pub struct Cvars {
    vars: HashMap<CvarName, (CvarValue, SystemTime)>,
}

impl Cvars {
    pub fn get(&self, cvar: &str) -> Option<&str> {
        self.vars.get(cvar).map(|(x, _)| x.as_str())
    }

    pub fn set(&mut self, cvar: String, value: String) {
        self.vars.insert(cvar, (value, SystemTime::now()));
    }

    pub fn clear(&mut self, cvar: &str) {
        self.vars.remove(cvar);
    }
}

mod base_parser;
mod cmd_parser;
pub mod commands;
mod log_parser;
mod players;
mod state;
mod types;

use self::base_parser::CombinedParser;
pub use self::players::*;
use self::state::ServerState;
pub use self::types::{RconEvent, RconForwardedEvent};
use crate::command::Command;
use crate::db_utils::{
    get_current_rating, store_anon_cts_record, store_cts_record,
};
use crate::discord::DiscordEvent;
use crate::endpoint::{ConnectionStatus, Endpoint, EndpointId};
use crate::events::MessageSpec;
use crate::events::{DeliveredEvent, DeliveredEventPayload, ForwardedEvent};
use crate::irc_conn::IrcEvent;
use crate::reactor::{Globals, PanicSender, ReactorError, Void, WARMUP_TIME};
use crate::{config, db_utils};
use async_trait::async_trait;
use bytes::{BufMut, BytesMut};
use cmd_parser::get_cmd_parser;
use dpcolors::DPString;
use hmac::{Hmac, Mac};
use log_parser::get_log_parser;
use md4::Md4;
use rust_decimal::Decimal;
use rust_decimal_macros::*;
use std::fmt::Write as _;
use std::net::SocketAddr;
use std::path::PathBuf;
use std::str::FromStr;
use std::sync::Arc;
use std::time::{Duration, Instant, SystemTime, UNIX_EPOCH};
use thiserror::Error;
use tokio::io::AsyncWriteExt;
use tokio::net::UdpSocket;
use tokio::sync::mpsc::{unbounded_channel, UnboundedReceiver};
use tokio::sync::{Mutex, RwLock};
use tokio::time::sleep;
use tracing::{info, trace, warn};

const MAX_PACKET_SIZE: usize = 1400;
const QUAKE_PACKET_HEADER: &[u8] = &[b'\xFF'; 4];
const RCON_RESPONSE_HEADER: &[u8] = b"\xFF\xFF\xFF\xFFn";
// const RCON_GETSTATUS: &[u8] = b"\xFF\xFF\xFF\xFFgetstatus";
// const RCON_GETINFO: &[u8] = b"\xFF\xFF\xFF\xFFgetinfo";
// const CHALLENGE_PACKET: &[u8] = b"\xFF\xFF\xFF\xFFgetchallenge";
// const CHALLENGE_RESPONSE_HEADER: &[u8] = b"\xFF\xFF\xFF\xFFchallenge ";

const STATUS_CHECK_PERIOD: u64 = 4;

struct Timestamp {
    secs: u64,
    subsecs: f64,
}

impl Timestamp {
    fn new(time_diff: u64) -> Self {
        let t = (SystemTime::now() + Duration::from_secs(time_diff))
            .duration_since(UNIX_EPOCH)
            .unwrap();
        Self {
            secs: t.as_secs(),
            subsecs: f64::from(t.subsec_nanos()) * 0.001 * 0.001 * 0.001,
        }
    }
}

type HmacMd4 = Hmac<Md4>;

fn hmac_md4(key: &[u8], msg: &[u8]) -> Vec<u8> {
    let mut mac = HmacMd4::new_from_slice(key).unwrap();
    mac.update(msg);
    let code = mac.finalize().into_bytes();
    code.to_vec()
}

#[derive(Error, Debug)]
pub enum RconError {
    #[error("io error")]
    IOError(#[from] std::io::Error),
    #[error("rcon server disconnected")]
    Disconnect,
    #[error("internal error")]
    InternalError,
}

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum RconSecure {
    None,
    Time,
    // Let's not support challenge atm, too much hassle
    // Challenge,
}

impl RconSecure {
    pub fn from_u8(val: u8) -> Self {
        match val {
            0 => RconSecure::None,
            1 => RconSecure::Time,
            // 2 => Ok(RconSecure::Challenge),
            _ => panic!("Invalid RconSecure value: {}", val),
        }
    }
}

#[derive(Clone, Debug)]
pub struct RconRemote {
    pub addr: SocketAddr,
    pub secure: RconSecure,
    pub password: String,
}

impl RconRemote {
    pub fn new(addr: SocketAddr, secure: RconSecure, password: &str) -> Self {
        Self {
            addr,
            secure,
            password: String::from(password),
        }
    }
}

struct RconOutgoingPacket<'a> {
    secure: RconSecure,
    command: &'a str,
    password: &'a str,
}

impl<'a> RconOutgoingPacket<'a> {
    fn nosecure_packet(&self) -> Vec<u8> {
        [
            QUAKE_PACKET_HEADER,
            self.password.as_bytes(),
            b" ",
            self.command.as_bytes(),
        ]
        .concat()
    }

    fn secure_time_packet(&self, time_diff: u64) -> Vec<u8> {
        let t = Timestamp::new(time_diff);

        let cmd = format!("{}.{:.6} {}", t.secs, t.subsecs, self.command)
            .into_bytes();
        let key = hmac_md4(self.password.as_bytes(), &cmd);
        [
            QUAKE_PACKET_HEADER,
            b"srcon HMAC-MD4 TIME ",
            &key,
            b" ",
            &cmd,
        ]
        .concat()
    }
    pub fn prepare(&'a self) -> Vec<u8> {
        match self.secure {
            RconSecure::None => self.nosecure_packet(),
            RconSecure::Time => self.secure_time_packet(0),
        }
    }
}

struct RconReceiver {
    socket: Arc<UdpSocket>,
    buffer: BytesMut,
    line_sender: PanicSender<String>,
    logfile: Option<PathBuf>,
}

impl RconReceiver {
    fn new(
        socket: Arc<UdpSocket>,
        line_sender: PanicSender<String>,
        logfile: Option<PathBuf>,
    ) -> Self {
        Self {
            socket,
            buffer: BytesMut::with_capacity(MAX_PACKET_SIZE * 5),
            line_sender,
            logfile,
        }
    }

    fn process_buffer(&mut self) {
        loop {
            let maybe_pos = self
                .buffer
                .iter()
                .enumerate()
                .find(|&(_, c)| *c == b'\n')
                .map(|(i, _)| i);
            if let Some(pos) = maybe_pos {
                let mut line = self.buffer.split_to(pos + 1);
                line.truncate(pos);
                let s = String::from_utf8_lossy(&line[..]).to_string();
                self.line_sender.send(s);
            } else {
                break;
            }
        }
    }

    async fn receive_data(&mut self) -> Result<Void, RconError> {
        let mut file = match self.logfile {
            Some(ref p) => Some(
                tokio::fs::OpenOptions::new()
                    .append(true)
                    .create(true)
                    .open(p)
                    .await?,
            ),
            None => None,
        };
        loop {
            let mut buf = [0u8; MAX_PACKET_SIZE];
            let n = self.socket.recv(&mut buf).await?;
            let header_len = RCON_RESPONSE_HEADER.len();
            if buf.starts_with(RCON_RESPONSE_HEADER) {
                if self.buffer.remaining_mut() < n - header_len {
                    self.buffer.reserve(n - header_len);
                }
                if let Some(ref mut f) = file {
                    f.write_all(&buf[header_len..n]).await?;
                }
                self.buffer.put(&buf[header_len..n]);
            } else {
                if self.buffer.remaining_mut() < n {
                    self.buffer.reserve(n);
                }
                if let Some(ref mut f) = file {
                    f.write_all(&buf[..n]).await?;
                }
                self.buffer.put(&buf[..n]);
            }
            self.process_buffer();
        }
    }
}

// #[derive(Debug)]
// enum RconChallenge {
//     None,
//     Pending,
//     Challenge(Vec<u8>)
// }

#[derive(Debug)]
struct RconSockets {
    cmd_socket: Arc<UdpSocket>,
    // log_socket: Arc<UdpSocket>,
    // log_listen_addr: SocketAddr,
}

#[derive(Debug)]
pub struct RconConnection {
    endpoint_id: EndpointId,
    endpoint_config: config::Endpoint,
    pub remote: RconRemote,
    log_listen_addr: SocketAddr,
    pub connect_url: String,
    pub state: Arc<RwLock<ServerState>>,
    pub players: Arc<RwLock<PlayerManager>>,
    outer_event_receiver: Mutex<UnboundedReceiver<DeliveredEvent>>,
    sockets: RwLock<ConnectionStatus<RconSockets>>,
    pub globals: Arc<Globals>,
    logfile: Option<PathBuf>,
    start_ts: RwLock<Instant>,
    // challenge: Mutex<RconChallenge>
}

impl RconConnection {
    pub fn new(
        endpoint_id: EndpointId,
        endpoint_config: config::Endpoint,
        remote: RconRemote,
        log_listen_addr: SocketAddr,
        connect_url: String,
        outer_event_receiver: Mutex<UnboundedReceiver<DeliveredEvent>>,
        globals: Arc<Globals>,
        logfile: Option<PathBuf>,
    ) -> Self {
        RconConnection {
            endpoint_id,
            endpoint_config,
            remote,
            log_listen_addr,
            connect_url,
            state: Default::default(),
            players: Default::default(),
            outer_event_receiver,
            sockets: Default::default(),
            globals,
            logfile,
            start_ts: RwLock::new(Instant::now()),
            // challenge: Mutex::new(Challenge::None)
        }
    }

    pub async fn send_raw(&self, data: &[u8]) -> Result<(), RconError> {
        if let ConnectionStatus::Connected(sockets) =
            &*self.sockets.write().await
        {
            sockets.cmd_socket.send(data).await?;
        } else {
            warn!(
                "Can't send to rcon at endpoint {}, not connected!",
                self.endpoint_id
            );
        }
        Ok(())
    }

    pub async fn send(&self, data: &str) -> Result<(), RconError> {
        let packet = RconOutgoingPacket {
            secure: self.remote.secure,
            command: data,
            password: &self.remote.password,
        };
        let bytes = packet.prepare();
        self.send_raw(&bytes).await?;
        Ok(())
    }

    async fn keepalive(
        &self,
        ts: Arc<Mutex<(Instant, Instant)>>,
    ) -> Result<Void, RconError> {
        loop {
            {
                let (cmd_ts, log_ts) = *ts.lock().await;
                let dur = Duration::from_secs(4 * STATUS_CHECK_PERIOD);
                if (cmd_ts.elapsed() > dur) || (log_ts.elapsed() > dur) {
                    info!(
                        "Haven't received any data from xonotic server {} for {}, {:?}, {:?}",
                        self.remote.addr,
                        4 * STATUS_CHECK_PERIOD,
                        cmd_ts.elapsed(),
                        log_ts.elapsed(),
                    );
                    return Err(RconError::Disconnect);
                }
            }
            if let Err(_err) = self.send("status 1").await {
                info!(
                    "Failed to send data to xonotic server {}, server must be disconnected",
                    self.remote.addr
                );
                return Err(RconError::Disconnect);
            };
            sleep(Duration::from_secs(STATUS_CHECK_PERIOD)).await;
        }
    }

    async fn handle_inner_event(
        &self,
        forwarded_event_sender: &PanicSender<ForwardedEvent>,
        ev: RconEvent,
        sender: PanicSender<RconEvent>,
    ) -> Result<(), RconError> {
        trace!(
            "Endpoint {} received inner event {:?}",
            self.endpoint_id,
            ev
        );
        match ev {
            RconEvent::Cvar { name, value } => {
                self.state.write().await.cvars.set(name, value);
            }
            RconEvent::StatusHost(host) => {
                let mut s = self.state.write().await;
                s.host = host;
                s.connected = true;
                // also do some stuff here which we should normally do once per status check
                s.last_updated = Instant::now();
                PlayerManager::cleanup_stale_players(
                    self.players.clone(),
                    sender,
                )
                .await;
            }
            RconEvent::StatusMap(map) => {
                let mut state = self.state.write().await;
                self.globals
                    .metrics
                    .xonotic_maps
                    .with_label_values(&[&self.endpoint_id, &state.game.map])
                    .set(0);
                let mgr = self.players.read().await;
                self.globals
                    .metrics
                    .xonotic_maps
                    .with_label_values(&[&self.endpoint_id, &map])
                    .set(mgr.count_active() as i64);
                state.game.map = map;
            }
            RconEvent::StatusTiming(timing) => {
                self.globals
                    .metrics
                    .xonotic_cpu
                    .with_label_values(&[&self.endpoint_id])
                    .set(timing.cpu as f64);
                self.globals
                    .metrics
                    .xonotic_lost
                    .with_label_values(&[&self.endpoint_id])
                    .set(timing.lost as f64);
                self.globals
                    .metrics
                    .xonotic_offset_avg
                    .with_label_values(&[&self.endpoint_id])
                    .set(timing.offset_avg as f64);
                self.globals
                    .metrics
                    .xonotic_offset_max
                    .with_label_values(&[&self.endpoint_id])
                    .set(timing.offset_max as f64);
                self.globals
                    .metrics
                    .xonotic_offset_sdev
                    .with_label_values(&[&self.endpoint_id])
                    .set(timing.offset_sdev as f64);
                self.state.write().await.resources = timing;
            }
            RconEvent::StatusPlayers(cur, max) => {
                self.state.write().await.game.players = (cur, max);
            }
            RconEvent::StatusPlayer(status_player) => {
                let ent_id = status_player.entity_id;
                let is_new = PlayerManager::handle_status_player(
                    self.players.clone(),
                    status_player,
                )
                .await;
                let mgr = self.players.read().await;
                self.globals
                    .metrics
                    .number_of_players
                    .with_label_values(&[&self.endpoint_id])
                    .set(mgr.count_total() as i64);

                self.globals
                    .metrics
                    .number_of_active_players
                    .with_label_values(&[&self.endpoint_id])
                    .set(mgr.count_active() as i64);
                if is_new {
                    self.send(&format!("sv_cmd printplayer {}", ent_id))
                        .await?;
                }
            }
            RconEvent::Join {
                slot_id,
                entity_id,
                ip,
                name,
            } => {
                let is_new = PlayerManager::handle_join(
                    self.players.clone(),
                    slot_id,
                    entity_id,
                    ip,
                    name,
                )
                .await;
                if is_new {
                    self.send(&format!("sv_cmd printplayer {}", entity_id))
                        .await?;
                }
            }
            RconEvent::PlayerInfo {
                slot_id,
                entity_id,
                allow_uidtracking,
                allow_uid2name,
                allow_uidranking,
                ip,
                crypto_idfp,
                name,
            } => {
                PlayerManager::handle_playerinfo(
                    self.players.clone(),
                    &self.globals.geoip_city,
                    &self.globals.geoip_asn,
                    sender.clone(),
                    slot_id,
                    entity_id,
                    allow_uidtracking,
                    allow_uid2name,
                    allow_uidranking,
                    ip,
                    crypto_idfp,
                    name,
                )
                .await;
            }
            RconEvent::Part { slot_id } => {
                PlayerManager::handle_part(
                    self.players.clone(),
                    slot_id,
                    sender,
                )
                .await;
            }
            RconEvent::Chat {
                slot_id,
                message,
                team_id,
            } => {
                self.globals
                    .metrics
                    .number_of_chat_messages
                    .with_label_values(&[self.endpoint_id.as_str()])
                    .inc();
                let players = &self.players.read().await;
                if let Some(player) = players.get_by_slot_id(slot_id) {
                    if let Some(cmd) = Command::maybe_from(
                        &self.endpoint_config.command_prefixes,
                        player.entity_id as usize,
                        player.name.clone(),
                        MessageSpec::None,
                        DPString::parse(&message),
                    ) {
                        forwarded_event_sender.send(
                            ForwardedEvent::new_command(
                                self.endpoint_id.clone(),
                                cmd,
                            ),
                        );
                    } else {
                        forwarded_event_sender.send(ForwardedEvent::new_rcon(
                            self.endpoint_id.clone(),
                            RconForwardedEvent::Chat {
                                player_name: player.name.clone(),
                                team: team_id,
                                message: DPString::parse(&message),
                            },
                        ));
                    }
                }
            }
            RconEvent::GameStart { mode, map } => {
                let state = &mut self.state.write().await;
                state.game.mode = mode;
                let players = state.game.players;
                forwarded_event_sender.send(ForwardedEvent::new_rcon(
                    self.endpoint_id.clone(),
                    RconForwardedEvent::GameStart {
                        mode,
                        map: map.clone(),
                        players: (
                            players.0, // self.players.read().await.len() as u16,
                            players.1,
                        ),
                    },
                ));
                // slot numbers are reset when a game start so we need to update this mapping
                self.players.write().await.clear_slot_to_entity();
                let pool = self.globals.db_pool.clone();
                let endpoint_id = self.endpoint_id.clone();
                let sender = sender.clone();
                tokio::spawn(async move {
                    sleep(Duration::from_secs(5)).await;
                    match get_current_rating(pool, endpoint_id, map.clone())
                        .await
                    {
                        Ok((sum, count)) => sender.send(RconEvent::MapRating {
                            map,
                            count,
                            sum,
                        }),
                        Err(e) => warn!("Error querying map_rating: {}", e),
                    }
                });
            }
            RconEvent::XonstatsQueryComplete { entity_id } => {
                if let Some(player) =
                    self.players.write().await.get_mut_by_entity_id(entity_id)
                {
                    let stats_id = match player.xonstats {
                        XonStatsQueryStatus::Received(_, ref d) => {
                            Some(d.player.player_id)
                        }
                        _ => None,
                    };
                    // TODO: we need to tokio::spawn to prevent keeping lock on self.players...
                    match db_utils::store_player_identification(
                        self.globals.db_pool.clone(),
                        player.name.clone(),
                        self.endpoint_id.clone(),
                        player.ip,
                        player.crypto_idfp.clone(),
                        stats_id,
                        player.city_data.clone(),
                        player.asn_data.clone(),
                    )
                    .await
                    {
                        Ok(db_id) => {
                            player.db_id = db_id;
                        }
                        Err(e) => warn!(
                            "Error during storing player identification: {}",
                            e
                        ),
                    }
                    let state = self.state.read().await;
                    let mode = state.game.mode;
                    let map = state.game.map.clone();
                    let players = state.game.players;
                    if self.start_ts.read().await.elapsed() > WARMUP_TIME {
                        let msg = format_join_msg(mode, player, &map, players);
                        self.public_message("", &msg).await?;
                    }
                    forwarded_event_sender.send(ForwardedEvent::new_rcon(
                        self.endpoint_id.clone(),
                        RconForwardedEvent::Join {
                            mode,
                            player: player.clone(),
                            map,
                            players: (
                                players.0, // self.players.read().await.len() as u16,
                                players.1,
                            ),
                        },
                    ));
                }
            }
            RconEvent::PlayerRemoved { player } => {
                let state = self.state.read().await;
                let map = state.game.map.clone();
                let players = state.game.players;
                forwarded_event_sender.send(ForwardedEvent::new_rcon(
                    self.endpoint_id.clone(),
                    RconForwardedEvent::Part {
                        player,
                        map,
                        players: (
                            players.0, // self.players.read().await.len() as u16,
                            players.1,
                        ),
                    },
                ))
            }
            RconEvent::NameChange { slot_id, name } => {
                let players = &mut self.players.write().await;
                if let Some(player) = players.get_mut_by_slot_id(slot_id) {
                    forwarded_event_sender.send(ForwardedEvent::new_rcon(
                        self.endpoint_id.clone(),
                        RconForwardedEvent::NameChange {
                            old_name: player.name.clone(),
                            new_name: name.clone(),
                        },
                    ));
                    player.name = name;
                }
            }
            RconEvent::VCall { slot_id, vote } => {
                let players = &self.players.read().await;
                self.state.write().await.game.vote =
                    Some((slot_id, vote.clone()));
                if let Some(player) = players.get_by_slot_id(slot_id) {
                    forwarded_event_sender.send(ForwardedEvent::new_rcon(
                        self.endpoint_id.clone(),
                        RconForwardedEvent::VCall {
                            player_name: player.name.clone(),
                            vote,
                        },
                    ));
                }
            }
            RconEvent::VStop { slot_id } => {
                let players = &self.players.read().await;
                let state = &mut self.state.write().await;
                let vote = if let Some(ref vote) = state.game.vote {
                    vote.1.clone()
                } else {
                    DPString::parse("")
                };
                if let Some(player) = players.get_by_slot_id(slot_id) {
                    forwarded_event_sender.send(ForwardedEvent::new_rcon(
                        self.endpoint_id.clone(),
                        RconForwardedEvent::VStop {
                            player_name: player.name.clone(),
                            vote,
                        },
                    ));
                }
                state.game.vote = None;
            }
            RconEvent::VResult {
                passed,
                accepted,
                rejected,
                abstained,
                didnt_vote,
                mincount,
            } => {
                let state = &mut self.state.write().await;
                let vote = if let Some(ref vote) = state.game.vote {
                    vote.1.clone()
                } else {
                    DPString::parse("")
                };
                forwarded_event_sender.send(ForwardedEvent::new_rcon(
                    self.endpoint_id.clone(),
                    RconForwardedEvent::VResult {
                        vote,
                        passed,
                        accepted,
                        rejected,
                        abstained,
                        didnt_vote,
                        mincount,
                    },
                ));
                state.game.vote = None;
            }
            RconEvent::RecordSet {
                pos,
                entity_id,
                time,
                ..
            } => {
                let pos_str = format!("{pos}");
                self.globals
                    .metrics
                    .number_of_records
                    .with_label_values(&[&self.endpoint_id, &pos_str])
                    .inc();
                let players = &self.players.read().await;
                if let Some(player) = players.get_by_entity_id(entity_id) {
                    let state = self.state.read().await;
                    let map = state.game.map.clone();
                    if pos <= 3 {
                        let msg =
                            format_recordset(&player.name, &map, pos, time);
                        self.public_message("", &msg).await?;
                    }
                    forwarded_event_sender.send(ForwardedEvent::new_rcon(
                        self.endpoint_id.clone(),
                        RconForwardedEvent::RecordSet {
                            player_name: player.name.clone(),
                            map: map.clone(),
                            pos,
                            time,
                        },
                    ));
                    let pool = self.globals.db_pool.clone();
                    let endpoint_id = self.endpoint_id.clone();
                    let crypto_idfp = player.crypto_idfp.clone();
                    let nickname = player.name.clone();
                    let ip_addr = player.ip;
                    tokio::spawn(async move {
                        if let Err(e) = store_cts_record(
                            pool,
                            &endpoint_id,
                            &map,
                            crypto_idfp.as_deref(),
                            nickname,
                            f64_to_decimal(time, 2),
                            ip_addr,
                        )
                        .await
                        {
                            warn!("Error during storing cts record: {}", e);
                        }
                    });
                }
            }
            RconEvent::AnonRecordSet { nickname, time } => {
                let pool = self.globals.db_pool.clone();
                let endpoint_id = self.endpoint_id.clone();
                let state = self.state.read().await;
                let map = state.game.map.clone();
                tokio::spawn(async move {
                    if let Err(e) = store_anon_cts_record(
                        pool,
                        &endpoint_id,
                        &map,
                        nickname,
                        time,
                    )
                    .await
                    {
                        warn!("Error when storing anon cts record: {}", e);
                    }
                });
            }
            RconEvent::MapRating { map, count, sum } => {
                let msg = if count == 0 {
                    format!("^3{map} ^7has not yet been rated - Use ^7/^2+++^7,^2++^7,^2+^7,^1-^7,^1--^7,^1--- ^7to rate the map.")
                } else {
                    let sum_clamped = sum.clamp(-30, 30);
                    let g = (sum_clamped + 30) / 4;
                    let r = 15 - g;
                    let color = format!("^x{r:X}{g:X}0");
                    format!("^3{map} ^7has {color}{sum} points. ^5[{count} votes]^7 - Use ^7/^2+++^7,^2++^7,^2+^7,^1-^7,^1--^7,^1--- ^7to rate the map.")
                };
                self.public_message("", &msg).await?;
            }
        }
        Ok(())
    }

    async fn recv_inner_events(
        &self,
        forwarded_event_sender: &PanicSender<ForwardedEvent>,
        mut cmd_line_receiver: UnboundedReceiver<String>,
        mut log_line_receiver: UnboundedReceiver<String>,
        mut cmd_parser: CombinedParser,
        mut log_parser: CombinedParser,
        ts: Arc<Mutex<(Instant, Instant)>>,
    ) -> Result<Void, RconError> {
        let (sender, mut receiver) = unbounded_channel();
        let sender = PanicSender::new(sender);
        loop {
            tokio::select! {
                maybe_cmd_line = cmd_line_receiver.recv() => {
                    {
                        ts.lock().await.0 = Instant::now();
                    }
                    if let Some(line) = maybe_cmd_line {
                        cmd_parser.parse(&line, &sender);
                    } else {
                        return Err(RconError::InternalError)
                    }
                },
                maybe_log_line = log_line_receiver.recv() => {
                    {
                        ts.lock().await.1 = Instant::now();
                    }
                    if let Some(line) = maybe_log_line {
                        log_parser.parse(&line, &sender);
                    } else {
                        return Err(RconError::InternalError)
                    }
                },
                maybe_event = receiver.recv() => {
                    if let Some(ev) = maybe_event {
                        self.handle_inner_event(forwarded_event_sender, ev, sender.clone()).await?;
                    } else {
                        return Err(RconError::InternalError)
                    }
                }
            }
        }
    }

    async fn public_message(
        &self,
        prefix: &str,
        message: &str,
    ) -> Result<(), RconError> {
        self.send(&format!("sv_cmd ircmsg ^5{}^7{}", prefix, message))
            .await
    }

    async fn process_rcon_event(
        &self,
        prefix: String,
        _from: String,
        ev: RconForwardedEvent,
    ) -> Result<(), RconError> {
        match ev {
            RconForwardedEvent::Chat {
                player_name,
                team,
                message,
            } => {
                let msg = match team {
                    Some(team) => format!(
                        "{}(^7{}{})^7: {}",
                        team.color_code(),
                        player_name.to_dp(),
                        team.color_code(),
                        message.to_dp()
                    ),
                    None => format!(
                        "{}^7: {}",
                        player_name.to_dp(),
                        message.to_dp()
                    ),
                };
                self.public_message(&prefix, &msg).await?;
            }
            RconForwardedEvent::ServerConnected { host, connect_url } => {
                let msg = format!(
                    "^2Server connected^7: ^3{}^7 connect at: ^3{}^7",
                    host, connect_url
                );
                self.public_message(&prefix, &msg).await?;
            }
            RconForwardedEvent::ServerDisconnected { host } => {
                let msg = format!("^1Server disconnected^7: ^3{}^7", host);
                self.public_message(&prefix, &msg).await?;
            }
            RconForwardedEvent::GameStart { mode, map, players } => {
                if players.0 > 0 {
                    let msg = format!(
                        "^2Playing ^5{}^7 on ^2{}^7 [{}/{}]",
                        mode.to_long_str(),
                        map,
                        players.0,
                        players.1
                    );
                    self.public_message(&prefix, &msg).await?;
                }
            }
            RconForwardedEvent::Join {
                mode,
                player,
                map,
                players,
            } => {
                let msg = format_join_msg(mode, &player, &map, players);
                self.public_message(&prefix, &msg).await?;
            }
            RconForwardedEvent::Part {
                player,
                map,
                players,
            } => {
                // ^1-part:^7 %(name)s
                let mut msg = format!("^1 -part:^7 {}", player.name.to_dp());
                if let Some(ref country) = player.city_data.country {
                    let _ = write!(msg, " ^2{}^7", country.1);
                }
                let _ =
                    write!(msg, " ^3{} ^5[{}/{}]^7", map, players.0, players.1);
                self.public_message(&prefix, &msg).await?;
            }
            RconForwardedEvent::NameChange { old_name, new_name } => {
                let msg = format!(
                    "^5* {} ^7is now known as {}",
                    old_name.to_dp(),
                    new_name.to_dp()
                );
                self.public_message(&prefix, &msg).await?;
            }
            RconForwardedEvent::VCall { player_name, vote } => {
                let msg = format!(
                    "^5* ^7{} ^7calls a vote for ^1{}",
                    player_name.to_dp(),
                    vote.to_dp()
                );
                self.public_message(&prefix, &msg).await?;
            }
            RconForwardedEvent::VStop { player_name, vote } => {
                let msg = format!(
                    "^5* {} ^7stopped {}'s ^7vote for ^1{}",
                    player_name.to_dp(),
                    player_name.to_dp(),
                    vote.to_dp()
                );
                self.public_message(&prefix, &msg).await?;
            }
            RconForwardedEvent::VResult {
                vote,
                passed,
                accepted,
                rejected,
                ..
            } => {
                let msg = if passed {
                    format!(
                        "^5*^7 vote for ^3{}^7 ^2passed^7: {}:{}",
                        vote.to_dp(),
                        accepted,
                        rejected
                    )
                } else {
                    format!(
                        "^5*^7 vote for ^3{}^7 ^1rejected^7: {}:{}",
                        vote.to_dp(),
                        accepted,
                        rejected
                    )
                };
                self.public_message(&prefix, &msg).await?;
            }
            RconForwardedEvent::RecordSet {
                player_name,
                map,
                pos,
                time,
            } => {
                if pos <= 3 {
                    let msg = format_recordset(&player_name, &map, pos, time);
                    self.public_message(&prefix, &msg).await?;
                }
            }
        }
        Ok(())
    }

    async fn process_irc_event(
        &self,
        prefix: String,
        _from: String,
        ev: IrcEvent,
    ) -> Result<(), RconError> {
        match ev {
            IrcEvent::Message(_chan, nick, msg) => {
                self.send(&format!(
                    "sv_cmd ircmsg ^5{}^3{}:^7 {}",
                    prefix,
                    nick,
                    msg.to_dp()
                ))
                .await?;
            }
            IrcEvent::Action(_chan, nick, msg) => {
                self.send(&format!(
                    "sv_cmd ircmsg ^5{} ^1* ^2{} {}",
                    prefix,
                    nick,
                    msg.to_dp()
                ))
                .await?;
            }
            _ => {}
        }
        Ok(())
    }

    async fn process_discord_event(
        &self,
        prefix: String,
        _from: String,
        ev: DiscordEvent,
    ) -> Result<(), RconError> {
        match ev {
            DiscordEvent::Message(_spec, nick, message) => {
                for line in message.to_dp().replace("\r\n", "\n").split('\n') {
                    if !line.trim().is_empty() {
                        let msg = format!(
                            "sv_cmd ircmsg ^5{prefix}^3{nick}^7: {}",
                            line
                        );
                        self.send(&msg).await?;
                    }
                }
            }
        }
        Ok(())
    }

    async fn process_reactor_message(
        &self,
        message_spec: MessageSpec,
        message: DPString,
    ) -> Result<(), RconError> {
        match message_spec {
            MessageSpec::None => {
                self.send(&format!(
                    "sv_cmd ircmsg ^1BOT^7: {}",
                    message.to_dp()
                ))
                .await?;
            }
            MessageSpec::Private(user_id) => {
                self.send(&format!("tell #{} {}", user_id, message.to_dp()))
                    .await?;
            }
            _ => {
                warn!(
                    "Xonotic endpoint can't handle target spec {:?}",
                    message_spec
                );
            }
        }
        Ok(())
    }

    async fn process_command(
        &self,
        prefix: String,
        _from: EndpointId,
        cmd: Command,
    ) -> Result<(), RconError> {
        if let Some(cmd_prefix) = self.endpoint_config.command_prefixes.get(0) {
            self.send(&format!(
                "sv_cmd ircmsg ^5{}^3{}: ^7{}{} {}",
                prefix,
                cmd.nickname.to_dp(),
                cmd_prefix,
                cmd.cmd,
                cmd.args.to_dp(),
            ))
            .await?;
        }
        Ok(())
    }

    async fn recv_outer_events(&self) -> Result<Void, RconError> {
        let mut receiver = self.outer_event_receiver.lock().await;
        loop {
            if let Some(ev) = receiver.recv().await {
                if !ev.validate_ttl() {
                    warn!(
                        "Discarding old event at endpoint {}",
                        self.endpoint_id
                    );
                    continue;
                }
                info!("Endpoint {} received event {:?}", self.endpoint_id, ev);
                match ev {
                    DeliveredEvent::EndpointEvent {
                        from,
                        payload,
                        prefix,
                        ..
                    } => match payload {
                        DeliveredEventPayload::RconEvent(re) => {
                            self.process_rcon_event(prefix, from, re).await?
                        }
                        DeliveredEventPayload::IrcEvent(ie) => {
                            self.process_irc_event(prefix, from, ie).await?
                        }
                        DeliveredEventPayload::DiscordEvent(ev) => {
                            self.process_discord_event(prefix, from, ev).await?
                        }
                        DeliveredEventPayload::Command(cmd) => {
                            self.process_command(prefix, from, cmd).await?
                        }
                    },
                    DeliveredEvent::ReactorMessage {
                        message_spec,
                        message,
                        ..
                    } => {
                        self.process_reactor_message(message_spec, message)
                            .await?
                    }
                }
            } else {
                return Err(RconError::InternalError);
            }
        }
    }

    pub async fn get_current_map(&self) -> String {
        let state = self.state.read().await;
        state.game.map.clone()
    }

    async fn cleanup_log_dest_udp(
        &self,
        current: SocketAddr,
    ) -> Result<Void, RconError> {
        loop {
            {
                let state = &mut self.state.write().await;
                state.cvars.clear("log_dest_udp");
            }
            self.send("log_dest_udp").await?;
            sleep(Duration::from_secs(1)).await;
            let state = self.state.write().await;
            if let Some(val) = state.cvars.get("log_dest_udp") {
                for addr_raw in val.split(' ') {
                    if let Ok(addr) = addr_raw.parse::<SocketAddr>() {
                        if (current.ip() == addr.ip())
                            && (current.port() != addr.port())
                        {
                            self.send(&format!(
                                "sv_cmd removefromlist log_dest_udp {}",
                                addr_raw
                            ))
                            .await?;
                        }
                    }
                }
                break;
            }
        }
        loop {
            // we need to run it forever so select! in the main function works
            // should be a better solution tho. In an ideal world at least...
            sleep(Duration::from_secs(100000000)).await;
        }
    }

    async fn run_connection(
        &self,
        event_sender: &PanicSender<ForwardedEvent>,
    ) -> Result<Void, RconError> {
        *self.state.write().await = Default::default();
        *self.players.write().await = Default::default();
        let cmd_socket = Arc::new(UdpSocket::bind("0.0.0.0:0").await?);
        cmd_socket.connect(&self.remote.addr).await?;
        let log_socket = Arc::new(UdpSocket::bind(self.log_listen_addr).await?);
        log_socket.connect(&self.remote.addr).await?;
        let log_listen_addr = log_socket.local_addr()?;
        {
            *self.sockets.write().await =
                ConnectionStatus::Connected(RconSockets {
                    // log_socket: log_socket.clone(),
                    cmd_socket: cmd_socket.clone(),
                    // log_listen_addr,
                });
        }
        let endpoint_id = self.endpoint_id.clone();
        let conn_url = self.connect_url.clone();
        let state = self.state.clone();
        let sender_clone = (*event_sender).clone();
        tokio::spawn(async move {
            sleep(Duration::from_secs(2)).await;
            let state = state.read().await;
            if state.connected {
                sender_clone.send(ForwardedEvent::new_rcon(
                    endpoint_id,
                    RconForwardedEvent::ServerConnected {
                        host: state.host.clone(),
                        connect_url: conn_url,
                    },
                ))
            }
        });
        info!(
            "Connected to xonotic server {}, log socket at: {}",
            self.remote.addr, log_listen_addr
        );
        let keepalive_timestamp =
            Arc::new(Mutex::new((Instant::now(), Instant::now())));
        self.send("sv_eventlog 1").await?;
        self.send("sv_logscores_bots 1").await?;
        self.send("sv_eventlog_ipv6_delimiter 1").await?;
        self.send("g_maplist").await?;
        self.send(&format!("addtolist log_dest_udp {}", log_listen_addr))
            .await?;

        let cmd_parser = get_cmd_parser();
        let log_parser = get_log_parser();
        let (cmd_line_sender, cmd_line_receiver) = unbounded_channel();
        let mut cmd_receiver = RconReceiver::new(
            cmd_socket,
            PanicSender::new(cmd_line_sender),
            None,
        );
        let (log_line_sender, log_line_receiver) = unbounded_channel();
        let mut log_receiver = RconReceiver::new(
            log_socket,
            PanicSender::new(log_line_sender),
            self.logfile.clone(),
        );

        tokio::select!(
            res = self.cleanup_log_dest_udp(log_listen_addr) => res,
            res = self.keepalive(keepalive_timestamp.clone()) => res,
            res = self.recv_inner_events(
                event_sender,
                cmd_line_receiver,
                log_line_receiver,
                cmd_parser,
                log_parser,
                keepalive_timestamp) => res,
            res = cmd_receiver.receive_data() => res,
            res = log_receiver.receive_data() => res,
            res = self.recv_outer_events() => res
        )
    }
}

#[async_trait]
impl Endpoint for RconConnection {
    fn get_endpoint_id(&self) -> &str {
        &self.endpoint_id
    }

    async fn disconnected_since(&self) -> Option<Instant> {
        match *self.sockets.read().await {
            ConnectionStatus::Connected(_) => None,
            ConnectionStatus::Disconnected(i) => Some(i),
        }
    }

    async fn run(
        &self,
        forwarded_event_sender: &PanicSender<ForwardedEvent>,
    ) -> Result<Void, ReactorError> {
        const RECONNECT_MIN: Duration = Duration::from_secs(2);
        const RECONNECT_MAX: Duration = Duration::from_secs(600);
        const RECONNECT_INC_THRESHOLD: Duration = Duration::from_secs(30);
        let mut reconnect_cur = RECONNECT_MIN;
        loop {
            let started_at = Instant::now();
            let result = self.run_connection(forwarded_event_sender).await;
            {
                *self.sockets.write().await = Default::default();
            }
            if started_at.elapsed() < RECONNECT_INC_THRESHOLD {
                reconnect_cur = std::cmp::min(RECONNECT_MAX, reconnect_cur * 2);
            } else {
                reconnect_cur = RECONNECT_MIN;
            }
            {
                let state = &mut self.state.write().await;
                if state.connected {
                    state.connected = false;
                    forwarded_event_sender.send(ForwardedEvent::new_rcon(
                        self.endpoint_id.clone(),
                        RconForwardedEvent::ServerDisconnected {
                            host: state.host.clone(),
                        },
                    ))
                }
            }
            info!(
                "xonotic endpoint {} disconnected: {:?}, attempting reconnect in {} seconds...",
                self.endpoint_id, result, reconnect_cur.as_secs()
            );
            sleep(reconnect_cur).await;
        }
    }
}

pub fn f64_to_decimal(f: f64, prec: usize) -> rust_decimal::Decimal {
    Decimal::from_str(&format!("{0:.1$}", f, prec)).unwrap()
}

pub fn format_cts_time(time: Decimal) -> String {
    let sixty_x_sixty = dec!(3600.00);
    let sixty = dec!(60.00);
    let hours = (time / sixty_x_sixty).floor();
    let minutes = ((time - hours * sixty_x_sixty) / sixty).floor();
    let mut seconds = time - hours * sixty_x_sixty - minutes * sixty;
    seconds.rescale(2);
    let seconds = format!("{:05}", seconds);
    if hours == dec!(0) {
        if minutes == dec!(0) {
            format!("{seconds}s")
        } else {
            format!("{minutes}:{seconds}s")
        }
    } else {
        format!("{hours}:{minutes:02}:{seconds}s")
    }
}

fn format_join_msg(
    mode: types::GameMode,
    player: &Player,
    map: &types::MapName,
    players: (u16, u16),
) -> String {
    let mut msg = format!("^2 +join:^7 {}", player.name.to_dp());
    if let Some(ref country) = player.city_data.country {
        let _ = write!(msg, " ^2{}^7", country.1);
    }
    if let Some(formatted_stats) = player.format_stats(mode) {
        let _ = write!(msg, " ^x4F0[{}^x4F0]", formatted_stats.to_dp());
    }
    let _ = write!(msg, " ^3{} ^5[{}/{}]^7", map, players.0, players.1);
    msg
}

fn format_recordset(
    player_name: &DPString,
    map: &types::MapName,
    pos: u16,
    time: f64,
) -> String {
    let colors = ["FD0", "CCC", "A76"];
    let positions = ["1st", "2nd", "3rd"];
    if pos <= 3 {
        let color = colors[pos as usize - 1];
        let pos = positions[pos as usize - 1];
        let time = format_cts_time(f64_to_decimal(time, 2));
        format!(
            "^1\\o/ ^x{color}{map} {pos}:^7 {} - ^x{color}{time}^7",
            player_name.to_dp()
        )
    } else {
        String::from("")
    }
}

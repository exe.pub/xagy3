use lazy_static::lazy_static;
use regex::Regex;

use crate::rcon::base_parser::{
    one_line_regex_parser, parse_capture, CombinedParser, RconEventSender,
};
use crate::rcon::types::*;
use dpcolors::DPString;

use super::{base_parser::*, players::CryptoIdfp};

fn parse_join(
    _state: &mut ParserState,
    line: &str,
    event_sender: &RconEventSender,
) -> ParserResult {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r":join:(\d+):(\d+):([^:]*):(.*)").unwrap();
    }
    one_line_regex_parser(line, &RE, |caps| {
        if let Ok(ip) = caps
            .get(3)
            .unwrap()
            .as_str()
            .replace('_', ":")
            .parse::<std::net::IpAddr>()
        {
            let slot_id: SlotId = parse_capture(&caps, 1);
            let entity_id: EntityId = parse_capture(&caps, 2);
            // let crypto_idfp: CryptoIdfp = parse_capture(&caps, 3);
            let name = caps.get(4).unwrap().as_str();
            event_sender.send(RconEvent::Join {
                slot_id,
                ip,
                entity_id,
                // crypto_idfp: if crypto_idfp.is_empty() {None} else {Some(crypto_idfp)},
                name: DPString::parse(name),
            });
            ParserResult::Success
        } else {
            ParserResult::NoParse
        }
    })
}

pub fn parse_playerinfo(
    _state: &mut ParserState,
    line: &str,
    event_sender: &RconEventSender,
) -> ParserResult {
    lazy_static! {
        static ref RE: Regex = Regex::new(
            r":playerinfo:(\d+):(\d+):(\d+):(\d+):(\d+):([^:]*):([^:]*):(.*)"
        )
        .unwrap();
    }
    one_line_regex_parser(line, &RE, |caps| {
        if let Ok(ip) = caps
            .get(6)
            .unwrap()
            .as_str()
            .replace('_', ":")
            .parse::<std::net::IpAddr>()
        {
            let slot_id: SlotId = parse_capture(&caps, 1);
            let entity_id: EntityId = parse_capture(&caps, 2);
            let allow_uidtracking: u8 = parse_capture(&caps, 3);
            let allow_uid2name: u8 = parse_capture(&caps, 4);
            let allow_uidranking: u8 = parse_capture(&caps, 5);
            let crypto_idfp: CryptoIdfp = parse_capture(&caps, 7);
            let name = caps.get(8).unwrap().as_str();
            event_sender.send(RconEvent::PlayerInfo {
                slot_id,
                entity_id,
                allow_uidtracking: allow_uidtracking != 0,
                allow_uid2name: allow_uid2name != 0,
                allow_uidranking: allow_uidranking != 0,
                ip,
                crypto_idfp: if crypto_idfp.is_empty() {
                    None
                } else {
                    Some(crypto_idfp)
                },
                name: DPString::parse(name),
            });
            ParserResult::Success
        } else {
            ParserResult::NoParse
        }
    })
}

fn parse_part(
    _state: &mut ParserState,
    line: &str,
    event_sender: &RconEventSender,
) -> ParserResult {
    lazy_static! {
        static ref RE: Regex = Regex::new(r":part:(\d+)").unwrap();
    }
    one_line_regex_parser(line, &RE, |caps| {
        let slot_id: SlotId = parse_capture(&caps, 1);
        event_sender.send(RconEvent::Part { slot_id });
        ParserResult::Success
    })
}

fn parse_chat(
    _state: &mut ParserState,
    line: &str,
    event_sender: &RconEventSender,
) -> ParserResult {
    // TODO: can we get says from admin console?
    lazy_static! {
        static ref RE: Regex = Regex::new(r":chat:(\d+):(.*)").unwrap();
        static ref RE_TEAM: Regex =
            Regex::new(r":chat_team:(\d+):(\d+):(.*)").unwrap();
        static ref RE_SPEC: Regex =
            Regex::new(r":chat_spec:(\d+):(.*)").unwrap();
        static ref RE_MINI: Regex =
            Regex::new(r":chat_minigame:(\d+):([^:].+):(.*)").unwrap();
    }
    one_line_regex_parser(line, &RE, |caps| {
        let slot_id: SlotId = parse_capture(&caps, 1);
        let message = string_capture(&caps, 2);
        event_sender.send(RconEvent::Chat {
            slot_id,
            message,
            team_id: None,
        });
        ParserResult::Success
    })
    .or(one_line_regex_parser(line, &RE_TEAM, |caps| {
        let slot_id: SlotId = parse_capture(&caps, 1);
        let team_id: TeamId = parse_capture(&caps, 2);
        let message = string_capture(&caps, 3);
        event_sender.send(RconEvent::Chat {
            slot_id,
            message,
            team_id: Some(Team::from_u32(team_id)),
        });
        ParserResult::Success
    }))
    .or(one_line_regex_parser(line, &RE_SPEC, |caps| {
        let slot_id: SlotId = parse_capture(&caps, 1);
        let message = string_capture(&caps, 2);
        event_sender.send(RconEvent::Chat {
            slot_id,
            message,
            team_id: Some(Team::Spec),
        });
        ParserResult::Success
    }))
    .or(one_line_regex_parser(line, &RE_MINI, |_caps| {
        // TODO implement support for minigame chat when we need it
        // let slot_id: SlotId = parse_capture(&caps, 1);
        // let netname = string_capture(&caps, 2);
        // let message = string_capture(&caps, 3);
        ParserResult::Success
    }))
}

fn parse_gamestart(
    _state: &mut ParserState,
    line: &str,
    event_sender: &RconEventSender,
) -> ParserResult {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r#":gamestart:([^_]+)_([^:]+):.*"#).unwrap();
    }
    one_line_regex_parser(line, &RE, |caps| {
        let mode = GameMode::from(caps.get(1).unwrap().as_str());
        let map = string_capture(&caps, 2);
        event_sender.send(RconEvent::GameStart { mode, map });
        ParserResult::Success
    })
}

fn parse_name_change(
    _state: &mut ParserState,
    line: &str,
    event_sender: &RconEventSender,
) -> ParserResult {
    lazy_static! {
        static ref RE: Regex = Regex::new(r":name:(\d+):(.*)").unwrap();
    }
    one_line_regex_parser(line, &RE, |caps| {
        let slot_id = parse_capture(&caps, 1);
        let name = caps.get(2).unwrap().as_str();
        event_sender.send(RconEvent::NameChange {
            slot_id,
            name: DPString::parse(name),
        });
        ParserResult::Success
    })
}

fn parse_vote(
    _state: &mut ParserState,
    line: &str,
    event_sender: &RconEventSender,
) -> ParserResult {
    lazy_static! {
        static ref VCALL: Regex =
            Regex::new(r":vote:vcall:(\d+):(.*)").unwrap();
        static ref VSTOP: Regex = Regex::new(r":vote:vstop:(\d+)").unwrap();
        static ref VRES: Regex =
            Regex::new(r":vote:v(yes|no):(\d+):(\d+):(\d+):(\d+):(-?\d+)")
                .unwrap();
    }
    one_line_regex_parser(line, &VCALL, |caps| {
        let slot_id = parse_capture(&caps, 1);
        let vote = DPString::parse(caps.get(2).unwrap().as_str());
        event_sender.send(RconEvent::VCall { slot_id, vote });
        ParserResult::Success
    })
    .or(one_line_regex_parser(line, &VSTOP, |caps| {
        let slot_id = parse_capture(&caps, 1);
        event_sender.send(RconEvent::VStop { slot_id });
        ParserResult::Success
    }))
    .or(one_line_regex_parser(line, &VRES, |caps| {
        let passed = caps.get(1).unwrap().as_str() == "yes";
        event_sender.send(RconEvent::VResult {
            passed,
            accepted: parse_capture(&caps, 2),
            rejected: parse_capture(&caps, 3),
            abstained: parse_capture(&caps, 4),
            didnt_vote: parse_capture(&caps, 5),
            mincount: parse_capture(&caps, 6),
        });
        ParserResult::Success
    }))
}

fn parse_recordset(
    _state: &mut ParserState,
    line: &str,
    event_sender: &RconEventSender,
) -> ParserResult {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r":recordset:(\d+):(\d+):(\d+):(\d+\.?\d*)").unwrap();
    }
    one_line_regex_parser(line, &RE, |caps| {
        event_sender.send(RconEvent::RecordSet {
            pos: parse_capture(&caps, 1),
            slot_id: parse_capture(&caps, 2),
            entity_id: parse_capture(&caps, 3),
            time: parse_capture(&caps, 4),
        });
        ParserResult::Success
    })
}

fn parse_anon_recordset(
    _state: &mut ParserState,
    line: &str,
    event_sender: &RconEventSender,
) -> ParserResult {
    lazy_static! {
        static ref RE: Regex = Regex::new(
            r#"^(.*) scored a new record with (.*), but.*will be lost."#
        )
        .unwrap();
    }
    one_line_regex_parser(line, &RE, |caps| {
        event_sender.send(RconEvent::AnonRecordSet {
            nickname: DPString::parse(caps.get(1).unwrap().as_str()),
            time: string_capture(&caps, 2),
        });
        ParserResult::Success
    })
}

pub fn get_log_parser() -> CombinedParser {
    CombinedParser::new(vec![
        parse_join,
        parse_playerinfo,
        parse_part,
        parse_chat,
        parse_gamestart,
        parse_name_change,
        parse_vote,
        parse_recordset,
        parse_anon_recordset,
    ])
}

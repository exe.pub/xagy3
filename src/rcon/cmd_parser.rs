use lazy_static::lazy_static;
use regex::Regex;
use std::net::IpAddr;
use std::time::Instant;

use crate::rcon::base_parser::*;
use crate::rcon::state::ResourcesState;
use crate::rcon::types::*;
use dpcolors::DPString;

fn parse_cvar(
    _state: &mut ParserState,
    line: &str,
    event_sender: &RconEventSender,
) -> ParserResult {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r#"^(?:")?(?:\^3)?(\w+)(?:\^7)?(?:")? is "([^"]*)""#)
                .unwrap();
    }
    if let Some(caps) = RE.captures(line) {
        let name = string_capture(&caps, 1);
        let value = string_capture(&caps, 2);
        event_sender.send(RconEvent::Cvar { name, value });
        ParserResult::Success
    } else {
        ParserResult::NoParse
    }
}

fn parse_status_line(
    _state: &mut ParserState,
    line: &str,
    event_sender: &RconEventSender,
) -> ParserResult {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r"^(host|version|protocol|map|timing|players):\s*(.*)$").unwrap();
        static ref PROTOCOL_RE: Regex = Regex::new(r"(\d+) \((.*)\)").unwrap();
        static ref PLAYERS_RE: Regex = Regex::new(r"(\d+) active \((\d+) max\)").unwrap();
        static ref TIMING_RE: Regex = Regex::new(
            // 37.9% CPU, 0.00% lost, offset avg 2.4ms, max 13.3ms, sdev 2.6ms
            r"(\d{1,2}\.\d)% CPU, (\d+\.\d{2})% lost, offset avg (\d+\.\d)ms, max (\d+\.\d)ms, sdev (\d+\.\d)ms"
        ).unwrap();
    }
    if let Some(caps) = RE.captures(line) {
        let status_type = caps.get(1).unwrap().as_str();
        let status_value = string_capture(&caps, 2);
        match status_type {
            "host" => {
                event_sender.send(RconEvent::StatusHost(status_value));
            }
            // "version" => {
            // }
            // "protocol" => {
            //     // if let Some(caps) = PROTOCOL_RE.captures(&status_value) {
            //     // }
            // }
            "map" => {
                event_sender.send(RconEvent::StatusMap(status_value));
            }
            "timing" => {
                if let Some(caps) = TIMING_RE.captures(&status_value) {
                    event_sender.send(RconEvent::StatusTiming(
                        ResourcesState {
                            cpu: parse_capture(&caps, 1),
                            lost: parse_capture(&caps, 2),
                            offset_avg: parse_capture(&caps, 3),
                            offset_max: parse_capture(&caps, 4),
                            offset_sdev: parse_capture(&caps, 5),
                        },
                    ));
                }
            }
            "players" => {
                if let Some(caps) = PLAYERS_RE.captures(&status_value) {
                    event_sender.send(RconEvent::StatusPlayers(
                        parse_capture(&caps, 1),
                        parse_capture(&caps, 2),
                    ));
                }
            }
            _ => {}
        }
        ParserResult::Success
    } else {
        ParserResult::NoParse
    }
}

fn parse_status_player(
    _state: &mut ParserState,
    line: &str,
    event_sender: &RconEventSender,
) -> ParserResult {
    // print ("%s%-47s %2i %4i %2i:%02i:%02i %4i  #%-3u ^7%s\n", k%2 ? "^3" : "^7", ip, packetloss, ping, hours, minutes, seconds, frags, i+1, client->name);
    lazy_static! {
        static ref RE: Regex = Regex::new(
            r"^\^(?:3|7)\[?(?P<ip>[\da-fA-F:\.]+)\]?:(?P<port>\d+)\s+(?P<pl>\d+)\s+(?P<ping>\d+)\s+(?P<time_h>\d{1,2}):(?P<time_m>\d{2}):(?P<time_s>\d{2}+)\s+(?P<frags>-?\d+)\s+#(?P<entity_id>\d+)\s+(?P<nickname>.*)$"
        ).unwrap();
    }
    // println!("Trying to parse line {}", line);
    if let Some(caps) = RE.captures(line) {
        let ip: IpAddr = parse_capture_n(&caps, "ip");
        let time_h: u32 = parse_capture_n(&caps, "time_h");
        let time_m: u32 = parse_capture_n(&caps, "time_m");
        let time_s: u32 = parse_capture_n(&caps, "time_s");
        let player = StatusPlayer {
            updated_at: Instant::now(),
            entity_id: parse_capture_n(&caps, "entity_id"),
            ip,
            port: parse_capture_n(&caps, "port"),
            ping: parse_capture_n(&caps, "ping"),
            pl: parse_capture_n(&caps, "pl"),
            frags: parse_capture_n(&caps, "frags"),
            name: DPString::parse(caps.name("nickname").unwrap().as_str()),
            time: time_h * 60 * 60 + time_m * 60 + time_s,
        };
        event_sender.send(RconEvent::StatusPlayer(player));
        ParserResult::Success
    } else {
        ParserResult::NoParse
    }
}

pub fn get_cmd_parser() -> CombinedParser {
    CombinedParser::new(vec![
        parse_cvar,
        parse_status_line,
        parse_status_player,
        super::log_parser::parse_playerinfo,
    ])
}

use super::{types::*, STATUS_CHECK_PERIOD};
use crate::geo::*;
use crate::reactor::PanicSender;
use bimap::BiMap;
use chrono::prelude::*;
use dpcolors::DPString;
use percent_encoding::{utf8_percent_encode, NON_ALPHANUMERIC};
use serde::Deserialize;
use std::collections::HashMap;
use std::fmt::Write as _;
use std::net::IpAddr;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::RwLock;
use tokio_retry::strategy::{jitter, FixedInterval};
use tokio_retry::Retry;
use tracing::{info, trace, warn};

pub type CryptoIdfp = String;

static XONSTAT_BASE_SKILL_URL: &str =
    "https://stats.xonotic.org/skill?hashkey=";
static XONSTAT_BASE_PLAYER_URL: &str = "https://stats.xonotic.org/player/";

#[derive(Debug, Clone, Deserialize)]
pub struct EloData {
    pub player_id: u64,
    #[serde(rename(deserialize = "game_type_cd"))]
    pub game_type: GameMode,
    pub mu: f64,
    pub sigma: f64,
    pub active_ind: bool,
    pub create_dt: DateTime<Utc>,
    pub update_dt: DateTime<Utc>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct GamesPlayed {
    // #[serde(rename(deserialize = "game_type_cd"))]
    // pub game_type: GameMode,
    pub games: u64,
    pub wins: u64,
    pub losses: u64,
    pub win_pct: f64,
}

#[derive(Debug, Clone, Deserialize)]
pub struct OverallStats {
    // #[serde(rename(deserialize = "game_type_cd"))]
    // pub game_type: GameMode,
    pub total_kills: u64,
    pub total_deaths: u64,
    pub k_d_ratio: f64,
    pub total_captures: u64,
    pub total_pickups: u64,
    pub cap_ratio: f64,
    pub total_carrier_frags: u64,
    pub last_played: DateTime<Utc>,
    pub total_playing_time: u64,
}

#[derive(Debug, Clone, Deserialize)]
pub struct PlayerData {
    pub player_id: u64,
    pub nick: DPString,
    pub stripped_nick: String,
    pub active_ind: bool,
    pub joined: DateTime<Utc>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct XonStatsData {
    pub player: PlayerData,
    pub games_played: HashMap<GameMode, GamesPlayed>,
    pub overall_stats: HashMap<GameMode, OverallStats>,
}

impl XonStatsData {
    pub fn format_for_mode(&self, mode: GameMode) -> DPString {
        let mode = match mode {
            GameMode::Unknown => GameMode::Overall,
            _ => mode,
        };
        let (games, wins, losses) =
            if let Some(gp) = self.games_played.get(&mode) {
                (gp.games, gp.wins, gp.losses)
            } else {
                (0, 0, 0)
            };
        let mut str =
            format!("^3{}^7: ^2games: {}", mode.to_short_str(), games);
        if mode != GameMode::Cts {
            let _ = write!(str, ", w/l: {}/{}", wins, losses);
        }
        if let Some(overall) = self.overall_stats.get(&mode) {
            match mode {
                GameMode::Cts => {
                    let dur = chrono::Duration::seconds(
                        overall.total_playing_time as i64,
                    );
                    let ht = chrono_humanize::HumanTime::from(dur);
                    let ht_str = ht.to_text_en(
                        chrono_humanize::Accuracy::Rough,
                        chrono_humanize::Tense::Present,
                    );
                    let _ = write!(str, ", time played: {}", ht_str);
                }
                GameMode::Ctf => {
                    let _ = write!(
                        str,
                        ", caps: {}, k/d: {}/{}",
                        overall.total_captures,
                        overall.total_kills,
                        overall.total_deaths
                    );
                }
                _ => {
                    let _ = write!(
                        str,
                        ", k/d: {}/{}",
                        overall.total_kills, overall.total_deaths
                    );
                }
            }
        }
        DPString::parse(&str)
    }
}

async fn query_xonstat_inner(
    crypto_idfp: &str,
) -> reqwest::Result<Option<(Vec<EloData>, XonStatsData)>> {
    // TODO: use separate retry for each HTTP request
    let encoded = utf8_percent_encode(crypto_idfp, NON_ALPHANUMERIC);
    let url = format!("{}{}", XONSTAT_BASE_SKILL_URL, encoded);
    let mut headers = reqwest::header::HeaderMap::new();
    headers.insert(
        "ACCEPT",
        reqwest::header::HeaderValue::from_static("application/json"),
    );
    let client = reqwest::Client::builder()
        .default_headers(headers)
        .redirect(reqwest::redirect::Policy::limited(3))
        .build()?;
    trace!("Will query xonstat url {}", url);
    let elo_data: Vec<EloData> = client.get(url).send().await?.json().await?;
    if elo_data.is_empty() {
        Ok(None)
    } else {
        let player_id = elo_data[0].player_id;
        let url = format!("{}{}", XONSTAT_BASE_PLAYER_URL, player_id);
        let stats_data: XonStatsData =
            client.get(&url).send().await?.json().await?;
        Ok(Some((elo_data, stats_data)))
    }
}

async fn get_xonstat_data(
    crypto_idfp: &str,
) -> Option<(Vec<EloData>, XonStatsData)> {
    let retry_strat = FixedInterval::from_millis(1000).map(jitter).take(3);
    match Retry::spawn(retry_strat, || query_xonstat_inner(crypto_idfp)).await {
        Ok(data) => data,
        Err(err) => {
            warn!("Error querying xonstat data for {}: {}", crypto_idfp, err);
            None
        }
    }
}

#[derive(Debug, Clone)]
pub enum XonStatsQueryStatus {
    NotRequested,
    Pending,
    Received(Vec<EloData>, XonStatsData),
    Unavailable,
    NoCryptoIdfp,
    // Concealed,
}

impl XonStatsQueryStatus {
    fn already_requested(&self) -> bool {
        !matches!(self, XonStatsQueryStatus::NotRequested)
    }
}

#[derive(Debug, Clone)]
pub struct Player {
    pub db_id: Option<i32>,
    // pub slot_id: SlotId,
    pub entity_id: EntityId,
    pub name: DPString,
    pub ip: IpAddr,
    pub allow_uid2name: bool,
    pub allow_uidtracking: bool,
    pub allow_uidranking: bool,
    pub crypto_idfp: Option<CryptoIdfp>,
    pub xonstats: XonStatsQueryStatus,
    pub city_data: Geoip2CityData,
    pub asn_data: Option<Geoip2AsnData>,
}

impl Player {
    pub fn format_stats(&self, mode: GameMode) -> Option<DPString> {
        if let XonStatsQueryStatus::Received(_, ref data) = self.xonstats {
            Some(data.format_for_mode(mode))
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, Default)]
pub struct PlayerManager {
    players: HashMap<EntityId, Player>,
    pub status_players: HashMap<EntityId, StatusPlayer>,
    pub slot_to_entity: BiMap<SlotId, EntityId>,
}

async fn get_xonstat_data_task(
    mgr: Arc<RwLock<PlayerManager>>,
    sender: PanicSender<RconEvent>,
    entity_id: EntityId,
    crypto_idfp: Option<String>,
) {
    let event = RconEvent::XonstatsQueryComplete { entity_id };
    {
        let mgr = &mut mgr.write().await;
        if let Some(player) = mgr.players.get_mut(&entity_id) {
            if player.xonstats.already_requested() {
                // we already have started xonstat query for this player
                sender.send(event);
                return;
            }
            if player.crypto_idfp == crypto_idfp {
                if crypto_idfp.is_some() {
                    player.xonstats = XonStatsQueryStatus::Pending;
                } else {
                    player.xonstats = XonStatsQueryStatus::NoCryptoIdfp;
                    sender.send(event);
                    return;
                }
            }
        }
    }
    if let Some(ref real_crypto_idfp) = crypto_idfp {
        let data = get_xonstat_data(real_crypto_idfp).await;
        let mgr = &mut mgr.write().await;
        if let Some(player) = mgr.players.get_mut(&entity_id) {
            if player.crypto_idfp == crypto_idfp {
                if let Some(data) = data {
                    player.xonstats =
                        XonStatsQueryStatus::Received(data.0, data.1);
                } else {
                    player.xonstats = XonStatsQueryStatus::Unavailable;
                }
            } else {
                player.xonstats = XonStatsQueryStatus::Unavailable;
            }
        }
    }
    sender.send(event);
}

impl PlayerManager {
    pub fn get_by_slot_id(&self, slot_id: SlotId) -> Option<&Player> {
        if let Some(ent_id) = self.slot_to_entity.get_by_left(&slot_id) {
            self.players.get(ent_id)
        } else {
            None
        }
    }

    pub fn get_by_entity_id(&self, entity_id: EntityId) -> Option<&Player> {
        self.players.get(&entity_id)
    }

    pub fn get_mut_by_entity_id(
        &mut self,
        entity_id: EntityId,
    ) -> Option<&mut Player> {
        self.players.get_mut(&entity_id)
    }

    pub fn get_mut_by_slot_id(
        &mut self,
        slot_id: SlotId,
    ) -> Option<&mut Player> {
        if let Some(ent_id) = self.slot_to_entity.get_by_left(&slot_id) {
            self.players.get_mut(ent_id)
        } else {
            None
        }
    }

    pub fn clear_slot_to_entity(&mut self) {
        self.slot_to_entity.clear()
    }

    pub fn is_empty(&self) -> bool {
        self.players.is_empty()
    }

    pub fn len(&self) -> usize {
        self.players.len()
    }

    pub fn iter(
        &self,
    ) -> std::collections::hash_map::Iter<'_, EntityId, Player> {
        self.players.iter()
    }

    // Tracking players properly is the trickiest logic in the application :(
    // the thing here is that entity_ids of players stay but slot_ids can change
    // from one game to another
    // also we must keep in mind that it's possible that we will receive event log
    // in incorrect order or will lose some lines from it. Probability of that is
    // close to zero it's but still...
    pub async fn handle_status_player(
        mgr: Arc<RwLock<Self>>,
        sp: StatusPlayer,
    ) -> bool {
        let mgr = &mut mgr.write().await;
        let ent_id = sp.entity_id;
        // check if we have this player in .players? If not, then ask for playerinfo
        let mut res = if let Some(player) = mgr.players.get_mut(&ent_id) {
            // TODO: fire name change event here?
            player.name = sp.name.clone();
            false
        } else {
            true
        };
        if !mgr.slot_to_entity.contains_right(&ent_id) {
            res = true;
        };
        mgr.status_players.insert(sp.entity_id, sp);
        res
    }

    pub async fn cleanup_stale_players(
        mgr: Arc<RwLock<Self>>,
        sender: PanicSender<RconEvent>,
    ) {
        let mgr = &mut mgr.write().await;
        let mut to_remove = Vec::new();
        for (ent_id, sp) in mgr.status_players.iter() {
            if sp.updated_at.elapsed()
                >= Duration::from_secs(STATUS_CHECK_PERIOD * 5 + 1)
            {
                to_remove.push(*ent_id);
            }
        }
        for i in to_remove.iter() {
            mgr.status_players.remove(i);
            mgr.slot_to_entity.remove_by_right(i);
            if let Some(removed) = mgr.players.remove(i) {
                sender.send(RconEvent::PlayerRemoved { player: removed });
            }
        }
    }

    pub async fn handle_join(
        mgr: Arc<RwLock<Self>>,
        slot_id: SlotId,
        entity_id: EntityId,
        _ip: IpAddr,
        name: DPString,
    ) -> bool {
        // Do we need to do anything or handling playerinfo would be enough?
        info!("join: slot_id: {slot_id}, entity_id: {entity_id}");
        let mgr = &mut mgr.write().await;
        mgr.slot_to_entity.insert(slot_id, entity_id);
        if let Some(player) = mgr.players.get_mut(&entity_id) {
            // player.slot_id = slot_id;
            player.name = name;
            false
        } else {
            true
        }
    }

    pub async fn handle_playerinfo(
        mgr: Arc<RwLock<Self>>,
        geoip_city: &maxminddb::Reader<Vec<u8>>,
        geoip_asn: &maxminddb::Reader<Vec<u8>>,
        sender: PanicSender<RconEvent>,
        slot_id: SlotId,
        entity_id: EntityId,
        allow_uidtracking: bool,
        allow_uid2name: bool,
        allow_uidranking: bool,
        ip: IpAddr,
        crypto_idfp: Option<CryptoIdfp>,
        name: DPString,
    ) -> bool {
        let mgr_clone = mgr.clone();
        let mgr = &mut mgr.write().await;
        mgr.slot_to_entity.insert(slot_id, entity_id);
        info!(
            "handle playerinfo: slot_id={}, entity_id={}",
            slot_id, entity_id
        );
        if let Some(_p) = mgr.get_mut_by_entity_id(entity_id) {
            // p.slot_id = slot_id;
            false
        } else {
            mgr.players.insert(
                entity_id,
                Player {
                    db_id: None,
                    entity_id,
                    // slot_id,
                    allow_uidtracking,
                    allow_uid2name,
                    allow_uidranking,
                    ip,
                    crypto_idfp: crypto_idfp.clone(),
                    name,
                    xonstats: if crypto_idfp.is_some() {
                        XonStatsQueryStatus::NotRequested
                    } else {
                        XonStatsQueryStatus::NoCryptoIdfp
                    },
                    city_data: lookup_city(geoip_city, ip),
                    asn_data: lookup_asn(geoip_asn, ip),
                },
            );
            tokio::spawn(async move {
                get_xonstat_data_task(mgr_clone, sender, entity_id, crypto_idfp)
                    .await
            });
            true
        }
    }

    pub async fn handle_part(
        mgr: Arc<RwLock<Self>>,
        slot_id: SlotId,
        sender: PanicSender<RconEvent>,
    ) {
        info!("part: slot_id={slot_id}");
        let mgr = &mut mgr.write().await;
        if let Some((_, ent_id)) = mgr.slot_to_entity.remove_by_left(&slot_id) {
            if let Some(removed) = mgr.players.remove(&ent_id) {
                sender.send(RconEvent::PlayerRemoved { player: removed });
            }
            mgr.status_players.remove(&ent_id);
        }
    }
    pub fn count_active(&self) -> usize {
        self.status_players
            .values()
            .filter(|p| p.frags != -666)
            .count()
    }

    pub fn count_total(&self) -> usize {
        self.status_players.len()
    }
}

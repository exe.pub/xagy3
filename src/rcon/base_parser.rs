use crate::reactor::PanicSender;
use regex::{Captures, Regex};
use std::fmt::Debug;
use std::str::FromStr;

use super::types::RconEvent;

pub type RconEventSender = PanicSender<RconEvent>;

#[derive(Debug, Clone, Copy)]
pub enum ParserResult {
    NoParse,
    Success,
    // WantMore,
}

impl ParserResult {
    pub fn or(&self, other: ParserResult) -> ParserResult {
        use ParserResult::*;
        match (*self, other) {
            (NoParse, NoParse) => NoParse,
            // (WantMore, _) => WantMore,
            // (_, WantMore) => WantMore,
            _ => Success,
        }
    }
}

pub type ParserState = Vec<String>;
pub type Parser = fn(&mut ParserState, &str, &RconEventSender) -> ParserResult;

pub fn string_capture(c: &Captures, i: usize) -> String {
    String::from(c.get(i).unwrap().as_str())
}

pub fn parse_capture<T: FromStr + Debug>(c: &Captures, i: usize) -> T
where
    <T as FromStr>::Err: Debug,
{
    c.get(i).unwrap().as_str().parse().unwrap()
}

pub fn parse_capture_n<T: FromStr + Debug>(c: &Captures, n: &str) -> T
where
    <T as FromStr>::Err: Debug,
{
    c.name(n).unwrap().as_str().parse().unwrap()
}

pub fn one_line_regex_parser<F>(
    line: &str,
    re: &Regex,
    caps_processor: F,
) -> ParserResult
where
    F: FnOnce(Captures) -> ParserResult,
{
    if let Some(caps) = re.captures(line) {
        caps_processor(caps)
    } else {
        ParserResult::NoParse
    }
}

pub struct CombinedParser {
    parsers: Vec<Parser>,
    active_parser: Option<(ParserState, Parser)>,
}

impl CombinedParser {
    pub fn new(parsers: Vec<Parser>) -> Self {
        Self {
            parsers,
            active_parser: None,
        }
    }

    pub fn parse(&mut self, line: &str, event_sender: &PanicSender<RconEvent>) {
        if self.active_parser.is_some() {
            self.active_parser = None;
            // match parser(&mut state, line, event_sender) {
            //     ParserResult::WantMore => {
            //         self.active_parser = Some((state, parser));
            //     }
            //     _ => {
            //         self.active_parser = None;
            //     }
            // }
        }
        for i in self.parsers.iter() {
            let mut state = ParserState::new();
            match i(&mut state, line, event_sender) {
                ParserResult::NoParse => {}
                ParserResult::Success => break,
                // ParserResult::WantMore => {
                //     self.active_parser = Some((state, *i));
                //     break;
                // }
            }
        }
    }
}

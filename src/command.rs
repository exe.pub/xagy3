use crate::config::RoutingRuleInput;
use crate::db_utils::store_feedback;
use crate::endpoint::{Endpoint, EndpointId, EndpointType, TypedEndpoint};
use crate::events::{DeliveredEvent, MessageSpec};
use crate::rcon;
use crate::reactor::{can_send_to_endpoint, EndpointRef};
use crate::{fun, map_rating};
use dpcolors::DPString;
use sqlx::PgPool;
use std::collections::HashMap;
use std::future::Future;
use std::pin::Pin;
use std::sync::Arc;
use std::time::{Instant, Duration};
use tokio::sync::Mutex;
use tracing::warn;

#[derive(Clone, Debug)]
pub struct Command {
    pub user: CommandUserId,
    pub message_spec: MessageSpec,
    pub nickname: DPString,
    pub cmd: String,
    pub args: DPString,
}

impl Command {
    pub fn matches_input(spec: &MessageSpec, inp: &RoutingRuleInput) -> bool {
        let a = inp.prefix.is_empty();
        match spec {
            MessageSpec::None => a,
            MessageSpec::Channel(_) => {
                inp.from_channels.contains(spec) && a
            }
            _ => false,
        }
    }

    pub fn maybe_from(
        input_prefixes: &[String],
        user: usize,
        nickname: DPString,
        message_spec: MessageSpec,
        s: DPString,
    ) -> Option<Self> {
        for i in input_prefixes.iter() {
            if s.starts_with(i) {
                let slice = s.skip(i.len());
                let slice_clone = slice.clone();
                if !slice.is_empty() {
                    return match slice.split_once(' ') {
                        Some((cmd, args)) => Some(Self {
                            user,
                            nickname,
                            message_spec,
                            cmd: cmd.to_none().to_lowercase(),
                            args,
                        }),
                        None => Some(Self {
                            user,
                            nickname,
                            message_spec,
                            cmd: slice_clone.to_none().to_lowercase(),
                            args: DPString::parse(""),
                        }),
                    };
                }
            }
        }
        None
    }
}

pub type CommandUserId = usize;
pub type CommandState = HashMap<String, String>;

type CommandHandler = Arc<
    dyn Fn(
            PgPool,
            CommandState,
            EndpointRef,
            Arc<HashMap<EndpointId, EndpointRef>>,
            Command,
        )
            -> Pin<Box<dyn Future<Output = Option<CommandState>> + Send>>
        + Send
        + Sync,
>;

pub struct DispatchTable {
    table: HashMap<EndpointType, HashMap<String, CommandHandler>>,
    states: Arc<Mutex<HashMap<(EndpointId, CommandUserId), CommandState>>>,
}

impl Default for DispatchTable {
    fn default() -> Self {
        let commands: [(&str, &str, CommandHandler); 15] = [
            ("praise", "all", Arc::new(fun::praise)),
            ("excuse", "all", Arc::new(fun::excuse)),
            ("ask", "all", Arc::new(fun::ask)),
            ("+++", "xonotic", Arc::new(map_rating::map_rating)),
            ("++", "xonotic", Arc::new(map_rating::map_rating)),
            ("+", "xonotic", Arc::new(map_rating::map_rating)),
            ("-", "xonotic", Arc::new(map_rating::map_rating)),
            ("--", "xonotic", Arc::new(map_rating::map_rating)),
            ("---", "xonotic", Arc::new(map_rating::map_rating)),
            ("who", "all", Arc::new(rcon::commands::who)),
            ("maps", "all", Arc::new(rcon::commands::maps)),
            ("help", "all", Arc::new(help)),
            ("admin", "all", Arc::new(feedback)),
            ("reportbug", "all", Arc::new(feedback)),
            ("reportthanks", "all", Arc::new(feedback)),
        ];
        let mut table = HashMap::new();
        for (cmd, types, func) in commands.into_iter() {
            if types == "all" || types == "xonotic" {
                table
                    .entry(EndpointType::Xonotic)
                    .or_insert_with(HashMap::new)
                    .insert(String::from(cmd), func.clone());
            }
            if types == "all" || types == "irc" {
                table
                    .entry(EndpointType::Irc)
                    .or_insert_with(HashMap::new)
                    .insert(String::from(cmd), func.clone());
            }
            if types == "all" || types == "discord" {
                table
                    .entry(EndpointType::Discord)
                    .or_insert_with(HashMap::new)
                    .insert(String::from(cmd), func);
            }
        }
        Self {
            table,
            states: Default::default(),
        }
    }
}

impl DispatchTable {
    pub async fn run_command(
        &self,
        pool: PgPool,
        endpoint: EndpointRef,
        endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
        cmd: Command,
    ) {
        let endpoint_type = endpoint.endpoint.get_endpoint_type();
        if let Some(handler) =
            self.table.get(&endpoint_type).and_then(|t| t.get(&cmd.cmd))
        {
            let handler = handler.clone();
            let state = {
                self.states
                    .lock()
                    .await
                    .remove(&(
                        String::from(endpoint.endpoint.get_endpoint_id()),
                        cmd.user,
                    ))
                    .unwrap_or_default()
            };
            let states = self.states.clone();
            tokio::spawn(async move {
                let user_id = cmd.user;
                let endpoint_id =
                    String::from(endpoint.endpoint.get_endpoint_id());
                if let Some(new_state) =
                    handler(pool, state, endpoint, endpoints, cmd).await
                {
                    states
                        .lock()
                        .await
                        .insert((endpoint_id, user_id), new_state);
                }
            });
        } else {
            private_reply(
                endpoint,
                cmd.user,
                DPString::parse(&format!("No such command: {}", cmd.cmd)),
            )
            .await;
        }
    }
}

pub async fn private_reply(
    endpoint: EndpointRef,
    user_id: CommandUserId,
    message: DPString,
) {
    let msg = DeliveredEvent::ReactorMessage {
        message_spec: MessageSpec::Private(user_id),
        message,
        timestamp: Instant::now(),
    };
    endpoint.delivered_event_sender.send(msg).unwrap();
}

pub async fn public_reply(
    message_spec: &MessageSpec,
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    message: DPString,
) {
    if let MessageSpec::Private(user_id) = message_spec {
        private_reply(endpoint, *user_id, message).await
    } else {
        tokio::time::sleep(Duration::from_millis(500)).await;
        let msg = DeliveredEvent::ReactorMessage {
            message_spec: message_spec.clone(),
            message: message.clone(),
            timestamp: Instant::now(),
        };
        endpoint.delivered_event_sender.send(msg).unwrap();
        for rule in endpoint.routing_rules.iter() {
            if Command::matches_input(message_spec, &rule.input) {
                for output in rule.output.iter() {
                    for target_spec in &output.targets {
                        if let Some(target_endpoint) =
                            endpoints.get(&target_spec.endpoint_id)
                        {
                            if can_send_to_endpoint(
                                target_endpoint.endpoint.as_ref(),
                            )
                            .await
                            {
                                let msg = DeliveredEvent::ReactorMessage {
                                    message_spec: target_spec.message_spec.clone(),
                                    message: message.clone(),
                                    timestamp: Instant::now(),
                                };
                                target_endpoint
                                    .delivered_event_sender
                                    .send(msg)
                                    .unwrap()
                            }
                        } else {
                            warn!(
                                "Target endpoint {} not found!",
                                target_spec.endpoint_id
                            );
                        }
                    }
                }
            }
        }
    }
}

async fn help_inner(
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Option<CommandState> {
    let table = DispatchTable::default();
    let endpoint_type = endpoint.endpoint.get_endpoint_type();
    let mut commands: Vec<_> = table
        .table
        .get(&endpoint_type)
        .unwrap()
        .keys()
        .map(|i| i.as_str())
        .collect();
    commands.sort();
    let reply =
        format!("^2Available commands^7: {}", commands.as_slice().join(", "));
    public_reply(
        &cmd.message_spec,
        endpoint,
        endpoints,
        DPString::parse(&reply),
    )
    .await;
    None
}

fn help(
    _pool: PgPool,
    _state: CommandState,
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Pin<Box<dyn Future<Output = Option<CommandState>> + Send>> {
    Box::pin(help_inner(endpoint, endpoints, cmd))
}

async fn feedback_inner(
    pool: PgPool,
    endpoint: EndpointRef,
    cmd: Command,
) -> Option<CommandState> {
    if cmd.args.to_none().trim().is_empty() {
        private_reply(
            endpoint.clone(),
            cmd.user,
            DPString::parse("Please leave some feedback."),
        )
        .await;
        return None;
    }
    let player_id =
        if let TypedEndpoint::Xonotic(c) = endpoint.endpoint.as_ref() {
            let players = c.players.read().await;
            let player = players.get_by_entity_id(cmd.user as u32);
            player.and_then(|p| p.db_id)
        } else {
            None
        };
    if let Err(e) = store_feedback(
        &pool,
        player_id,
        endpoint.endpoint.get_endpoint_id(),
        &cmd.cmd,
        &cmd.args.to_none(),
    )
    .await
    {
        warn!("Error storing feedback `{}': {}", cmd.args.to_none(), e);
    }
    private_reply(
        endpoint,
        cmd.user,
        DPString::parse("Thanks for the feedback!"),
    )
    .await;
    None
}

fn feedback(
    pool: PgPool,
    _state: CommandState,
    endpoint: EndpointRef,
    _endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Pin<Box<dyn Future<Output = Option<CommandState>> + Send>> {
    Box::pin(feedback_inner(pool, endpoint, cmd))
}

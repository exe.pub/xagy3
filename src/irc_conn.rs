use crate::config::RoutingRuleInput;
use crate::discord::DiscordEvent;
use crate::endpoint::{ConnectionStatus, Endpoint, EndpointId};
use crate::events::{
    DeliveredEvent, DeliveredEventPayload, ForwardedEvent, MessageSpec,
    XagyEvent,
};
use crate::rcon::{f64_to_decimal, format_cts_time, RconForwardedEvent};
use crate::reactor::{PanicSender, ReactorError, Void};
use crate::{command, config};
use async_trait::async_trait;
use bimap::BiMap;
use dpcolors::DPString;
use futures::*;
use irc::client::prelude::*;
use lazy_static::lazy_static;
use regex::Regex;
use std::fmt::Write as _;
use std::time::Duration;
use std::time::Instant;
use thiserror::Error;
use tokio::sync::mpsc::UnboundedReceiver;
use tokio::sync::Mutex;
use tokio::time::sleep;
use tracing::{debug, info, trace, warn};

#[derive(Debug)]
pub struct IrcConnection {
    endpoint_id: EndpointId,
    pub endpoint_config: config::Endpoint,
    pub config: IrcConfig,
    outer_event_receiver: Mutex<UnboundedReceiver<DeliveredEvent>>,
    client: Mutex<ConnectionStatus<Client>>,
    usermap: Mutex<BiMap<usize, String>>,
    maxid: Mutex<usize>,
}

#[derive(Error, Debug)]
pub enum IrcError {
    #[error("irc error: {0}")]
    IrcError(#[from] irc::error::Error),
    #[error("internal error")]
    InternalError,
    #[error("disconnected")]
    Disconnect,
}

#[derive(Debug, Clone)]
pub struct IrcConfig {
    pub nickname: String,
    pub realname: String,
    pub server: String,
    // pub port: Option<u16>,
    pub channels: Vec<String>,
    pub quakenet_auth: Option<String>,
}

impl IrcConfig {
    fn into_config(self) -> Config {
        Config {
            nickname: Some(self.nickname),
            server: Some(self.server),
            // port: self.port,
            realname: Some(self.realname),
            use_tls: Some(false),
            channels: self.channels,
            ..Config::default()
        }
    }
}

pub type IrcChannel = String;

#[derive(Debug, Clone)]
pub enum IrcEvent {
    Message(MessageSpec, String, DPString),
    PrivateMessage(String, DPString),
    Join(MessageSpec, String),
    Part(MessageSpec, String, Option<String>),
    Action(MessageSpec, String, DPString),
    PrivateAction(String, DPString),
}

impl XagyEvent for IrcEvent {
    fn matches_input(&self, inp: &RoutingRuleInput) -> bool {
        match self {
            IrcEvent::Message(chan, _user, msg) => {
                inp.from_channels.contains(chan) && msg.starts_with(&inp.prefix)
            }
            IrcEvent::PrivateMessage(_user, _msg) => false,
            IrcEvent::Join(chan, _user) => {
                inp.from_channels.contains(chan) && inp.prefix.is_empty()
            }
            IrcEvent::Part(chan, _user, _maybe_reason) => {
                inp.from_channels.contains(chan) && inp.prefix.is_empty()
            }
            IrcEvent::Action(chan, _user, _text) => {
                inp.from_channels.contains(chan) && inp.prefix.is_empty()
            }
            IrcEvent::PrivateAction(_user, _msg) => false,
        }
    }
}

impl IrcConnection {
    pub fn new(
        endpoint_id: EndpointId,
        endpoint_config: config::Endpoint,
        config: IrcConfig,
        outer_event_receiver: Mutex<UnboundedReceiver<DeliveredEvent>>,
    ) -> Self {
        Self {
            endpoint_id,
            endpoint_config,
            config,
            outer_event_receiver,
            client: Default::default(),
            usermap: Default::default(),
            maxid: Mutex::new(0),
        }
    }

    async fn do_quakenet_auth(
        &self,
        quakenet_auth: &str,
    ) -> Result<(), IrcError> {
        if let ConnectionStatus::Connected(client) = &*self.client.lock().await
        {
            trace!("Sending quakenet auth: {}", quakenet_auth);
            // TODO: use CHALLENGE auth here
            client.send_privmsg("Q@CServe.quakenet.org", quakenet_auth)?;
            client.send_mode(
                client.current_nickname(),
                &[Mode::Plus(UserMode::MaskedHost, None)],
            )?;
        }
        Ok(())
    }

    fn send_event(&self, sender: &PanicSender<ForwardedEvent>, ev: IrcEvent) {
        sender.send(ForwardedEvent::new_irc(self.endpoint_id.clone(), ev))
    }

    async fn send_privmsg(
        &self,
        client: &Client,
        message_spec: &MessageSpec,
        formatted_msg: &str,
    ) -> Result<(), IrcError> {
        match &message_spec {
            MessageSpec::None => {}
            MessageSpec::Channel(chan) => {
                client.send_privmsg(format!("#{}", chan), formatted_msg)?;
            }
            MessageSpec::Private(user_id) => {
                if let Some(nickname) = self.get_nickname_by_id(*user_id).await
                {
                    client.send_privmsg(nickname, formatted_msg)?;
                }
            }
            MessageSpec::PrivateUsername(chan) => {
                client.send_privmsg(chan, formatted_msg)?;
            }
        }
        Ok(())
    }

    async fn process_rcon_event(
        &self,
        prefix: String,
        message_spec: MessageSpec,
        _from: String,
        ev: RconForwardedEvent,
    ) -> Result<(), IrcError> {
        if let ConnectionStatus::Connected(client) = &*self.client.lock().await
        {
            match ev {
                RconForwardedEvent::Chat {
                    player_name,
                    team,
                    message,
                } => {
                    let msg = if let Some(team) = team {
                        let nick = format!(
                            "{}(^7{}{})^7",
                            team.color_code(),
                            player_name.to_dp(),
                            team.color_code()
                        );
                        format!(
                            "\x0311{}\x0f{} {}",
                            prefix,
                            DPString::parse(&nick).to_irc(),
                            message.to_irc()
                        )
                    } else {
                        format!(
                            "\x0311{}\x0307{}\x0f: {}",
                            prefix,
                            player_name.to_irc(),
                            message.to_irc()
                        )
                    };
                    self.send_privmsg(client, &message_spec, &msg).await?;
                }
                RconForwardedEvent::GameStart { mode, map, players } => {
                    if players.0 > 0 {
                        let msg = format!(
                            "\x0311{}\x0fPlaying \x0312{}\x0f on \x0304{}\x0f [{}/{}] - https://xdf.gg/map/{}",
                            prefix,
                            mode.to_long_str(),
                            map,
                            players.0,
                            players.1,
                            map
                        );
                        self.send_privmsg(client, &message_spec, &msg).await?;
                    }
                }
                RconForwardedEvent::ServerConnected { host, connect_url } => {
                    let msg = format!(
                        "\x0311{}\x0303Server Connected:\x0f \x0312{}\x0f; connect at: \x02{}\x0f",
                        prefix, host, connect_url
                    );
                    self.send_privmsg(client, &message_spec, &msg).await?;
                }
                RconForwardedEvent::ServerDisconnected { host } => {
                    let msg = format!(
                        "\x0311{}\x0304Server Disconnected:\x0f \x0312{}\x0f",
                        prefix, host
                    );
                    self.send_privmsg(client, &message_spec, &msg).await?;
                }
                RconForwardedEvent::Join {
                    mode,
                    player,
                    map,
                    players,
                } => {
                    let mut msg = format!(
                        "\x0311{}\x0303+ join\x0f: {}\x0f",
                        prefix,
                        player.name.to_irc()
                    );
                    if let Some(ref country) = player.city_data.country {
                        let _ = write!(msg, " \x0303{}\x0f", country.1);
                    }
                    if let Some(formatted_stats) = player.format_stats(mode) {
                        let _ = write!(
                            msg,
                            " \x0312[{}\x0f\x0312]\x0f",
                            formatted_stats.to_irc()
                        );
                    }
                    let _ = write!(
                        msg,
                        " \x0304{}\x0f [\x0304{}\x0f/\x0304{}\x0f] https://xdf.gg/map/{}",
                        map, players.0, players.1, map
                    );
                    self.send_privmsg(client, &message_spec, &msg).await?;
                }
                RconForwardedEvent::Part {
                    player,
                    map,
                    players,
                } => {
                    let mut msg = format!(
                        "\x0311{}\x0304- part\x0f: {}\x0f",
                        prefix,
                        player.name.to_irc()
                    );
                    if let Some(ref country) = player.city_data.country {
                        let _ = write!(msg, " \x0303{}\x0f", country.1);
                    }
                    let _ = write!(
                        msg,
                        " \x0304{}\x0f [\x0304{}\x0f/\x0304{}\x0f] https://xdf.gg/map/{}",
                        map, players.0, players.1, map
                    );
                    self.send_privmsg(client, &message_spec, &msg).await?;
                }
                RconForwardedEvent::NameChange { old_name, new_name } => {
                    let msg = format!(
                        "\x0311{}\x0312*\x0f {}\x0f is now known as {}\x0f",
                        prefix,
                        old_name.to_irc(),
                        new_name.to_irc()
                    );
                    self.send_privmsg(client, &message_spec, &msg).await?;
                }
                RconForwardedEvent::VCall { player_name, vote } => {
                    let msg = format!(
                        "\x0311{}\x0312*\x0f {}\x0f calls a vote for \x0312{}\x0f",
                        prefix,
                        player_name.to_irc(),
                        vote.to_irc()
                    );
                    self.send_privmsg(client, &message_spec, &msg).await?;
                }
                RconForwardedEvent::VStop { player_name, vote } => {
                    let msg = format!(
                        "\x0311{}\x0312*\x0f {}\x0f stopped {}\x0f vote for \x0312{}\x0f",
                        prefix,
                        player_name.to_irc(),
                        player_name.to_irc(),
                        vote.to_irc()
                    );
                    self.send_privmsg(client, &message_spec, &msg).await?;
                }
                RconForwardedEvent::VResult {
                    vote,
                    passed,
                    accepted,
                    rejected,
                    ..
                } => {
                    let res = if passed {
                        "\x0303passed\x0f"
                    } else {
                        "\x0304rejected\x0f"
                    };
                    let msg = format!(
                        "{}\x0312*\x0f vote for \x0312{}\x0f {}: {}:{}",
                        prefix,
                        vote.to_irc(),
                        res,
                        accepted,
                        rejected
                    );
                    self.send_privmsg(client, &message_spec, &msg).await?;
                }
                RconForwardedEvent::RecordSet {
                    player_name,
                    map,
                    pos,
                    time,
                } => {
                    let (color, position, medal) = match pos {
                        1 => ("\x0307", "1st", "\u{1F947}"),
                        2 => ("\x0315", "2nd", "\u{1F948}"),
                        3 => ("\x0305", "3rd", "\u{1F949}"),
                        _ => ("", "", ""),
                    };
                    if pos <= 3 {
                        let msg = format!(
                            "\x0311{}\x0305\\o/\x0f {color}{map} {medal}{position}:\x0f {}\x0f - {color}{}",
                            prefix,
                            player_name.to_irc(),
                            format_cts_time(f64_to_decimal(time, 2))
                        );
                        self.send_privmsg(client, &message_spec, &msg).await?;
                    }
                }
            }
        }
        Ok(())
    }

    async fn process_irc_event(
        &self,
        _prefix: String,
        _message_spec: &MessageSpec,
        _from: String,
        _ev: IrcEvent,
    ) -> Result<(), IrcError> {
        Ok(())
    }

    async fn process_discord_event(
        &self,
        prefix: String,
        message_spec: &MessageSpec,
        _from: String,
        ev: DiscordEvent,
    ) -> Result<(), IrcError> {
        if let ConnectionStatus::Connected(client) = &*self.client.lock().await
        {
            match ev {
                DiscordEvent::Message(_spec, nick, message) => {
                    for line in
                        message.to_none().replace("\r\n", "\n").split('\n')
                    {
                        if !line.trim().is_empty() {
                            let msg = format!(
                                "\x0311{}\x0307{}\x0f: {}",
                                prefix, nick, line
                            );
                            self.send_privmsg(client, message_spec, &msg)
                                .await?;
                        }
                    }
                }
            }
        }
        Ok(())
    }

    async fn process_reactor_message(
        &self,
        message_spec: &MessageSpec,
        message: DPString,
    ) -> Result<(), IrcError> {
        if let ConnectionStatus::Connected(client) = &*self.client.lock().await
        {
            self.send_privmsg(client, message_spec, &message.to_irc())
                .await?;
        }
        Ok(())
    }

    async fn process_command(
        &self,
        prefix: String,
        message_spec: &MessageSpec,
        _from: EndpointId,
        cmd: command::Command,
    ) -> Result<(), IrcError> {
        if let ConnectionStatus::Connected(client) = &*self.client.lock().await
        {
            if let Some(cmd_prefix) =
                self.endpoint_config.command_prefixes.get(0)
            {
                let msg = format!(
                    "\x0311{}\x0f{}: {}{} {}",
                    prefix,
                    cmd.nickname.to_irc(),
                    cmd_prefix,
                    cmd.cmd,
                    cmd.args.to_irc()
                );
                self.send_privmsg(client, message_spec, &msg).await?;
            }
        }
        Ok(())
    }

    async fn recv_outer_events(&self) -> Result<Void, IrcError> {
        let mut receiver = self.outer_event_receiver.lock().await;
        loop {
            if let Some(ev) = receiver.recv().await {
                if !ev.validate_ttl() {
                    warn!(
                        "Discarding old event at endpoint {}",
                        self.endpoint_id
                    );
                    continue;
                }
                info!("Endpoint {} receieved event {:?}", self.endpoint_id, ev);
                match ev {
                    DeliveredEvent::EndpointEvent {
                        from,
                        message_spec,
                        payload,
                        prefix,
                        ..
                    } => match payload {
                        DeliveredEventPayload::RconEvent(re) => {
                            self.process_rcon_event(
                                prefix,
                                message_spec,
                                from,
                                re,
                            )
                            .await?;
                        }
                        DeliveredEventPayload::IrcEvent(ie) => {
                            self.process_irc_event(
                                prefix,
                                &message_spec,
                                from,
                                ie,
                            )
                            .await?;
                        }
                        DeliveredEventPayload::DiscordEvent(ev) => {
                            self.process_discord_event(
                                prefix,
                                &message_spec,
                                from,
                                ev,
                            )
                            .await?;
                        }
                        DeliveredEventPayload::Command(cmd) => {
                            self.process_command(
                                prefix,
                                &message_spec,
                                from,
                                cmd,
                            )
                            .await?;
                        }
                    },
                    DeliveredEvent::ReactorMessage {
                        message_spec,
                        message,
                        ..
                    } => {
                        self.process_reactor_message(&message_spec, message)
                            .await?
                    }
                }
            } else {
                return Err(IrcError::InternalError);
            }
        }
    }

    async fn get_user_id(&self, nick: &str) -> usize {
        let map = &mut self.usermap.lock().await;
        if let Some(id) = map.get_by_right(nick) {
            return *id;
        }
        let mut maxid = self.maxid.lock().await;
        *maxid += 1;
        map.insert(*maxid, String::from(nick));
        *maxid
    }

    async fn get_nickname_by_id(&self, id: usize) -> Option<String> {
        let map = self.usermap.lock().await;
        map.get_by_left(&id).map(String::from)
    }

    async fn process_chat_message(
        &self,
        event_sender: &PanicSender<ForwardedEvent>,
        nickname: &str,
        message_nick: String,
        channel: String,
        message_body: String,
    ) {
        lazy_static! {
            static ref ACTION_RE: Regex =
                Regex::new("^\u{1}ACTION (.*)\u{1}$").unwrap();
        }
        let processed_body;
        let is_action;
        if let Some(caps) = ACTION_RE.captures(&message_body) {
            processed_body = String::from(caps.get(1).unwrap().as_str());
            is_action = true;
        } else {
            processed_body = message_body;
            is_action = false;
        }
        let processed_body =
            DPString::parse_irc(&processed_body.replace('\u{200b}', ""));
        let mut is_command = false;
        if !is_action {
            let user_id = self.get_user_id(&message_nick).await;
            let message_spec = if channel == nickname {
                MessageSpec::Private(user_id)
            } else {
                // TODO: if this message isn't a command then this clone is
                // just a waste. Must optimize.
                MessageSpec::Channel(String::from(&channel[1..]))
            };
            if let Some(cmd) = command::Command::maybe_from(
                &self.endpoint_config.command_prefixes,
                self.get_user_id(&message_nick).await,
                DPString::parse_irc(&message_nick),
                message_spec,
                processed_body.clone(),
            ) {
                is_command = true;
                event_sender.send(ForwardedEvent::new_command(
                    self.endpoint_id.clone(),
                    cmd,
                ));
            }
        }
        if !is_command {
            let ev = if channel == nickname {
                if is_action {
                    IrcEvent::PrivateAction(message_nick, processed_body)
                } else {
                    IrcEvent::PrivateMessage(message_nick, processed_body)
                }
            } else if is_action {
                IrcEvent::Action(
                    MessageSpec::Channel(channel[1..].into()),
                    message_nick,
                    processed_body,
                )
            } else {
                IrcEvent::Message(
                    MessageSpec::Channel(channel[1..].into()),
                    message_nick,
                    processed_body,
                )
            };
            self.send_event(event_sender, ev);
        }
    }

    async fn run_connection(
        &self,
        event_sender: &PanicSender<ForwardedEvent>,
    ) -> Result<Void, IrcError> {
        // TODO: check if we are still on all channels we need periodically
        // TODO: handle kicks
        let quakenet_auth = self.config.quakenet_auth.clone();
        let mut client =
            Client::from_config(self.config.clone().into_config()).await?;
        client.identify()?;
        // let mut motd_ended = false;

        let mut stream = client.stream()?;
        let nickname = String::from(client.current_nickname());
        {
            *self.client.lock().await = ConnectionStatus::Connected(client);
            *self.maxid.lock().await = 1;
            self.usermap.lock().await.clear();
        }
        while let Some(message) = stream.next().await.transpose()? {
            let maybe_nick = message.source_nickname().map(String::from);
            if let Command::PRIVMSG(channel, message_body) = message.command {
                if let Some(nick) = maybe_nick {
                    self.process_chat_message(
                        event_sender,
                        &nickname,
                        nick,
                        channel,
                        message_body,
                    )
                    .await;
                }
            } else if let Command::NOTICE(channel, message_body) =
                message.command
            {
                if let Some(nick) = maybe_nick {
                    self.process_chat_message(
                        event_sender,
                        &nickname,
                        nick,
                        channel,
                        message_body,
                    )
                    .await;
                }
            } else if let Command::JOIN(
                chanlist,
                _maybe_chankeys,
                _maybe_realname,
            ) = message.command
            {
                if let Some(nick) = maybe_nick {
                    for chan in chanlist.split(',') {
                        self.send_event(
                            event_sender,
                            IrcEvent::Join(
                                MessageSpec::Channel(chan[1..].into()),
                                nick.clone(),
                            ),
                        );
                    }
                }
            } else if let Command::PART(chanlist, maybe_comment) =
                message.command
            {
                if let Some(nick) = maybe_nick {
                    for chan in chanlist.split(',') {
                        self.send_event(
                            event_sender,
                            IrcEvent::Part(
                                MessageSpec::Channel(chan[1..].into()),
                                nick.clone(),
                                maybe_comment.clone(),
                            ),
                        );
                    }
                }
            } else if let Command::QUIT(_maybe_comment) = message.command {
                // TODO: gotta find some way to handle this gracefully,
                // TODO: most likely we need to track precise user list for each channel
                debug!("User {:?} quit Irc", maybe_nick);
            } else if let Command::Response(Response::RPL_ENDOFMOTD, _) =
                message.command
            {
                // motd_ended = true;
                if let Some(ref a) = quakenet_auth {
                    self.do_quakenet_auth(a).await?;
                }
            } else if let Command::Response(Response::ERR_NOMOTD, _) =
                message.command
            {
                // motd_ended = true;
                if let Some(ref a) = quakenet_auth {
                    self.do_quakenet_auth(a).await?;
                }
            } else {
                debug!("Irc COMMAND {:?}", message.command);
            }
        }
        Err(IrcError::Disconnect)
    }
}

#[async_trait]
impl Endpoint for IrcConnection {
    fn get_endpoint_id(&self) -> &str {
        &self.endpoint_id
    }

    async fn disconnected_since(&self) -> Option<Instant> {
        match *self.client.lock().await {
            ConnectionStatus::Connected(_) => None,
            ConnectionStatus::Disconnected(i) => Some(i),
        }
    }

    async fn run(
        &self,
        forwarded_event_sender: &PanicSender<ForwardedEvent>,
    ) -> Result<Void, ReactorError> {
        const RECONNECT_PERIOD: u64 = 10;

        loop {
            let result = tokio::select! {
                res = self.run_connection(forwarded_event_sender) => res,
                res = self.recv_outer_events() => res
            };
            {
                *self.client.lock().await = Default::default();
            }
            info!(
                "IRC endpoint {} disconnected: {:?}",
                self.endpoint_id, result
            );

            sleep(Duration::from_secs(RECONNECT_PERIOD)).await;
        }
    }
}

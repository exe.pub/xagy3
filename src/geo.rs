use maxminddb::{geoip2, Reader};
use std::hash::{Hash, Hasher};
use std::net::IpAddr;
use tracing::warn;

#[derive(Debug, Clone, Default)]
pub struct Geoip2CityData {
    pub continent: Option<(String, String)>, // (code, name)
    pub country: Option<(String, String)>,   // (code, name)
    pub location: Option<(u16, f64, f64)>,   // (radius, latitude, longitude)
    pub city: Option<String>,
    pub subdivisions: Vec<(String, String)>, // [(code, name)]
}

impl PartialEq for Geoip2CityData {
    fn eq(&self, other: &Self) -> bool {
        (self.continent == other.continent)
            && (self.country == other.country)
            && (self.city == other.city)
            && (self.subdivisions == other.subdivisions)
    }
}

impl Eq for Geoip2CityData {}

impl Hash for Geoip2CityData {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.continent.hash(state);
        self.country.hash(state);
        self.city.hash(state);
        self.subdivisions.hash(state);
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Geoip2AsnData {
    pub asn_id: u32,
    pub asn_name: String,
}

pub fn lookup_asn<T: AsRef<[u8]>>(
    r: &Reader<T>,
    ip: IpAddr,
) -> Option<Geoip2AsnData> {
    match r.lookup::<geoip2::Asn>(ip) {
        Ok(res) => res.autonomous_system_number.and_then(|asn_id| {
            res.autonomous_system_organization
                .map(|name| Geoip2AsnData {
                    asn_id,
                    asn_name: String::from(name),
                })
        }),
        Err(e) => {
            warn!("Error during Geoip2 Asn lookup for ip {}: {}", ip, e);
            None
        }
    }
}

pub fn lookup_city<T: AsRef<[u8]>>(
    r: &Reader<T>,
    ip: IpAddr,
) -> Geoip2CityData {
    // Welcome to Option hell. Enjoy your time
    match r.lookup::<geoip2::City>(ip) {
        Ok(res) => {
            let continent = res.continent.and_then(|c| {
                c.code.and_then(|code| {
                    c.names.and_then(|names| {
                        names.get("en").map(|name| {
                            (String::from(code), String::from(*name))
                        })
                    })
                })
            });
            let country = res.country.and_then(|c| {
                c.iso_code.and_then(|code| {
                    c.names.and_then(|names| {
                        names.get("en").map(|name| {
                            (String::from(code), String::from(*name))
                        })
                    })
                })
            });
            let location = res.location.and_then(|l| {
                l.accuracy_radius.and_then(|radius| {
                    l.latitude.and_then(|latitude| {
                        l.longitude
                            .map(|longitude| (radius, latitude, longitude))
                    })
                })
            });
            let city = res.city.and_then(|c| {
                c.names
                    .and_then(|names| names.get("en").map(|x| String::from(*x)))
            });
            let subdivisions = match res.subdivisions {
                Some(subdivs) => subdivs
                    .iter()
                    .filter_map(|subdiv| {
                        subdiv.iso_code.and_then(|code| {
                            subdiv.names.as_ref().and_then(|names| {
                                names.get("en").map(|name| {
                                    (String::from(code), String::from(*name))
                                })
                            })
                        })
                    })
                    .collect(),
                None => Vec::new(),
            };
            Geoip2CityData {
                continent,
                country,
                location,
                city,
                subdivisions,
            }
        }
        Err(e) => {
            warn!("Error during Geoip2 City lookup for ip {}: {}", ip, e);
            Default::default()
        }
    }
}

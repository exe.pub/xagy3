use argon2::{
    password_hash::{rand_core::OsRng, PasswordHasher, SaltString},
    Argon2,
};
use inquire::{
    validator::Validation, CustomType, Password, PasswordDisplayMode, Text,
};
use sqlx::PgPool;
use tracing::Level;
use tracing_subscriber::FmtSubscriber;
use xagy3::config;
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(author)]
#[structopt(about)]
struct Opt {
    #[structopt(short, long, default_value = "./config.toml")]
    config: std::path::PathBuf
}

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    let args = Opt::from_args();
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();
    tracing::subscriber::set_global_default(subscriber)
        .expect("setting default subscriber failed");
    let raw_config = std::fs::read_to_string(&args.config)?;
    let config: config::Config = toml::from_str(&raw_config)?;
    let db_pool = PgPool::connect(&config.db_url).await?;
    let username_validator = |input: &str| {
        if input.chars().count() < 3 || input.chars().count() > 32 {
            Ok(Validation::Invalid(
                "Username must be of length 4..32 characters".into(),
            ))
        } else {
            Ok(Validation::Valid)
        }
    };
    let username = Text::new("Username:")
        .with_validator(username_validator)
        .prompt()?;
    let password_validator = |input: &str| {
        if input.chars().count() < 7 || input.chars().count() > 32 {
            Ok(Validation::Invalid(
                "Username must be of length 8..32 characters".into(),
            ))
        } else {
            Ok(Validation::Valid)
        }
    };
    let password = Password::new("Password:")
        .with_display_toggle_enabled()
        .with_display_mode(PasswordDisplayMode::Hidden)
        .with_validator(password_validator)
        .prompt()?;

    let player_id = CustomType::<u64>::new("Player id:")
        .with_formatter(&|i| format!("{}", i))
        .with_error_message("Please type a valid number")
        .with_help_message("Player id from the database")
        .prompt()?;

    let id = uuid::Uuid::new_v4();

    let salt = SaltString::generate(&mut OsRng);
    let argon2 = Argon2::default();
    let password_hash = match argon2.hash_password(password.as_bytes(), &salt) {
        Ok(h) => h.to_string(),
        Err(e) => anyhow::bail!("Password hashing error: {}", e),
    };
    xagy3::db_utils::create_web_user(
        &db_pool,
        &id,
        &username,
        &password_hash,
        player_id as i64,
    )
    .await?;
    Ok(())
}

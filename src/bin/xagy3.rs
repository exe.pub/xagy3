use prometheus::{
    register_gauge_vec, register_int_counter_vec, register_int_gauge_vec,
};
use structopt::StructOpt;
use tracing::{trace, Level};
use tracing_subscriber::FmtSubscriber;
use xagy3::{
    config,
    reactor::{self, Metrics},
    web::run_web,
};

#[derive(StructOpt)]
#[structopt(author)]
#[structopt(about)]
struct Opt {
    #[structopt(short, long, default_value = "./config.toml")]
    config: std::path::PathBuf,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    std::env::set_var("RUST_LOG", "debug");
    env_logger::init();
    let metrics = Metrics {
        number_of_players: register_int_gauge_vec!(
            "number_of_players",
            "total number of players",
            &["endpoint_id"]
        )?,
        number_of_active_players: register_int_gauge_vec!(
            "number_of_active_players",
            "number of active players (non-spectators)",
            &["endpoint_id"]
        )?,
        number_of_chat_messages: register_int_counter_vec!(
            "number_of_chat_messages",
            "number of chat messages sent from an endpoint",
            &["endpoint_id"]
        )?,
        xonotic_cpu: register_gauge_vec!(
            "xonotic_cpu",
            "xonotic CPU usage (from status)",
            &["endpoint_id"]
        )?,
        xonotic_lost: register_gauge_vec!(
            "xonotic_lost",
            "xonotic lost (from status)",
            &["endpoint_id"]
        )?,
        xonotic_offset_avg: register_gauge_vec!(
            "xonotic_offset_avg",
            "xonotic offset avg (from status)",
            &["endpoint_id"]
        )?,
        xonotic_offset_max: register_gauge_vec!(
            "xonotic_offset_max",
            "xonotic offset max (from status)",
            &["endpoint_id"]
        )?,
        xonotic_offset_sdev: register_gauge_vec!(
            "xonotic_offset_sdev",
            "xonotic offset sdev (from status)",
            &["endpoint_id"]
        )?,
        number_of_records: register_int_counter_vec!(
            "number_of_records",
            "number of records set on a xonotic server",
            &["endpoint_id", "pos"]
        )?,
        xonotic_maps: register_int_gauge_vec!(
            "xonotic_maps",
            "total time spent on a map on an endpoint",
            &["endpoint_id", "map"],
        )?,
    };

    let args = Opt::from_args();
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();
    tracing::subscriber::set_global_default(subscriber)
        .expect("setting default subscriber failed");
    let raw_config = std::fs::read_to_string(&args.config)?;
    let config: config::Config = toml::from_str(&raw_config)?;
    trace!("CONFIG: {:#?}", config);
    let web_config = config.web.clone();
    let reactor = reactor::Reactor::new(config, metrics).await?;
    let db_pool = reactor.globals.db_pool.clone();
    let endpoints = reactor.endpoints.clone();
    if let Some(web_config) = web_config {
        tokio::select! {
            _ = reactor.run() => (),
            _ = run_web(endpoints, db_pool, web_config) => ()
        }
    } else {
        reactor.run().await?;
    }
    Ok(())
}

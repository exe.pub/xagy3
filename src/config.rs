use crate::endpoint::EndpointId;
use crate::events::{MessageSpec, TargetSpec};
use bimap::BiMap;
use serde::Deserialize;
use serde_with::{serde_as, DisplayFromStr};
use std::collections::HashMap;
use std::net::SocketAddr;
use std::path::PathBuf;

#[derive(Debug, Deserialize, Clone)]
#[serde(tag = "type")]
pub enum Connection {
    Xonotic {
        addr: SocketAddr,
        secure: u8,
        password: String,
        log_listen_addr: SocketAddr,
        public_addr: String,
        logfile: Option<PathBuf>,
    },
    Irc {
        server: String,
        nickname: String,
        realname: String,
        channels: Vec<String>,
        #[serde(default)]
        quakenet_auth: Option<String>,
    },
    Discord {
        token: String,
        channels: BiMap<String, u64>,
        self_id: u64,
    },
}

#[serde_as]
#[derive(Debug, Deserialize, Clone)]
pub struct RoutingRuleInput {
    pub prefix: String,
    #[serde_as(as = "Vec<DisplayFromStr>")]
    #[serde(default)]
    pub from_channels: Vec<MessageSpec>,
    #[serde(default)]
    pub include_types: Vec<String>,
    #[serde(default)]
    pub exclude_types: Vec<String>,
}

#[serde_as]
#[derive(Debug, Deserialize, Clone)]
pub struct RoutingRuleOutput {
    #[serde_as(as = "Vec<DisplayFromStr>")]
    pub targets: Vec<TargetSpec>,
    pub prefix: String,
}

#[derive(Debug, Deserialize, Clone)]
pub struct RoutingRule {
    pub input: RoutingRuleInput,
    pub output: Vec<RoutingRuleOutput>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Endpoint {
    pub connection: Connection,
    pub routing_rules: Vec<RoutingRule>,
    pub command_prefixes: Vec<String>,
    #[serde(default)]
    pub disabled: bool,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Web {
    pub listen_on: SocketAddr,
    pub secret_key: String,
    pub metrics_auth: String,
}

#[derive(Debug, Deserialize)]
pub struct Config {
    pub endpoints: HashMap<EndpointId, Endpoint>,
    pub db_url: String,
    pub geodb_city_path: PathBuf,
    pub geodb_asn_path: PathBuf,
    pub web: Option<Web>,
}

use crate::command::{public_reply, Command, CommandState};
use crate::endpoint::EndpointId;
use crate::reactor::EndpointRef;
use dpcolors::DPString;
use rand::prelude::*;
use rand::seq::SliceRandom;
use std::collections::HashMap;
use std::future::Future;
use std::pin::Pin;
use std::sync::Arc;

async fn ask_inner(
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Option<CommandState> {
    let s = cmd.args.to_none();
    let trimmed = s.trim();
    let msg = {
        let mut rng = rand::thread_rng();
        if trimmed.ends_with('?') {
            if trimmed.len() > 5 {
                format!(
                    "{} {}",
                    ANSWERS_PART1.choose(&mut rng).unwrap(),
                    ANSWERS_PART2.choose(&mut rng).unwrap()
                )
            } else {
                String::from(*ANSWERS_TOO_SHORT.choose(&mut rng).unwrap())
            }
        } else {
            String::from(*ANSWERS_NOT_A_QUESTION.choose(&mut rng).unwrap())
        }
    };
    public_reply(&cmd.message_spec, endpoint, endpoints, DPString::parse(&msg)).await;
    None
}

pub fn ask(
    _pool: sqlx::PgPool,
    _state: CommandState,
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Pin<Box<dyn Future<Output = Option<CommandState>> + Send>> {
    Box::pin(ask_inner(endpoint, endpoints, cmd))
}

async fn praise_inner(
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Option<CommandState> {
    let msg = {
        let mut rng = rand::thread_rng();
        let name = if cmd.args.to_none().trim().is_empty() {
            cmd.nickname.to_dp()
        } else {
            cmd.args.to_dp()
        };
        format!(
            "{} ^7is {} {} {}",
            name,
            PRAISES.choose(&mut rng).unwrap(),
            COMPARISONS.choose(&mut rng).unwrap(),
            ANIMALS.choose(&mut rng).unwrap()
        )
    };
    public_reply(&cmd.message_spec, endpoint, endpoints, DPString::parse(&msg)).await;
    None
}

pub fn praise(
    _pool: sqlx::PgPool,
    _state: CommandState,
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Pin<Box<dyn Future<Output = Option<CommandState>> + Send>> {
    Box::pin(praise_inner(endpoint, endpoints, cmd))
}

async fn excuse_inner(
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Option<CommandState> {
    let msg = {
        let mut rng = rand::thread_rng();
        let name = if cmd.args.to_none().trim().is_empty() {
            cmd.nickname.to_dp()
        } else {
            cmd.args.to_dp()
        };
        let f: f32 = rng.gen();
        let total_excuses =
            (EXCUSE_NOUNS.len() + EXCUSE_EVENTS.len() + EXCUSE_REASONS.len()) as f32;
        let np = EXCUSE_NOUNS.len() as f32 / total_excuses;
        let ne = EXCUSE_EVENTS.len() as f32 / total_excuses;
        // let nr = EXCUSE_REASONS.len() as f32 / total_excuses;
        if f < np {
            // NOUN-type excuse
            let f: f32 = rng.gen();
            let noun = EXCUSE_NOUNS.choose(&mut rng).unwrap();
            if f < 0.25 {
                format!("{name} would certainly have done better but {noun} happened.")
            } else if f < 0.5 {
                format!("Unfortunately {noun} seriously let {name} down.")
            } else if f < 0.75 {
                format!("That's all because of {noun}, {name}, don't be sad.")
            } else {
                format!("{name} was having such an amazing day until {noun} happened.")
            }
        } else if f < np + ne {
            // EVENT-type excuse
            let f: f32 = rng.gen();
            let sentence = EXCUSE_EVENTS.choose(&mut rng).unwrap();
            if f < 0.25 {
                format!("Everything was going so well for {name} and then {sentence}.")
            } else if f < 0.5 {
                format!("{name} tried his best but alas {sentence}.")
            } else if f < 0.75 {
                format!("Eh, {sentence} and {name} failed, such a sad story.")
            } else {
                format!("Wow, {sentence}! Poor {name}")
            }
        } else {
            let f: f32 = rng.gen();
            let reason = EXCUSE_REASONS.choose(&mut rng).unwrap();
            if f < 0.25 {
                format!("{name}, it's pointless to try when {reason}")
            } else if f < 0.5 {
                format!("Oops, {reason}, {name}, can't help with that.")
            } else if f < 0.75 {
                format!("You can't do much when {reason}, {name}.")
            } else {
                format!("Wow, {name}, {reason}!")
            }
        }
    };
    public_reply(&cmd.message_spec, endpoint, endpoints, DPString::parse(&msg)).await;
    None
}

pub fn excuse(
    _pool: sqlx::PgPool,
    _state: CommandState,
    endpoint: EndpointRef,
    endpoints: Arc<HashMap<EndpointId, EndpointRef>>,
    cmd: Command,
) -> Pin<Box<dyn Future<Output = Option<CommandState>> + Send>> {
    Box::pin(excuse_inner(endpoint, endpoints, cmd))
}

const ANSWERS_PART1: [&str; 6] = [
    "Good question.",
    "Let me think.",
    "This is really tough one.",
    "Only a great mind can ask such a question.",
    "Even Einstein would struggle here.",
    "Let me consult with my records.",
];

const ANSWERS_PART2: [&str; 25] = [
    "I'd say outlook is good.",
    "I guess outlook is not so good.",
    "I think outlook is grim.",
    "I'd say yes.",
    "I'd say no.",
    "I'm not sure",
    "I'm sure that it's true.",
    "I'm sure that's a lie.",
    "I honestly don't know.",
    "Yes, of course.",
    "Of course not.",
    "Oh, I assure you that it's true.",
    "I need to consult with my bosses about this matter.",
    "Sadly, it's totally impossible.",
    "I's say events like that definitely happen but not too often.",
    "But why do you ask?",
    "Abslolutely.",
    "I think you should ask Des about it.",
    "Oops, my RNG is broken, please try again later.",
    "The answer is positive.",
    "The answer is negative.",
    "Probability is high but I'm not sure.",
    "I'd say hardly but who knows.",
    "Can't see it coming but miracles do happen.",
    "Surely yes. Or maybe..."
];

const ANSWERS_TOO_SHORT: [&str; 3] = [
    "Could you elaborate your question please?",
    "Could you tell us more details about the situation?",
    "We need more information to reason on this.",
];

const ANSWERS_NOT_A_QUESTION: [&str; 2] = [
    "Sorry, I'm not sure how to comment on this.",
    "If you want to ask something just ask, don't hesitate.",
];

const PRAISES: [&str; 83] = [
    "cute",
    "active",
    "adaptable",
    "adventurous",
    "affable",
    "amiable",
    "amicable",
    "amusing",
    "brave",
    "bright",
    "calm",
    "careful",
    "charming",
    "circumspect",
    "communicative",
    "compassionate",
    "conscientious",
    "considerate",
    "courageous",
    "courteous",
    "creative",
    "determined",
    "diligent",
    "diplomatic",
    "discreet",
    "dynamic",
    "empathetic",
    "energetic",
    "faithful",
    "fearless",
    "friendly",
    "funny",
    "generous",
    "gentle",
    "good",
    "gregarious",
    "helpful",
    "honest",
    "hopeful",
    "humorous",
    "imaginative",
    "intelligent",
    "intuitive",
    "inventive",
    "joyful",
    "kind",
    "loving",
    "loyal",
    "lucky",
    "mature",
    "motivated",
    "neat",
    "nice",
    "optimistic",
    "passionate",
    "patient",
    "plucky",
    "polite",
    "popular",
    "powerful",
    "practical",
    "rational",
    "realistic",
    "reliable",
    "resourceful",
    "romantic",
    "sensible",
    "sincere",
    "smart",
    "sociable",
    "sympathetic",
    "tidy",
    "understanding",
    "willing",
    "wise",
    "witty",
    "graceful",
    "fast",
    "strong",
    "noble",
    "cunning",
    "sharp-sighted",
    "hardworking",
];

const ANIMALS: [&str; 58] = [
    "a deer",
    "a cheetah",
    "an ox",
    "noone else",
    "a wolf",
    "an octopus",
    "a fox",
    "a bear",
    "a lion",
    "an ant",
    "an elephant",
    "a rhino",
    "an ostrich",
    "an owl",
    "a raven",
    "an eagle",
    "a shark",
    "an aper",
    "an elk",
    "a squirrel",
    "a moose",
    "an ibex",
    "a camel",
    "a mouse",
    "a rat",
    "a panda",
    "a kangaroo",
    "a mustang",
    "a donkey",
    "a tiger",
    "a panther",
    "a manul",
    "a polecat",
    "a fennec fox",
    "a swan",
    "a bilby",
    "a sea otter",
    "a margay",
    "an elephant shrew",
    "a quokka",
    "an antelope",
    "a klipspringer",
    "a weasel",
    "a pika",
    "a quoll",
    "a capybara",
    "a tarsier",
    "a mole",
    "a hippopotamus",
    "a sand cat",
    "an arctic fox",
    "a pygmy goat",
    "an aardwolf",
    "a racoon dog",
    "a giraffe",
    "a gerenuk",
    "an anteater",
    "a hare",
];

const COMPARISONS: [&str; 2] = ["like", "as"];

// NAME would certainly have done better but NOUN happened.
// Unfortunately NOUN seriously let NAME down
// NAME tried their best but alas SENTENCE
// That's all because of NOUN, NAME, don't be sad.
// Everything went so well for NAME and then SENTENCE.
// NAME had such an amazing day until NOUN happened.
// Noone can really handle NOUN. Even NAME.
// Wow, SENTENCE. Poor NAME.

const EXCUSE_NOUNS: &[&str] = &[
    "solar flares",
    "electromagnetic radiation from satellite debris",
    "global warming",
    "static buildup",
    "doppler effect",
    "temporary routing anomaly",
    "dry joints on cable plug",
    "floating point processor overflow",
    "terrorist activities",
    "piezo-electric interference",
    "heavy gravity fluctuation",
    "radiosity depletion",
    "cellular telephone interference",
    "positron router malfunction",
    "interrupt configuration error",
    "virus attack",
    "high pressure system failure",
    "bit bucket overflow",
    "electromagnetic energy loss",
    "budget cuts",
    "Internet outage",
    "SIMM crosstalk.",
    "IRQ dropout",
    "electro-magnetic pulses from French above ground nuke testing",
    "sticky bits on disk",
    "new management",
    "BNC (brain not connected) Error",
    "UBNC (user brain not connected) Error",
    "dew on the cable lines",
    "big to little endian conversion error",
    "too many zombie processes",
    "incorrect time synchronization",
    "defunct processes",
    "non-redundant fan failure",
    "monitor VLF leakage",
    "keyboard missing the \"any\" key",
    "backbone scoliosis",
    "broadcast packets on wrong frequency",
    "lightning strikes",
    "vapors from evaporating sticky-note adhesives",
    "recursive traversal of loopback mount points",
    "endothermal recalibration",
    "high nuclear activity in the area",
    "ionization from the air-conditioning",
    "interference from lunar radiation",
    "interference between the keyboard and the chair",
    "too many interrupts",
    "not enough interrupts",
    "quantum decoherence",
    "incompatible bit-registration operators",
    "asynchronous inode failure",
    "transient bus protocol violation",
    "scheduled global CPU outage",
    "high line impedance",
    "excess condensation in cloud network",
    "astropneumatic oscillations in the water-cooling",
    "emissions from GSM-phones",
    "microelectronic Riemannian curved-space fault in write-only file system",
    "the recent proliferation of Nuclear Testing",
    "microelectronic Riemannian curved-space fault in write-only file system",
    "incorrectly configured static routes on the corerouters.",
    "failure to adjust for daylight savings time.",
    "overflow error in /dev/null",
    "increased sunspot activity.",
    "redundant ACLs",
    "radial telemetry infiltration",
    "firmware update in the coffee machine",
    "interference from the Van Allen Belt.",
];

const EXCUSE_EVENTS: &[&str] = &[
    "clock speed changed",
    "power conditioning broke",
    "credit cards caused magnetic interference",
    "Earth's rotational speed changed",
    "spaghetti cable caused packet failure",
    "waste water tank overflowed onto computer",
    "dynamic software linking table got corrupted",
    "CPU radiator wrecked",
    "hairdryer was plugged into UPS",
    "cosmic ray particles crashed through the hard disk platter",
    "smell from unhygienic janitorial staff wrecked the tape heads",
    "plumber mistook routing panel for decorative wall fixture",
    "electricians made popcorn in the power supply room",
    "groundskeepers stole the root password",
    "PC grounding got messed up",
    "CPU needs recalibration",
    "knot in cables caused data stream to become twisted and kinked",
    "nesting roaches shorted out the cable",
    "mouse chewed through power cable",
    "small animal kamikaze attack on power supplies",
    "backbone collapsed",
    "power company started to test new voltage spike creation equipment",
    "UPS interrupted the server's power",
    "the air conditioning water supply pipe ruptured over the machine room",
    "the electricity substation in the car park blew up.",
    "the rolling stones concert down the road caused a brown out",
    "the salesman drove over the CPU board",
    "someone plugged the monitor into the serial port",
    "the real ttys became pseudo ttys and vice-versa",
    "the printer thought it was a router",
    "the router thought it was a printer.",
    "power company started having EMP problems with their reactor",
    "somebody started distributing pornography on server",
    "disks started spinning backwards",
    "loop found in loop in redundant loopback",
    "system consumed all the paper for paging",
    "the Internet bill wasn't paid in time",
    "CPU started running slowly due to weak power supply",
    "kernel pointers got messed up",
    "fractal radiation jammed the backbone",
    "fiber optics caused gas main leak",
    "greenpeace free'd the mallocs",
    "stray alpha particles from memory packaging caused Hard Memory Error on server",
    "repeated reboots of the system failed to solve problem",
    "PC suffered a memory leak",
    "suspicious pointer corrupted virtual machine",
    "someone hooked the twisted pair wires into the answering machine",
    "robotic tape changer mistook operator's tie for a backup tape",
    "leap second overloaded RHEL6 servers",
    "data for intranet got routed through the extranet and landed on the internet.",
];

const EXCUSE_REASONS: &[&str] = &[
    "network lag due to too many people playing",
    "/dev/null is being upgraded",
    "ISP is having switching problems",
    "ISP is having routing problems",
    "ISP is having frame relay problems",
    "ISP employees are on strike due to broken coffee machine",
    "PC had the fuses in backwards",
    "CPU needs recalibration",
    "data on the hard drive is out of balance",
    "your CPU is not ISO 9000 compliant",
    "your processor does not develop enough heat",
    "parallel processors running perpendicular today",
    "root nameservers are out of sync",
    "we've picked COBOL as the language of choice",
    "the AA battery in the wallclock sends magnetic interference",
    "the butane lighter causes the pincushioning",
    "old inkjet cartridges emanate barium-based fumes",
    "we need a licensed electrician to replace the light bulbs in the server room",
    "root name servers corrupted",
    "chewing gum on /dev/sd3c",
    "someone was smoking in the computer room and set off the halon systems",
    "domain controller not responding",
    "maintenance window broken",
    "electrical conduits in machine room are melting",
    "sticky bit has come loose",
    "tachyon emissions are overloading the system",
    "Jupiter is aligned with Mars.",
    "route flapping at the NAP",
    "computers under water due to SYN flooding",
];

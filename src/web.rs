use crate::config;
use crate::db_utils::{
    check_password, get_web_user, query_anon_cts_records, query_cts_records,
    query_identification, query_identifications_by_date, QueriedIdentification,
    QueryType,
};
use crate::endpoint::{Endpoint, EndpointId, TypedEndpoint};
use crate::rcon::XonStatsQueryStatus;
use crate::reactor::EndpointRef;
use actix_identity::{Identity, IdentityMiddleware};
use actix_session::{storage::CookieSessionStore, SessionMiddleware};
use actix_web::middleware::Compat;
use actix_web::{
    cookie::Key,
    error, get,
    http::{header::ContentType, StatusCode},
    post, web, App, HttpMessage, HttpRequest, HttpResponse, HttpServer,
    Responder,
};
use chrono::DateTime;
use dpcolors::DPString;
use lazy_static::lazy_static;
use maud::{html, Markup, PreEscaped, DOCTYPE};
use percent_encoding::{utf8_percent_encode, NON_ALPHANUMERIC};
use serde::Deserialize;
use sqlx::PgPool;
use std::collections::HashMap;
use std::sync::Arc;
use actix_identity::error::LoginError;
use thiserror::Error;
use tracing_actix_web::TracingLogger;

type Endpoints = Arc<HashMap<EndpointId, EndpointRef>>;

#[derive(Debug, Error)]
enum WebError {
    #[error("Unauthorized")]
    Unauthorized,
    #[error("Not Found")]
    NotFound(String),
    #[error("Uuid Error: {0}")]
    UuidError(#[from] uuid::Error),
    #[error("DB Error: {0}")]
    DbError(#[from] sqlx::Error),
    #[error("Anyhow Error: {0}")]
    AnyhowError(#[from] anyhow::Error),
    #[error("Login error: {0}")]
    LoginError(#[from] LoginError),
    #[error("Prometheus Error: {0}")]
    PrometheusError(#[from] prometheus::Error),
}

impl error::ResponseError for WebError {
    fn error_response(&self) -> HttpResponse {
        let body = match self {
            WebError::NotFound(p) => not_found_page(p),
            WebError::Unauthorized => base_page(
                "anons not allowed :(",
                None,
                html! {
                    h2 {"Unauthorized"}
                    p {"Sorry, this page isn't really meant to be seen by random eyes."}
                    p {
                        "Maybe you would like to ";
                        a href="/login" {"login"}
                        "?"
                    }
                },
            ),
            e => base_page(
                "DISASTER!",
                None,
                html! {
                    h2 {"Internal server error"}
                    p {
                        "Admin too dumb to write bugless software. You can always help by submitting ";
                        a href="https://code.teichisma.info/morosophos/xagy3" {"a pull request or two"}
                        "."
                    }
                    pre {
                        (format!("ERROR: {}", e))
                    }
                },
            ),
        };
        HttpResponse::build(self.status_code())
            .insert_header(ContentType::html())
            .body(body.into_string())
    }

    fn status_code(&self) -> StatusCode {
        match self {
            WebError::Unauthorized => StatusCode::UNAUTHORIZED,
            WebError::NotFound(_) => StatusCode::NOT_FOUND,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

#[derive(Debug, Clone)]
struct User {
    username: String,
}

async fn extract_user(
    id: Option<Identity>,
    pool: &PgPool,
) -> Result<User, WebError> {
    match id.and_then(|x| x.id().ok()) {
        Some(id) => {
            let id = uuid::Uuid::parse_str(&id)?;
            if let Some(username) = get_web_user(pool, &id).await? {
                Ok(User { username })
            } else {
                Err(WebError::Unauthorized)
            }
        }
        None => Err(WebError::Unauthorized),
    }
}

#[derive(Debug, Clone, Deserialize)]
struct LoginForm {
    username: String,
    password: String,
}

#[post("/login")]
async fn login_post(
    request: HttpRequest,
    pool: web::Data<PgPool>,
    form: web::Form<LoginForm>,
) -> Result<HttpResponse, WebError> {
    Ok(
        if let Some(user) =
            check_password(&pool, &form.username, &form.password).await?
        {
            Identity::login(&request.extensions(), user.to_string())?;
            HttpResponse::Found()
                .insert_header(("location", "/"))
                .finish()
        } else {
            let data = login_page(true, Some(form.into_inner())).into_string();
            HttpResponse::Ok()
                .insert_header(("Content-Type", "text/html"))
                .body(data)
        },
    )
}

#[get("/login")]
async fn login() -> maud::Markup {
    login_page(false, None)
}

#[post("/logout")]
async fn logout(user: Identity) -> HttpResponse {
    user.logout();
    HttpResponse::Found()
        .insert_header(("location", "/"))
        .finish()
}

#[get("/")]
async fn dashboard(
    id: Option<Identity>,
    pool: web::Data<PgPool>,
    endpoints: web::Data<Endpoints>,
) -> Result<Markup, WebError> {
    let user = extract_user(id, &pool).await?;
    let mut endpoints: Vec<_> = endpoints.iter().collect();
    endpoints.sort_by(|(id1, e1), (id2, e2)| {
        match (e1.endpoint.as_ref(), e2.endpoint.as_ref()) {
            (TypedEndpoint::Xonotic(_), TypedEndpoint::Xonotic(_)) => {
                id1.cmp(id2)
            }
            (TypedEndpoint::Xonotic(_), _) => std::cmp::Ordering::Less,
            (_, TypedEndpoint::Xonotic(_)) => std::cmp::Ordering::Greater,
            (_, _) => id1.cmp(id2),
        }
    });
    Ok(base_page(
        "Dashboard",
        Some(user),
        html! {
            div.pure-g {
                @for (endpoint_id, endpoint_ref) in endpoints {
                    div."pure-u-8-24" {
                        (endpoint_brief(endpoint_id, endpoint_ref).await)
                    }
                }
            }
        },
    ))
}

#[derive(Debug, Deserialize, Clone, Copy)]
struct DateRangeQuery {
    start: DateTime<chrono::Utc>,
    end: DateTime<chrono::Utc>,
}

impl Default for DateRangeQuery {
    fn default() -> Self {
        let now = chrono::Utc::now();
        DateRangeQuery {
            start: now,
            end: now - chrono::Duration::days(1),
        }
    }
}

impl DateRangeQuery {
    fn add(self, d: chrono::Duration) -> Self {
        Self {
            start: self.start + d,
            end: self.end + d,
        }
    }

    fn next_day(self) -> Self {
        self.add(chrono::Duration::days(1))
    }

    fn prev_day(self) -> Self {
        self.add(chrono::Duration::days(-1))
    }
}

fn pe(q: &str) -> String {
    utf8_percent_encode(q, NON_ALPHANUMERIC).to_string()
}

#[get("/endpoint/{endpoint_id}")]
async fn endpoint_page(
    pool: web::Data<PgPool>,
    endpoints: web::Data<Endpoints>,
    id: Option<Identity>,
    path: web::Path<String>,
    date_range: Option<web::Query<DateRangeQuery>>,
) -> Result<Markup, WebError> {
    let dr = date_range.map(|x| x.into_inner()).unwrap_or_default();
    let user = extract_user(id, &pool).await?;
    let endpoint_id = path.into_inner();
    let ids =
        query_identifications_by_date(&pool, &endpoint_id, dr.start, dr.end)
            .await?;
    if let Some(endpoint_ref) = endpoints.get(&endpoint_id) {
        let online = endpoint_ref.endpoint.disconnected_since().await.is_none();
        Ok(match endpoint_ref.endpoint.as_ref() {
            TypedEndpoint::Xonotic(con) => {
                let state = con.state.read().await;
                let players = con.players.read().await;
                base_page(
                    &endpoint_id,
                    Some(user),
                    html! {
                        h3.endpoint_id.online[online].offline[!online] {
                            (endpoint_id)
                        }
                        p {
                            "Playing ";
                            strong {(state.game.mode.to_long_str())}
                            " on ";
                            strong {(state.game.map)}
                        }
                        p {
                            (format!("{:.2}", state.resources.cpu)) "% CPU ";
                            (format!("{:.2}", state.resources.lost)) "% Lost";
                            " offset avg " (format!("{:.2}", state.resources.offset_avg));
                            " max " (format!("{:.2}", state.resources.offset_max));
                            " sdev " (format!("{:.2}", state.resources.offset_sdev));
                        }
                        @if online {
                            h4 {"Currently playing"}
                            code {
                                (format!("{:?}", players.slot_to_entity))
                            }
                            @if players.is_empty() {
                                p {"No one :("}
                            }
                            @for (_, player) in players.iter() {
                                div.active-player {
                                    p {
                                        (format!("#{} | ", player.entity_id));
                                        (dps_copy(&player.name));
                                        " | ";
                                        @if let Some(ref c) = player.crypto_idfp {
                                            a href=(format!("/players?query_type=CryptoIdfp&query={}", pe(c))) {
                                                i.icofont-key {}
                                            }
                                        }
                                        " | ";
                                        @if let XonStatsQueryStatus::Received(_, ref d) = player.xonstats {
                                            a href=(format!("https://stats.xonotic.org/player/{}", d.player.player_id)) {(d.player.player_id)}
                                            " ";
                                            a href=(format!("/players?query_type=StatsId&query={}", d.player.player_id)) {
                                                i.icofont-search {}
                                            }
                                        }
                                    }
                                    p {
                                        (format!("{} ", player.ip));
                                        a href=(format!("/players?query_type=IpAddr&query={}", player.ip)) {
                                            i.icofont-search {}
                                        }
                                        " | ";
                                        @if let Some(ref asn) = player.asn_data {
                                            (asn.asn_id);
                                            " ";
                                            a href=(format!("/players?query_type=AsnId&query={}", asn.asn_id)) {
                                                i.icofont-search {}
                                            }
                                            ": ";
                                            (asn.asn_name);
                                            " ";
                                            a href=(format!("/players?query_type=AsnName&query={}", pe(&asn.asn_name))) {
                                                i.icofont-search {}
                                            }
                                        }
                                        @else {
                                            (PreEscaped("&mdash;"))
                                        }
                                    }
                                    p {
                                        @if let Some(ref country) = player.city_data.country {
                                            (country.1);
                                            " ";
                                            a href=(format!("/players?query_type=Country&query={}", pe(&country.0))) {
                                                i.icofont-search {}
                                            }
                                        }
                                        " | ";
                                        @if let Some(ref city) = player.city_data.city {
                                            (city)
                                                " ";
                                            a href=(format!("/players?query_type=City&query={}", pe(city))) {
                                                i.icofont-search {}
                                            }
                                        }
                                        @if !player.city_data.subdivisions.is_empty() {
                                            " | ";
                                            (player.city_data.subdivisions.iter().map(|(_, s)| s.as_str()).collect::<Vec<_>>().join("->"))
                                        }
                                    }
                                }
                            }
                        }
                        h4 {
                            "Players connnected previously"
                        }
                        @let prev = dr.prev_day();
                        @let next = dr.next_day();
                        p.date-range {
                            a href=(format!("/endpoint/{}?start={}&end={}", endpoint_id, prev.start, prev.end)) {
                                i.icofont-ui-previous {}
                            }
                            (format!(" {} - {} ",
                                     dr.end.format("%y-%m-%d %H:%M"),
                                     dr.start.format("%y-%m-%d %H:%M")));
                            @if next.end < (chrono::Utc::now() - chrono::Duration::minutes(5)) {
                                a href=(format!("/endpoint/{}?start={}&end={}", endpoint_id, next.start, next.end)) {
                                    i.icofont-ui-next {}
                                }
                            }
                        }
                        (ids_table(ids))
                    },
                )
            }
            _ => base_page(
                &endpoint_id,
                Some(user),
                html! {
                    h3.endpoint_id.online[online].offline[!online] {
                        (endpoint_id)
                    }
                    p {
                        "Not too much information about this endpoint."
                    }
                },
            ),
        })
    } else {
        Err(WebError::NotFound(endpoint_id))
    }
}

#[derive(Deserialize, Debug)]
struct PlayerSearchQuery {
    query: String,
    query_type: QueryType,
}

fn render_query_type_option(
    selected: Option<QueryType>,
    current: QueryType,
) -> Markup {
    if Some(current) == selected {
        html! {
            option selected {(format!("{:?}", current))}
        }
    } else {
        html! {
            option {(format!("{:?}", current))}
        }
    }
}

#[get("/players")]
async fn player_search_page(
    pool: web::Data<PgPool>,
    id: Option<Identity>,
    query: Option<web::Query<PlayerSearchQuery>>,
) -> Result<Markup, WebError> {
    let user = extract_user(id, &pool).await?;
    let ids = if let Some(ref q) = query {
        query_identification(&pool, q.query_type, &q.query).await?
    } else {
        Vec::new()
    };
    let query_type = query.as_ref().map(|q| q.query_type);
    Ok(base_page(
        "Player search",
        Some(user),
        html! {
            h3 {"Search results"}
            form.pure-form method="get" {
                fieldset {
                    select name="query_type" {
                        (render_query_type_option(query_type, QueryType::Nickname));
                        (render_query_type_option(query_type, QueryType::CryptoIdfp));
                        (render_query_type_option(query_type, QueryType::StatsId));
                        (render_query_type_option(query_type, QueryType::AsnId));
                        (render_query_type_option(query_type, QueryType::AsnName));
                        (render_query_type_option(query_type, QueryType::IpAddr));
                        (render_query_type_option(query_type, QueryType::Country));
                        (render_query_type_option(query_type, QueryType::City));
                    }
                    " ";
                    @if let Some(q) = query {
                        input autofocus name="query" value=(q.query);
                    }
                    @else {
                        input autofocus name="query" placeholder="Search...";
                    }
                    " ";
                    button.pure-button type="submit" {"Search"}
                }
            }
            (ids_table(ids))
        },
    ))
}

fn ids_table(
    ids: Vec<(QueriedIdentification, Vec<DateTime<chrono::Utc>>)>,
) -> Markup {
    html! {
        @for (id, dates) in ids {
            div.identity-list-item {
                p {
                    (dps_copy(&id.nickname))
                        " | ";
                    @if let Some(ref c) = id.crypto_idfp {
                        a href=(format!("/players?query_type=CryptoIdfp&query={}", pe(c))) {
                            i.icofont-key {}
                        }
                    }
                    " | ";
                    @if let Some(s_id) = id.stats_id {
                        a href=(format!("https://stats.xonotic.org/player/{}", s_id)) {(s_id)}
                        " ";
                        a href=(format!("/players?query_type=StatsId&query={}", s_id)) {
                            i.icofont-search {}
                        }
                    }
                    (PreEscaped(" &mdash; "));
                    @let min_d = dates.iter().min().unwrap();
                    @let max_d = dates.iter().max().unwrap();
                    (dates.len());
                    " ids; ";
                    (format!("{}", min_d.format("%y-%m-%d %H:%M")));
                    " - ";
                    (format!("{}", max_d.format("%y-%m-%d %H:%M")));
                }
                p {
                    (format!("{} ", id.ip_addr));
                    a href=(format!("/players?query_type=IpAddr&query={}", id.ip_addr)) {
                        i.icofont-search {}
                    }
                    " | ";
                    @if let Some(asn_id) = id.asn_id {
                        (asn_id);
                        " ";
                        a href=(format!("/players?query_type=AsnId&query={}", asn_id)) {
                            i.icofont-search {}
                        }
                        ": ";
                    }
                    @if let Some(asn_name) = id.asn_name {
                        (asn_name);
                        " ";
                        a href=(format!("/players?query_type=AsnName&query={}", pe(&asn_name))) {
                            i.icofont-search {}
                        }
                    }
                    @else {
                        (PreEscaped("&mdash;"))
                    }
                }
                p {
                    @if let Some(ref country) = id.country {
                        (country);
                        " ";
                        a href=(format!("/players?query_type=Country&query={}", pe(country))) {
                            i.icofont-search {}
                        }
                    }
                    @if let Some(ref city) = id.city {
                        " | ";
                        (city);
                        " ";
                        a href=(format!("/players?query_type=City&query={}", pe(city))) {
                            i.icofont-search {}
                        }
                    }
                    @if let Some(subdivisions) = id.subdivisions {
                        " | ";
                        (subdivisions)
                    }
                }
            }
        }
    }
}

#[get("/cts-records")]
async fn get_cts_records(
    pool: web::Data<PgPool>,
    _endpoints: web::Data<Endpoints>,
    id: Option<Identity>,
    date_range: Option<web::Query<DateRangeQuery>>,
) -> Result<Markup, WebError> {
    let dr = date_range.map(|x| x.into_inner()).unwrap_or_default();
    let user = extract_user(id, &pool).await?;
    let records = query_cts_records(&pool, dr.start, dr.end).await?;
    Ok(base_page(
        "CTS Records",
        Some(user),
        html! {
            h4 {"CTS Records"}
            @let prev = dr.prev_day();
            @let next = dr.next_day();
            p.date-range {
                a href=(format!("/cts-records?start={}&end={}", prev.start, prev.end)) {
                    i.icofont-ui-previous {}
                }
                (format!(" {} - {} ",
                         dr.end.format("%y-%m-%d %H:%M"),
                         dr.start.format("%y-%m-%d %H:%M")));
                @if next.end < (chrono::Utc::now() - chrono::Duration::minutes(5)) {
                    a href=(format!("/cts-records?start={}&end={}", next.start, next.end)) {
                        i.icofont-ui-next {}
                    }
                }
            }
            table.pure-table.pure-table-bordered {
                @for i in records {
                    tr {
                        td {
                            (i.endpoint_id)
                        }
                        td {
                            (dps_copy(&i.nickname));
                            br;
                            code {(i.crypto_idfp.as_deref().unwrap_or("--"))}
                        }
                        td {
                            (format!("{}", i.created_at.format("%y-%m-%d %H:%M:%S")))
                        }
                        td {
                            (format!("{:.2}", i.time))
                        }
                        td {(i.map)}
                        td {(format!("{}", i.ip_addr.ip()))}
                    }
                }
            }
        },
    ))
}

#[get("/cts-records-anon")]
async fn get_cts_records_anon(
    pool: web::Data<PgPool>,
    _endpoints: web::Data<Endpoints>,
    id: Option<Identity>,
    date_range: Option<web::Query<DateRangeQuery>>,
) -> Result<Markup, WebError> {
    let dr = date_range.map(|x| x.into_inner()).unwrap_or_default();
    let user = extract_user(id, &pool).await?;
    let records = query_anon_cts_records(&pool, dr.start, dr.end).await?;
    Ok(base_page(
        "Anon CTS Records",
        Some(user),
        html! {
            h4 {"Anon CTS Records"}
            @let prev = dr.prev_day();
            @let next = dr.next_day();
            p.date-range {
                a href=(format!("/cts-records-anon?start={}&end={}", prev.start, prev.end)) {
                    i.icofont-ui-previous {}
                }
                (format!(" {} - {} ",
                         dr.end.format("%y-%m-%d %H:%M"),
                         dr.start.format("%y-%m-%d %H:%M")));
                @if next.end < (chrono::Utc::now() - chrono::Duration::minutes(5)) {
                    a href=(format!("/cts-records-anon?start={}&end={}", next.start, next.end)) {
                        i.icofont-ui-next {}
                    }
                }
            }
            table.pure-table.pure-table-bordered {
                @for i in records {
                    tr {
                        td {
                            (i.endpoint_id)
                        }
                        td {
                            (dps_copy(&i.nickname));
                        }
                        td {
                            (format!("{}", i.created_at.format("%y-%m-%d %H:%M:%S")))
                        }
                        td {
                            (i.time.to_none())
                        }
                        td {(i.map)}
                    }
                }
            }
        },
    ))
}

lazy_static! {
    static ref FILES: HashMap<&'static str, (&'static str, &'static [u8])> = {
        let mut files = HashMap::new();
        files.insert(
            "main.css",
            ("text/css", &include_bytes!("../static/main.css")[..]),
        );
        files.insert(
            "font-regular.woff",
            (
                "font/woff",
                &include_bytes!("../static/font-regular.woff")[..],
            ),
        );
        files.insert(
            "font-bold.woff",
            ("font/woff", &include_bytes!("../static/font-bold.woff")[..]),
        );
        files.insert(
            "font-italic.woff",
            (
                "font/woff",
                &include_bytes!("../static/font-italic.woff")[..],
            ),
        );
        files.insert(
            "icofont/icofont.min.css",
            (
                "text/css",
                &include_bytes!("../static/icofont/icofont.min.css")[..],
            ),
        );
        files.insert(
            "icofont/fonts/icofont.woff2",
            (
                "font/woff2",
                &include_bytes!("../static/icofont/fonts/icofont.woff2")[..],
            ),
        );
        files.insert(
            "icofont/fonts/icofont.woff",
            (
                "font/woff",
                &include_bytes!("../static/icofont/fonts/icofont.woff")[..],
            ),
        );
        files
    };
}

#[get("/static/{p:.*}")]
async fn statics(r: HttpRequest, p: web::Path<String>) -> impl Responder {
    if let Some((mime_type, data)) = FILES.get(p.into_inner().as_str()) {
        HttpResponse::Ok()
            .insert_header(("Content-Type", *mime_type))
            .body(*data)
    } else {
        not_found(r).await
    }
}

#[get("/metrics")]
async fn metrics(
    r: HttpRequest,
    cfg: web::Data<config::Web>,
) -> Result<HttpResponse, WebError> {
    let auth = r.headers().get("Authorization");
    if auth.and_then(|h| h.to_str().ok()) != Some(cfg.metrics_auth.as_str()) {
        return Ok(HttpResponse::Unauthorized().body("UNAUTHORIZED!"));
    }
    let report = prometheus::TextEncoder::new()
        .encode_to_string(&prometheus::default_registry().gather())?;

    Ok(HttpResponse::Ok()
        .insert_header(("Content-Type", "text/plain"))
        .body(report))
}

pub async fn run_web(
    endpoints: Endpoints,
    pool: PgPool,
    cfg: config::Web,
) -> Result<(), std::io::Error> {
    let endpoints = web::Data::new(endpoints);
    let pool = web::Data::new(pool);
    let cfg_clone = web::Data::new(cfg.clone());
    HttpServer::new(move || {
        App::new()
            .app_data(endpoints.clone())
            .app_data(pool.clone())
            .app_data(cfg_clone.clone())
            .wrap(IdentityMiddleware::default())
            .wrap(
                SessionMiddleware::builder(
                    CookieSessionStore::default(),
                    Key::from(cfg.secret_key.as_bytes()),
                )
                .cookie_secure(false)
                .build(),
            )
            .wrap(Compat::new(TracingLogger::default()))
            .service(dashboard)
            .service(login_post)
            .service(login)
            .service(logout)
            .service(statics)
            .service(endpoint_page)
            .service(player_search_page)
            .service(get_cts_records)
            .service(get_cts_records_anon)
            .service(metrics)
            .default_service(web::to(not_found))
    })
    .bind(cfg.listen_on)?
    .workers(1)
    .run()
    .await
}

async fn endpoint_brief(
    endpoint_id: &EndpointId,
    endpoint_ref: &EndpointRef,
) -> Markup {
    let online = endpoint_ref.endpoint.disconnected_since().await.is_none();
    match endpoint_ref.endpoint.as_ref() {
        TypedEndpoint::Xonotic(con) => {
            let (mode, map, players) = {
                let state = con.state.read().await;
                (state.game.mode, state.game.map.clone(), state.game.players)
            };
            html! {
                .endpoint-brief.online[online].offline[!online] {
                    h3.endpoint_id.online[online].offline[!online] {
                        a href=(format!("/endpoint/{endpoint_id}")) {
                            (endpoint_id);
                        }
                    }
                    ul {
                        li {
                            strong {"Server: "}
                            (format!("{} / {}", con.remote.addr, con.connect_url));
                        }
                        @if online {
                            li {
                                strong {"Playing: "}
                                (format!("{} on {}", mode.to_long_str(), map));
                            }
                            li {
                                strong {"Players: "}
                                (format!("{}/{}", players.0, players.1))
                            }
                        }
                    }
                }
            }
        }
        TypedEndpoint::Irc(con) => html! {
            .endpoint-brief.online[online].offline[!online] {
                h3.endpoint_id.online[online].offline[!online] {
                    a href=(format!("/endpoint/{endpoint_id}")) {
                        (endpoint_id);
                    }
                }
                ul {
                    li {
                        strong {"Server: "}
                        (con.config.server)
                    }
                    li {
                        strong {"nickname: "}
                        (con.config.nickname)
                    }
                    li {
                        strong {"channels: "}
                        (con.config.channels.join(", "))
                    }
                }
            }
        },
        TypedEndpoint::Discord(con) => html! {
            .endpoint-brief.online[online].offline[!online] {
                h3.endpoint_id.online[online].offline[!online] {
                    a href=(format!("/endpoint/{endpoint_id}")) {
                        (endpoint_id);
                    }
                }
                ul {
                    li {
                        strong {"channels: "}
                        (con.channels.left_values().map(|s| s.as_str()).collect::<Vec<_>>().join(", "))
                    }
                }
            }
        },
    }
}

async fn not_found(request: HttpRequest) -> HttpResponse {
    HttpResponse::NotFound()
        .insert_header(("Content-Type", "text/html"))
        .body(not_found_page(request.path()).into_string())
}

fn not_found_page(p: &str) -> Markup {
    base_page(
        "OOPS ERROR 404 PAGE NOT FOUND!!111",
        None,
        html! {
            h2.error {"Oh man, page not found! What a disaster."}
            div.center-text {
                p {
                    code {(p)};
                    "... are you sure it should be there? ";
                    a href="/" {"maybe let's go home?"}
                }
            }
        },
    )
}

fn base_page(title: &str, user: Option<User>, content: Markup) -> Markup {
    html! {
        (DOCTYPE)
        html {
            head {
                link rel="stylesheet" href="/static/main.css";
                link rel="stylesheet" href="/static/icofont/icofont.min.css";
                meta name="viewport" content="width=device-width, initial-scale=1";
                title {(title) " | XAGY3"}
            }
            body {
                header {
                    h1 {"XAGY3"}
                }
                div.pure-g {
                    ."pure-u-1-5" {}
                    ."pure-u-3-5" {
                        main.pure-g {
                            ."pure-u-1-5" {
                                .global-menu {
                                    h2 {
                                        a href="/" {"Servers"}
                                    }
                                    h2 {
                                        a href="/players" {"Players"}
                                    }
                                    h2 {
                                        a href="/cts-records" {"CTS Records"}
                                    }
                                    h2 {
                                        a href="/cts-records-anon" {"Anon CTS Records"}
                                    }
                                    h2 {
                                        "Map rating"
                                    }
                                    h2 {
                                        "Feedback"
                                    }
                                    h2 {
                                        "Banlists"
                                    }
                                }
                            }
                            ."pure-u-4-5" {
                                (content);
                            }
                        }
                    }
                    ."pure-u-1-5" {}
                }
                footer {
                    @if let Some(u) = user {
                        div.userbox {
                            (PreEscaped("Hello,&nbsp;"));
                            (u.username);
                            (PreEscaped("&nbsp;"));
                            form.pure-form method="post" action="/logout" {
                                button.button-xsmall.pure-button.logout-button {"logout"}
                            }
                        }
                    }
                    "XAGY3. (c) 2022 N. Savchenko morosophos@teichisma.info"
                }
                script {
                    (PreEscaped(r#"
document.querySelectorAll('.copy-text').forEach((el) => {
    el.addEventListener('click' , (event)=> {
        event.preventDefault();
        console.log(event);
        var textToCopy = event.target.attributes['data-original-string'].value;
        navigator.clipboard.writeText(textToCopy).then(
          function() {
           /* clipboard successfully set */
           // window.alert('Success! The text was copied to your clipboard')
          },
          function() {
         /* clipboard write failed */
         // window.alert('Opps! Your browser does not support the Clipboard API')
        }
      )
      }
  )   })"#
                    ))
                }
            }
        }
    }
}

fn login_page(_error: bool, form: Option<LoginForm>) -> Markup {
    base_page(
        "Login",
        None,
        html! {
            h2 {"Wow, you want to log in!"}
            div.center-text {
                p {"That's a great idea. But let's do the password entry challenge first."}
                form.pure-form.pure-form-aligned method="post" action="/login" {
                    fieldset {
                        div.pure-control-group {
                            label for="username" {"user name"}
                            @if let Some(ref f) = form {
                                input name="username" required autofocus value=(f.username);
                            } else {
                                input name="username" required autofocus;
                            }
                        }
                        div.pure-control-group {
                            label for="password" {"password"}
                            input name="password" required type="password";
                        }
                    }
                    button.pure-button {"\u{1F496} submit \u{1F496}"}
                }
            }
        },
    )
}

fn dps_copy(dp: &DPString) -> Markup {
    html! {
        (PreEscaped(dp.to_html()));
        " "
        a.copy-text href="" {
            i.icofont-copy data-original-string=(dp.to_dp()) {}
        }
    }
}

#!/bin/bash

set -e

SERVER=xon.teichisma.info
USER=xonotic
TARGET_DIR=target/x86_64-unknown-linux-musl/release

TARGET_CC=x86_64-linux-musl-gcc cargo build --release --target x86_64-unknown-linux-musl
# strip ./target/release/xagy3
scp ./$TARGET_DIR/xagy3 "$USER@$SERVER:/home/xonotic/xagy3/xagy3.tmp"
ssh "root@$SERVER" "systemctl stop xagy3"
ssh "$USER@$SERVER" "mv /home/xonotic/xagy3/xagy3.tmp /home/xonotic/xagy3/xagy3"
ssh "root@$SERVER" "systemctl start xagy3"

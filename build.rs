use rsass::{compile_scss_path, output};

fn main() {
    println!("cargo:rerun-if-changed=css/");
    println!("Building CSS...");
    let path = "css/main.scss";
    let format = output::Format {
        style: output::Style::Compressed,
        ..Default::default()
    };
    let css = compile_scss_path(path.as_ref(), format).unwrap();
    std::fs::write("static/main.css", css).unwrap();
}

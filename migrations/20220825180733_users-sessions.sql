CREATE TABLE web_user (
       id UUID NOT NULL primary key,
       username character varying(256) NOT NULL,
       created_at timestamp WITH TIME ZONE NOT NULL,
       last_login_at timestamp WITH TIME ZONE NOT NULL,
       password character varying(256) NOT NULL,
       player_id integer NOT NULL references player(id) on delete restrict
);

CREATE TABLE web_session (
       id character varying(256) NOT NULL primary key,
       user_id UUID NOT NULL references web_user(id) on delete cascade,
       data text
);

CREATE TABLE feedback (
       id serial primary key,
       player_id integer references player(id) on delete cascade,
       endpoint_id character varying(32) not null,
       created_at timestamp WITH TIME ZONE NOT NULL,
       message text NOT NULL
);

CREATE INDEX feedback__endpoint_id ON feedback USING btree (endpoint_id);
CREATE INDEX feedback__created_at ON feedback USING btree (created_at);

ALTER TABLE feedback ADD COLUMN feedback_type character varying(16) NOT NULL;
CREATE INDEX feedback__feedback_type ON feedback USING btree(feedback_type);

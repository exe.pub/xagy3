CREATE TABLE player (
       id serial primary key,
       nickname character varying(256) NOT NULL,
       nickname_nocolors character varying(256) NOT NULL,
       created_at timestamp WITH TIME ZONE NOT NULL,
       admin_notes text NOT NULL default ''
);

CREATE INDEX player__nickname ON player USING btree (nickname_nocolors);

CREATE TABLE player_key (
       id serial primary key,
       player_id integer not null references player(id) on delete cascade,
       crypto_idfp character varying(256) not null,
       created_at timestamp WITH TIME ZONE not null,
       last_used_at timestamp WITH TIME ZONE not null
);

CREATE INDEX player_key__crypto_idfp ON player_key USING btree (crypto_idfp);

CREATE TABLE player_identification (
       id serial primary key,
       player_key_id integer NULL references player_key(id) on delete restrict,
       endpoint_id character varying(32) not null,
       stats_id integer,
       nickname character varying(256) NOT NULL,
       nickname_nocolors character varying(256) NOT NULL,
       created_at timestamp WITH TIME ZONE not null,
       asn_id integer,
       asn_name character varying(512),
       country character varying(3),
       city character varying(255),
       subdivision_codes character varying(128),
       subdivision_names text,
       continent character varying(64),
       radius integer,
       latitude double precision,
       longitude double precision
);

CREATE INDEX player_identification__endpoint_id ON player_identification USING btree (endpoint_id);
CREATE INDEX player_identification__stats_id ON player_identification USING btree (stats_id);
CREATE INDEX player_identification__nickname ON player_identification USING btree (nickname_nocolors);
CREATE INDEX player_identification__created_at ON player_identification USING btree (created_at);
CREATE INDEX player_identification__asn_id ON player_identification USING btree (asn_id);
CREATE INDEX player_identification__asn_name ON player_identification USING btree (asn_name);
CREATE INDEX player_identification__country ON player_identification USING btree (country);
CREATE INDEX player_identification__city ON player_identification USING btree (city);
CREATE INDEX player_identification__subdivision_codes ON player_identification USING btree (subdivision_codes);
CREATE INDEX player_identification__continent ON player_identification USING btree (continent);
CREATE INDEX player_identification__latitude ON player_identification USING btree (latitude);
CREATE INDEX player_identification__longitude ON player_identification USING btree (longitude);

CREATE TABLE map_rating (
       id serial primary key,
       player_id integer NOT NULL references player(id) on delete restrict,
       endpoint_id character varying(32) not null,
       map character varying(128) not null,
       vote smallint not null,
       comment text not null default '',
       created_at timestamp WITH TIME ZONE not null
);

CREATE INDEX map_rating__endpoint_id ON map_rating USING btree (endpoint_id);
CREATE INDEX map_rating__map ON map_rating USING btree (map);
CREATE INDEX map_rating__vote ON map_rating USING btree (vote);

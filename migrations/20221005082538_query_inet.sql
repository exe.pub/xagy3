CREATE OR REPLACE FUNCTION query_inet(query text)
RETURNS INET AS $$
DECLARE return_value INET DEFAULT NULL;
BEGIN
    BEGIN
        return_value := INET(query);
    EXCEPTION WHEN OTHERS THEN
        RETURN NULL;
    END;
RETURN return_value;
END;
$$ LANGUAGE plpgsql;

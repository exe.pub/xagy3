CREATE TABLE cts_record_anon (
       id serial not null primary key,
       endpoint_id character varying(32) not null,
       nickname character varying(256) NOT NULL,
       nickname_nocolors character varying(256) not null,
       created_at timestamp WITH TIME ZONE NOT NULL,
       time character varying(128) NOT NULL,
       map character varying(128) not null
);

CREATE INDEX cts_record_anon__nickname ON cts_record_anon(nickname_nocolors);
CREATE INDEX cts_record_anon__created_at ON cts_record_anon(created_at);
CREATE INDEX cts_record_anon__endpoint_id ON cts_record_anon(endpoint_id);
CREATE INDEX cts_record_anon__map ON cts_record_anon(map);

CREATE TABLE cts_record (
       id serial not null primary key,
       endpoint_id character varying(32) not null,
       created_at timestamp WITH TIME ZONE NOT NULL,
       player_key_id integer NULL references player_key(id) on delete restrict,
       nickname character varying(256) NOT NULL,
       nickname_nocolors character varying(256) NOT NULL,
       time numeric(20, 5) NOT NULL,
       map character varying(128) not null,
       ip_addr inet NOT NULL
);

CREATE INDEX cts_record__nickname ON cts_record(nickname_nocolors);
CREATE INDEX cts_record__created_at ON cts_record(created_at);
CREATE INDEX cts_record__endpoint_id ON cts_record(endpoint_id);
CREATE INDEX cts_record__map ON cts_record(map);
CREATE INDEX cts_record__ip_addr ON cts_record(ip_addr);

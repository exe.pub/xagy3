ALTER TABLE player_identification ADD COLUMN ip_addr inet NOT NULL default '127.0.0.1';
CREATE INDEX player_identification__ip_addr ON player_identification USING btree (ip_addr);
